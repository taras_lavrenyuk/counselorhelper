package com.lavreniuk.counselorhelper.adapters.pageradapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.lavreniuk.counselorhelper.activities.fragments.PupilsListFragment
import com.lavreniuk.counselorhelper.activities.fragments.SquadInfoFragment

class SquadActivityPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private val numberOfTabs = 2

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                SquadInfoFragment()
            }
            1 -> {
                PupilsListFragment()
            }
            else -> {
                assert(false) { "Wrong isSomeActivityWillBeStarted!!! Process!!!" }
                SquadInfoFragment()
            }
        }
    }

    override fun getCount(): Int {
        return numberOfTabs
    }
}