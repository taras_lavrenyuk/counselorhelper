package com.lavreniuk.counselorhelper.adapters.pageradapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.lavreniuk.counselorhelper.activities.fragments.NoteFragment
import com.lavreniuk.counselorhelper.activities.fragments.ProgramActivityFragment

class ProgramActivityPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private val numberOfTabs = 2

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ProgramActivityFragment()
            1 -> NoteFragment()
            else -> {
                assert(false) { "Wrong isSomeActivityWillBeStarted!!! Process!!!" }
                ProgramActivityFragment()
            }
        }
    }

    override fun getCount(): Int {
        return numberOfTabs
    }
}