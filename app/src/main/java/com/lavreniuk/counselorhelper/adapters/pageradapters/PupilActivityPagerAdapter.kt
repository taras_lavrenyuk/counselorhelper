package com.lavreniuk.counselorhelper.adapters.pageradapters

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.lavreniuk.counselorhelper.activities.fragments.InfoAboutPupilFragment
import com.lavreniuk.counselorhelper.activities.fragments.PupilContactsFragment
import com.lavreniuk.counselorhelper.activities.fragments.PupilRelativesFragment

class PupilActivityPagerAdapter(fm: FragmentManager, private val pupilId: String) : FragmentStatePagerAdapter(fm) {

    private val numberOfTabs = 3

    override fun getItem(position: Int): Fragment {
        val args = Bundle()
        args.putString("pupil-id", pupilId)
        when (position) {
            0 -> {
                val infoAboutPupilFragment = InfoAboutPupilFragment()
                infoAboutPupilFragment.arguments = args
                return infoAboutPupilFragment
            }
            1 -> {
                val pupilsContactsFragment = PupilContactsFragment()
                pupilsContactsFragment.arguments = args
                return pupilsContactsFragment
            }
            2 -> {
                val pupilParentsFragment = PupilRelativesFragment()
                pupilParentsFragment.arguments = args
                return pupilParentsFragment
            }
            else -> {
                assert(false) { "Wrong isSomeActivityWillBeStarted!!! Process!!!" }
                val infoAboutPupilFragment = InfoAboutPupilFragment()
                infoAboutPupilFragment.arguments = args
                return infoAboutPupilFragment
            }
        }
    }

    override fun getCount(): Int {
        return numberOfTabs
    }
}