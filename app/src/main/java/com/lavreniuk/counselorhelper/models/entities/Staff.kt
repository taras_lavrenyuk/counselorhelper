package com.lavreniuk.counselorhelper.models.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.lavreniuk.counselorhelper.enums.Gender
import com.lavreniuk.counselorhelper.utils.UtilsMethods

@Entity(tableName = "staff")
data class Staff(
    @PrimaryKey
    var staffId: String,
    var position: String? = null,
    var orderInList: Long
) : PersonInfo(
    "staff"
) {
    @Ignore
    constructor(
        staffId: String = UtilsMethods.generateId(),
        firstName: String,
        secondName: String?,
        lastName: String?,
        position: String?,
        additionalInfo: String?,
        orderInList: Long
    ) : this(staffId, position, orderInList) {
        this.firstName = firstName
        this.secondName = secondName
        this.lastName = lastName
        this.bio = additionalInfo
        this.photo = ""
        this.birthday = null
        this.gender = Gender.Male
    }
}