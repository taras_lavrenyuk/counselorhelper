package com.lavreniuk.counselorhelper.models.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "squads")
data class Squad(
    @PrimaryKey var squadId: String,
    var squadName: String = "",
    var photo: String = ""
)