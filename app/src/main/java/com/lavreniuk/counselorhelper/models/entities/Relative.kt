package com.lavreniuk.counselorhelper.models.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "relatives")
data class Relative(
    @PrimaryKey var relativeId: String,
    var relativeOf: String,
    var connection: String,
    var relativeContact: String
) : PersonInfo(
    "Relative name"
) {
    @Ignore
    constructor(
        relativeId: String,
        firstName: String,
        secondName: String?,
        lastName: String?,
        connection: String,
        relativeContact: String
    ) : this(
        relativeId, "", connection, relativeContact
    ) {
        this.firstName = firstName
        this.secondName = secondName
        this.lastName = lastName
    }

    @Ignore
    constructor(
        relativeId: String,
        firstName: String,
        secondName: String,
        lastName: String,
        relativeOf: String,
        connection: String,
        relativeContact: String
    ) : this(
        relativeId, relativeOf, connection, relativeContact
    ) {
        this.firstName = firstName
        this.secondName = secondName
        this.lastName = lastName
    }
}