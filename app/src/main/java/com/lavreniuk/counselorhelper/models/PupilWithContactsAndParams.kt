package com.lavreniuk.counselorhelper.models

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation
import com.lavreniuk.counselorhelper.models.entities.Contact
import com.lavreniuk.counselorhelper.models.entities.Param
import com.lavreniuk.counselorhelper.models.entities.Pupil

class PupilWithContactsAndParams(

    @Embedded
    var pupil: Pupil? = null,

    @Relation(parentColumn = "pupilId", entityColumn = "personId")
    var contacts: List<Contact> = ArrayList(),

    @Relation(parentColumn = "pupilId", entityColumn = "entityId")
    var params: List<Param> = ArrayList()

)