package com.lavreniuk.counselorhelper.models.dtos

import android.arch.persistence.room.Ignore
import android.os.Parcel
import android.os.Parcelable

class NameDto(
    var firstName: String,
    var secondName: String?,
    var lastName: String?
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(firstName)
        parcel.writeString(secondName)
        parcel.writeString(lastName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NameDto> {
        override fun createFromParcel(parcel: Parcel): NameDto {
            return NameDto(parcel)
        }

        override fun newArray(size: Int): Array<NameDto?> {
            return arrayOfNulls(size)
        }
    }

    @Ignore
    constructor(firstName: String) : this(firstName, "", "")
}