package com.lavreniuk.counselorhelper.models.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "params")
data class Param(
    @PrimaryKey var paramId: String,
    var entityId: String,
    var paramValue: String,
    var paramName: String,
    var orderInList: Long
)