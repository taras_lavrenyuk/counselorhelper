package com.lavreniuk.counselorhelper.models

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation
import com.lavreniuk.counselorhelper.models.entities.Contact
import com.lavreniuk.counselorhelper.models.entities.User

class UserWithContacts(

    @Embedded
    var user: User? = null,

    @Relation(parentColumn = "userId", entityColumn = "personId")
    var contacts: List<Contact> = ArrayList()

)
