package com.lavreniuk.counselorhelper.models.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.lavreniuk.counselorhelper.models.dtos.Time

@Entity(tableName = "program_activities")
data class ProgramActivity(
    @PrimaryKey var programActivityId: String,
    var programActivityName: String, // also unique
    var programActivityDescription: String,
    var startTime: Time,
    var endTime: Time,
    var squadId: String,
    var orderInList: Long
)