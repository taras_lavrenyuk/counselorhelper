package com.lavreniuk.counselorhelper.models.dtos

import com.lavreniuk.counselorhelper.watchers.TimeEntryTextWatcher

/*
* Value (-1, -1) means time does not set
*/
class Time(
    private var hours: Int,
    private var minutes: Int,
    private var isTimeSet: Boolean
) {

    constructor(isTimeSet: Boolean) : this() {
        this.isTimeSet = isTimeSet
    }

    constructor(hours: Int, minutes: Int) : this() {
        if (hours == -1 && minutes == -1) {
            isTimeSet = false
        }
        this.isTimeSet = true
        this.hours = hours
        this.minutes = minutes
    }

    constructor() : this(-1, -1, false)

    fun setHours(hours: Int) {
        this.hours = hours;
    }

    fun setMinutes(minutes: Int) {
        this.minutes = minutes;
    }

    override fun toString(): String {
        if (!isTimeSet) return ""
        if (minutes in 0..9) return "$hours:0$minutes"
        return "$hours:$minutes"
    }

    companion object {

        fun fromStringToObject(timeString: String): Time {
            if (timeString.isEmpty()) return Time(false)
            if (!isValidString(timeString)) {
                assert(false) { "Error!!! Time timeString is invalid!!!" }
                return Time(false)
            }
            val split = timeString.split(Regex(":"), 0)
            return Time(split[0].toInt(), split[1].toInt())
        }

        private fun isValidString(timeString: String): Boolean {
            return timeString.matches(Regex(TimeEntryTextWatcher.TIME_REGEX))
        }
    }

}