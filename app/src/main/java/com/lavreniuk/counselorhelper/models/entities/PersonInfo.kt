package com.lavreniuk.counselorhelper.models.entities

import android.arch.persistence.room.Ignore
import com.lavreniuk.counselorhelper.enums.Gender
import java.util.*

open class PersonInfo(
    var firstName: String,
    var secondName: String?,
    var lastName: String?,
    var birthday: Date?,
    var photo: String?,
    var bio: String?,
    var gender: Gender

) {
    @Ignore
    constructor(firstName: String) : this(firstName, null, null, null, null, "Bio", Gender.Male)
}