package com.lavreniuk.counselorhelper.models.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.lavreniuk.counselorhelper.enums.ContactType

@Entity(tableName = "contacts")
data class Contact(
    @PrimaryKey var contactId: String,
    var contactType: ContactType,
    var value: String = "",
    var personId: String = ""
)