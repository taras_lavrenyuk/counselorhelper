package com.lavreniuk.counselorhelper.models.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.lavreniuk.counselorhelper.enums.Gender
import java.util.*

@Entity(tableName = "pupils")
data class Pupil(
    @PrimaryKey
    @NonNull
    var pupilId: String,
    var pupilsSquads: List<String>
) : PersonInfo(
    "Your name"
) {
    @Ignore
    constructor(
        pupilId: String,
        firstName: String,
        birthday: Date,
        gender: Gender,
        bio: String,
        photo: String,
        pupilsSquads: List<String>
    ) : this(
        pupilId, pupilsSquads
    ) {
        this.firstName = firstName
        this.birthday = birthday
        this.bio = bio
        this.photo = photo
        this.gender = gender
        this.pupilsSquads = pupilsSquads
    }

    @Ignore
    constructor(
        pupilId: String, firstName: String, lastName: String, pupilsSquads: List<String>
    ) : this(pupilId, pupilsSquads) {
        this.firstName = firstName
        this.lastName = lastName
        this.pupilsSquads = pupilsSquads
    }
}