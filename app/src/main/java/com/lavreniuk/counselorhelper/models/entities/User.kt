package com.lavreniuk.counselorhelper.models.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.lavreniuk.counselorhelper.enums.Gender
import java.util.*

@Entity(tableName = "users")
data class User(
    @PrimaryKey var userId: String
) : PersonInfo(
    "Your name"
) {
    @Ignore
    constructor(personId: String, firstName: String, birthday: Date) : this(personId) {
        this.firstName = firstName
        this.birthday = birthday
        this.bio = "Your bio"
        this.photo = ""
        this.gender = Gender.Male
    }
}