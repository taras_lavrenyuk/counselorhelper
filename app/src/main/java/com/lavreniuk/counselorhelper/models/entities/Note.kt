package com.lavreniuk.counselorhelper.models.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.lavreniuk.counselorhelper.enums.NoteOrTask
import com.lavreniuk.counselorhelper.enums.TaskStatus
import java.util.*

@Entity(tableName = "notes")
data class Note(
    @PrimaryKey var noteId: String,
    var noteText: String,
    var noteTargetDate: Date?,
    var noteOrTask: NoteOrTask,
    var taskStatus: TaskStatus?,
    var programActivityId: String?,
    var programActivityName: String?,
    var squadId: String,
    var orderInList: Long
)