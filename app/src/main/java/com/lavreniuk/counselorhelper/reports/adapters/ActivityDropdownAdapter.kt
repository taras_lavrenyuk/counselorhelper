package com.lavreniuk.counselorhelper.reports.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.models.entities.ProgramActivity


class ActivityDropdownAdapter(
    context: Context,
    private var initActivityList: List<ProgramActivity>,
    private var suggestionList: List<ProgramActivity> = emptyList()
) : ArrayAdapter<ProgramActivity>(
    context,
    R.layout.simple_dropdown_item_view,
    R.id.simple_dropdown_textview,
    suggestionList
) {

    private val activityFilter: ActivityFilter = ActivityFilter()

    override fun getCount() = suggestionList.size

    override fun getItem(position: Int) = suggestionList[position]

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(parent.context).inflate(
            R.layout.simple_dropdown_item_view,
            parent,
            false
        )
        view.findViewById<TextView>(R.id.simple_dropdown_textview).text = getItem(position).programActivityName
        return view
    }

    override fun getFilter(): Filter {
        return activityFilter
    }

    inner class ActivityFilter : Filter() {

        private val lock = Any()

        override fun performFiltering(searchString: CharSequence?): FilterResults {
            val filterResults = FilterResults()

            if (searchString.isNullOrBlank()) {
                synchronized(lock) {
                    filterResults.values = mutableListOf<String>()
                    filterResults.count = 0
                }
            } else {
                val prefixes = searchString.toString().split(" ")
                var filteredPupilsSet = emptySet<ProgramActivity>()
                for (prefix in prefixes) {
                    val list = initActivityList.filter {
                        it.programActivityName.contains(prefix, true) ||
                                it.programActivityDescription.contains(prefix, true)
                    }.toList()
                    filteredPupilsSet = filteredPupilsSet.plus(list)
                }
                filterResults.values =
                    filteredPupilsSet.toList().sortedWith(compareBy(ProgramActivity::orderInList))
                filterResults.count = filteredPupilsSet.size
            }
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            suggestionList = if (results?.values != null) {
                results.values as List<ProgramActivity>
            } else {
                emptyList()
            }
            if (results != null && results.count > 0) {
                notifyDataSetChanged()
            } else {
                notifyDataSetInvalidated()
            }
        }

    }
}