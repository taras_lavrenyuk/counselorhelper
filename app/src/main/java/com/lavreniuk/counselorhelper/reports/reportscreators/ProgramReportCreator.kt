package com.lavreniuk.counselorhelper.reports.reportscreators

import android.content.Context
import com.itextpdf.text.Chunk
import com.itextpdf.text.Document
import com.itextpdf.text.Paragraph
import com.itextpdf.text.Phrase
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.dao.NoteDao
import com.lavreniuk.counselorhelper.dao.ProgramActivityDao
import com.lavreniuk.counselorhelper.enums.TaskStatus
import com.lavreniuk.counselorhelper.models.entities.Note
import com.lavreniuk.counselorhelper.models.entities.ProgramActivity
import com.lavreniuk.counselorhelper.reports.models.dtos.AbstractReportDto
import com.lavreniuk.counselorhelper.reports.models.dtos.ProgramReportDto
import com.lavreniuk.counselorhelper.reports.utils.ReportUtils
import com.lavreniuk.counselorhelper.utils.PreferencesUtilsMethods
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils

class ProgramReportCreator(
    private val programActivityDao: ProgramActivityDao,
    private val noteDao: NoteDao
) : AbstractReportCreator {

    override fun createReport(
        document: Document,
        dto: AbstractReportDto,
        context: Context
    ): Document? {
        if (dto is ProgramReportDto) {
            // set metadata
            dto.reportName?.let { document.addTitle(it) }

            // set title
            dto.reportName?.let {
                val title = Paragraph(it, ReportUtils.TITLE_FONT)
                title.alignment = Paragraph.ALIGN_CENTER
                document.add(title)
            }

            // add content
            val programActivities = programActivityDao.getBySquadSortedByOrder(
                squadId = PreferencesUtilsMethods.getCurrentSquadId(context)!!,
                excludedActivityIds = if (dto.includeAllActivities) emptyList() else dto.excludedActivityIds
            )

            for (programActivity in programActivities) {
                val programActivityParagraph = Paragraph()
                addEmptyLine(programActivityParagraph, 1)
                val programNamePhrase = Phrase()

                if (dto.includeTime) {
                    programNamePhrase.add(
                        Chunk(
                            StringTransformationUtils.getProgramActivityTime(programActivity, context, false),
                            ReportUtils.PROGRAM_TIME_FONT
                        )
                    )
                }
                programNamePhrase.add(
                    Chunk(
                        " ${programActivity.programActivityName}",
                        ReportUtils.PROGRAM_NAME_TIME_FONT
                    )
                )
                if (dto.includeProgramDescription && programActivity.programActivityDescription.isNotBlank()) {
                    programNamePhrase.add(
                        Chunk(
                            " ${programActivity.programActivityDescription}",
                            ReportUtils.PROGRAM_DESCRIPTION_FONT
                        )
                    )
                }
                programActivityParagraph.add(programNamePhrase)

                // add notes (with date first)
                if (dto.includeNotes) {
                    val notes: List<Note> = if (dto.date != null) {
                        noteDao.getNotesByProgramActivityAndDateSortedByDescending(
                            programActivity.programActivityName,
                            dto.date!!
                        )
                    } else {
                        noteDao.getNotesByProgramActivitySortedByDescending(
                            programActivity.programActivityName
                        )
                    }
                    for (note in notes) {
                        val noteParagraph = Paragraph()
                        if (note.noteTargetDate != null) {
                            noteParagraph.add(
                                Chunk(
                                    "${StringTransformationUtils.fromDateToString(note.noteTargetDate!!)} ",
                                    ReportUtils.PROGRAM_NOTE_DATE_FONT
                                )
                            )
                        }
                        noteParagraph.add(
                            Chunk(
                                "${context.getString(R.string.ui_note)}:",
                                ReportUtils.PROGRAM_NOTE_DATE_FONT
                            )
                        )
                        noteParagraph.add(
                            Chunk(
                                " ${note.noteText}",
                                ReportUtils.PROGRAM_NOTE_TEXT_FONT
                            )
                        )
                        programActivityParagraph.add(noteParagraph)
                    }
                }

                // add todos (with date first)
                if (dto.includeTodoTasks) {
                    addTasksToParagraph(
                        getTaskList(dto, programActivity, TaskStatus.Todo),
                        context,
                        programActivityParagraph,
                        TaskStatus.Todo
                    )
                }

                // add dones (with date first)
                if (dto.includeDoneTasks) {
                    addTasksToParagraph(
                        getTaskList(dto, programActivity, TaskStatus.Done),
                        context,
                        programActivityParagraph,
                        TaskStatus.Done
                    )
                }
                document.add(programActivityParagraph)
            }
            return document
        }
        throw Exception("The dto object is of wrong type.")
    }

    private fun getTaskList(
        dto: ProgramReportDto,
        programActivity: ProgramActivity,
        taskStatus: TaskStatus
    ): List<Note> {
        return if (dto.date != null) {
            noteDao.getTasksByProgramActivityAndStatusAndDateSortedByDateAsc(
                taskStatus.toString(),
                programActivity.programActivityName,
                dto.date!!
            )
        } else {
            noteDao.getTasksByProgramActivityAndStatusSortedByDateAsc(
                taskStatus.toString(),
                programActivity.programActivityName
            )
        }
    }

    private fun addTasksToParagraph(
        tasks: List<Note>,
        context: Context,
        paragraph: Paragraph,
        taskStatus: TaskStatus
    ) {
        for (task in tasks) {
            val noteParagraph = Paragraph()
            if (task.noteTargetDate != null) {
                noteParagraph.add(
                    Chunk(
                        "${StringTransformationUtils.fromDateToString(task.noteTargetDate!!)} ",
                        ReportUtils.PROGRAM_NOTE_DATE_FONT
                    )
                )
            }
            noteParagraph.add(
                Chunk(
                    if (taskStatus == TaskStatus.Todo) context.getString(R.string.ui_todo) else context.getString(R.string.ui_done),
                    ReportUtils.PROGRAM_NOTE_DATE_FONT
                )
            )
            noteParagraph.add(
                Chunk(
                    " ${task.noteText}",
                    ReportUtils.PROGRAM_NOTE_TEXT_FONT
                )
            )
            paragraph.add(noteParagraph)
        }
    }
}