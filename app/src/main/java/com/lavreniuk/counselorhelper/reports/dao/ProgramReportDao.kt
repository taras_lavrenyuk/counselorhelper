package com.lavreniuk.counselorhelper.reports.dao

import android.arch.persistence.room.*
import com.lavreniuk.counselorhelper.reports.models.ProgramReport

@Dao
interface ProgramReportDao {

    @Insert
    fun save(pupilListReport: ProgramReport)

    @Delete
    fun delete(pupilListReport: ProgramReport)

    @Update
    fun update(pupilListReport: ProgramReport)

    @Query("SELECT * FROM program_reports PR WHERE PR.reportId = :reportId ")
    fun getById(reportId: String): ProgramReport

    @Query("UPDATE program_reports SET reportName = :reportName WHERE reportId = :reportId ")
    fun updateReportName(reportId: String, reportName: String)

    @Query("DELETE FROM program_reports WHERE reportId = :reportId ")
    fun deleteById(reportId: String)
}