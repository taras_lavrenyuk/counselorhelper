package com.lavreniuk.counselorhelper.reports.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import com.lavreniuk.counselorhelper.enums.ReportType

@Entity(tableName = "pupil_reports")
data class PupilListReport(
    var includeAllPupils: Boolean,
    var includeAge: Boolean,
    var includeBirthday: Boolean,
    var includeParams: Boolean,
    var includeAllContacts: Boolean,
    var includeNumber: Boolean,
    var addBlanks: Boolean,
    var excludedPupilIds: MutableList<String> = mutableListOf()
) : AbstractReport() {

    @Ignore
    constructor() : this(true, false, false, false, false, false, false)

    @Ignore
    constructor(orderInList: Long, squadId: String) : this() {
        this.orderInList = orderInList
        this.squadId = squadId
        this.reportType = ReportType.PupilListReport
    }

}