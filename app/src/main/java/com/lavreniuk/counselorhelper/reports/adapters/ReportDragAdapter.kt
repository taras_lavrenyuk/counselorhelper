package com.lavreniuk.counselorhelper.reports.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.enums.ReportType
import com.lavreniuk.counselorhelper.reports.activities.ProgramReportEditorActivity
import com.lavreniuk.counselorhelper.reports.activities.PupilListReportEditorActivity
import com.lavreniuk.counselorhelper.reports.activities.ReportsListActivity
import com.lavreniuk.counselorhelper.reports.models.AbstractReport
import com.woxthebox.draglistview.DragItemAdapter

class ReportDragAdapter(
    list: Array<AbstractReport>,
    private val reportListActivity: ReportsListActivity,
    private val activityOnResume: () -> Unit,
    private val deleteReport: (AbstractReport) -> Unit,
    private val getSpecificReport: (String, ReportType) -> AbstractReport?
) : DragItemAdapter<AbstractReport, ReportDragAdapter.ViewHolder>() {

    init {
        itemList = list.toMutableList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.report_drag_item_view,
                parent,
                false
            )
        )
    }

    override fun getUniqueItemId(position: Int): Long {
        return mItemList[position].orderInList
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.initHolder(mItemList[position], reportListActivity)
    }

    fun getReportIdOnPosition(position: Int): String {
        return mItemList[position].reportId
    }

    inner class ViewHolder(itemView: View) :
        DragItemAdapter.ViewHolder(itemView, R.id.report_drag_item_layout, true) {

        private val reportName: TextView = itemView.findViewById(R.id.report_name)
        private val reportType: TextView = itemView.findViewById(R.id.report_type)
        private val reportParamIcon: ImageView = itemView.findViewById(R.id.report_param_icon)


        private lateinit var currentReport: AbstractReport

        override fun onItemLongClicked(view: View): Boolean {
            Toast.makeText(reportListActivity, "LONG CLICK", Toast.LENGTH_SHORT).show()
            return true
        }

        fun initHolder(report: AbstractReport, context: Context) {

            this.currentReport = report

            report.reportName?.let { reportName.text = it }
            report.reportType?.let { reportType.text = it.getUiString(context) }

            itemView.tag = report.orderInList

            reportParamIcon.setOnClickListener {
                val popupMenu = PopupMenu(context, reportParamIcon)

                val editPopupMenuItem = popupMenu.menu.add(context.getString(R.string.ui_edit))
                editPopupMenuItem.setOnMenuItemClickListener {
                    when (currentReport.reportType) {
                        ReportType.PupilListReport -> {
                            val intent = Intent(reportListActivity, PupilListReportEditorActivity::class.java)
                            intent.putExtra(context.getString(R.string.intent_report_id), currentReport.reportId)
                            reportListActivity.startActivity(intent)
                        }
                        ReportType.ProgramReport -> {
                            val intent = Intent(reportListActivity, ProgramReportEditorActivity::class.java)
                            intent.putExtra(context.getString(R.string.intent_report_id), currentReport.reportId)
                            reportListActivity.startActivity(intent)
                        }
                        else -> assert(false) { "Specify creating EditorActivity procedure for specific report" }
                    }
                    return@setOnMenuItemClickListener true
                }

                val deletePopupMenuItem = popupMenu.menu.add(context.getString(R.string.ui_delete))
                deletePopupMenuItem.setOnMenuItemClickListener {
                    deleteReport(report)
                    activityOnResume()
                    Toast.makeText(context, context.getString(R.string.ui_deleted), Toast.LENGTH_SHORT).show()
                    return@setOnMenuItemClickListener true
                }
                popupMenu.show()
            }
        }
    }

}

