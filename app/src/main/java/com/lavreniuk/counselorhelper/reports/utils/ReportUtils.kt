package com.lavreniuk.counselorhelper.reports.utils

import com.itextpdf.text.Font
import com.itextpdf.text.pdf.BaseFont

// https://www.vogella.com/tutorials/JavaPDF/article.html
object ReportUtils {

    val TITLE_FONT = Font(
        BaseFont.createFont("/assets/fonts/Arial-BoldMT.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED),
        18f,
        Font.BOLD
    )
    val PUPIL_NAME_FONT = Font(
        BaseFont.createFont("/assets/fonts/Arial-BoldMT.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED),
        14f,
        Font.BOLD
    )
    val PUPIL_AGE_BIRTHDAY_FONT = Font(
        BaseFont.createFont("/assets/fonts/ArialMT.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED),
        14f,
        Font.NORMAL
    )
    val PARAM_FONT = Font(
        BaseFont.createFont("/assets/fonts/ArialMT.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED),
        11f,
        Font.NORMAL
    )

    val PROGRAM_NAME_TIME_FONT = Font(
        BaseFont.createFont("/assets/fonts/Arial-BoldMT.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED),
        14f,
        Font.BOLD
    )
    val PROGRAM_TIME_FONT = Font(
        BaseFont.createFont("/assets/fonts/Arial-ItalicMT.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED),
        14f,
        Font.BOLD
    )
    val PROGRAM_DESCRIPTION_FONT = Font(
        BaseFont.createFont("/assets/fonts/ArialMT.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED),
        14f,
        Font.NORMAL
    )
    val PROGRAM_NOTE_DATE_FONT = Font(
        BaseFont.createFont("/assets/fonts/Arial-BoldMT.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED),
        11f,
        Font.BOLD
    )
    val PROGRAM_NOTE_TEXT_FONT = Font(
        BaseFont.createFont("/assets/fonts/ArialMT.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED),
        11f,
        Font.NORMAL
    )


}