package com.lavreniuk.counselorhelper.reports.dao

import android.arch.persistence.room.*
import com.lavreniuk.counselorhelper.reports.models.AbstractReport

@Dao
interface AbstractReportDao {

    @Insert
    fun save(report: AbstractReport)

    @Update
    fun update(report: AbstractReport)

    @Delete
    fun delete(report: AbstractReport)

    @Query("UPDATE abstract_reports SET orderInList = :position WHERE reportId = :reportId ")
    fun updateOrderInList(reportId: String, position: Long)

    @Query("SELECT * from abstract_reports AR WHERE AR.squadId = :squadId ORDER BY AR.orderInList ")
    fun getBySquadIdOrderByOrderInList(squadId: String): List<AbstractReport>

    @Query("SELECT MAX(AR.orderInList) FROM abstract_reports AR WHERE AR.squadId = :squadId ")
    fun getSquadMaxOrderInListBySquadId(squadId: String): Long

    @Query("UPDATE abstract_reports SET reportName = :reportName WHERE reportId = :reportId ")
    fun updateReportName(reportId: String, reportName: String)
}