package com.lavreniuk.counselorhelper.reports.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.os.Environment
import android.widget.Toast
import com.itextpdf.text.Document
import com.itextpdf.text.pdf.PdfWriter
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.dao.AppDatabase
import com.lavreniuk.counselorhelper.markerinterfaces.AbstractReportViewModel
import com.lavreniuk.counselorhelper.reports.models.ProgramReport
import com.lavreniuk.counselorhelper.reports.models.dtos.AbstractReportDto
import com.lavreniuk.counselorhelper.reports.models.dtos.ProgramReportDto
import com.lavreniuk.counselorhelper.reports.reportscreators.ProgramReportCreator
import com.lavreniuk.counselorhelper.utils.PhotoLoaderUtils
import java.io.FileOutputStream

class ProgramReportViewModel(application: Application) : AndroidViewModel(application), AbstractReportViewModel {

    private val programReportDao = AppDatabase.getAppDataBase(application).programReportDao()
    private val programActivityDao = AppDatabase.getAppDataBase(application).programActivityDao()
    private val noteDao = AppDatabase.getAppDataBase(application).noteDao()

    private val context = application

    fun save(programReport: ProgramReport) = programReportDao.save(programReport)

    fun update(programReport: ProgramReport) = programReportDao.update(programReport)

    fun delete(programReport: ProgramReport) = programReportDao.delete(programReport)

    fun getById(reportId: String) = programReportDao.getById(reportId)

    fun deleteById(reportId: String) = programReportDao.deleteById(reportId)

    fun updateReportName(reportId: String, reportName: String) =
        programReportDao.updateReportName(reportId, reportName)

    // generates and saves report document and returns file name
    override fun generateReportDocument(dto: AbstractReportDto): String? {
        if (dto !is ProgramReportDto) {
            assert(false) { "Parameter \"dto\" must be of type ${ProgramReportDto::class.java} " }
        }
        val document = Document()
        val fileName = dto.reportName ?: "Report${System.currentTimeMillis()}"
        val filePath =
            "${Environment.getExternalStorageDirectory()}${PhotoLoaderUtils.REPORTS_DIRECTORY}/$fileName.pdf"
        PdfWriter.getInstance(document, FileOutputStream(filePath))
        document.open()

        val programReportCreator = ProgramReportCreator(programActivityDao, noteDao)

        try {
            programReportCreator.createReport(document, dto, context)
            Toast.makeText(
                context,
                "${context.getString(R.string.ui_report_saved_to)}: $filePath",
                Toast.LENGTH_SHORT
            ).show()
            document.close()
            return filePath
        } catch (e: Exception) {
            Toast.makeText(
                context,
                context.getString(R.string.ui_report_was_not_saved),
                Toast.LENGTH_SHORT
            ).show()
            document.close()
            return null
        }
    }
}