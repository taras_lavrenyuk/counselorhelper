package com.lavreniuk.counselorhelper.reports.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.itextpdf.text.log.LoggerFactory
import com.lavreniuk.counselorhelper.dao.AppDatabase
import com.lavreniuk.counselorhelper.enums.ReportType
import com.lavreniuk.counselorhelper.reports.models.AbstractReport
import com.lavreniuk.counselorhelper.reports.models.ProgramReport
import com.lavreniuk.counselorhelper.reports.models.PupilListReport


class ReportViewModel(application: Application) : AndroidViewModel(application) {

    private val pupilListReportViewModel = PupilListReportViewModel(application)
    private val programReportViewModel = ProgramReportViewModel(application)

    private val abstractReportDao = AppDatabase.getAppDataBase(application).abstractReportDao()

    private val logger = LoggerFactory.getLogger(ReportViewModel::class.java)

    fun save(report: AbstractReport) {
        abstractReportDao.save(report)
        saveSpecificReport(report)
    }

    fun update(report: AbstractReport) {
        abstractReportDao.update(report)
        updateSpecificReport(report)
    }

    fun delete(report: AbstractReport) {
        abstractReportDao.delete(report)
        deleteSpecificReport(report)
    }

    private fun saveSpecificReport(report: AbstractReport) {
        when (report.reportType) {
            ReportType.PupilListReport -> pupilListReportViewModel.save(report as PupilListReport)
            ReportType.ProgramReport -> programReportViewModel.save(report as ProgramReport)
            else -> {
                logger.error("Specify save procedure for specific report")
                throw Exception("Specify save procedure for specific report")
            }
        }
    }

    private fun updateSpecificReport(report: AbstractReport) {
        when (report.reportType) {
            ReportType.PupilListReport -> pupilListReportViewModel.update(report as PupilListReport)
            ReportType.ProgramReport -> programReportViewModel.update(report as ProgramReport)
            else -> {
                logger.error("Specify update procedure for specific report")
                throw Exception("Specify update procedure for specific report")
            }
        }
    }

    private fun deleteSpecificReport(report: AbstractReport) {
        when (report.reportType) {
            ReportType.PupilListReport -> pupilListReportViewModel.deleteById(report.reportId)
            ReportType.ProgramReport -> programReportViewModel.deleteById(report.reportId)
            else -> {
                logger.error("Specify delete procedure for specific report")
                throw Exception("Specify delete procedure for specific report")
            }
        }
    }

    private fun updateSpecificReportName(reportId: String, reportName: String, reportType: ReportType) {
        when (reportType) {
            ReportType.PupilListReport -> pupilListReportViewModel.updateReportName(reportId, reportName)
            ReportType.ProgramReport -> programReportViewModel.updateReportName(reportId, reportName)
            else -> {
                logger.error("Specify update report name procedure for specific report")
                throw Exception("Specify update report name procedure for specific report")
            }
        }
    }

    fun updateReportsOrder(
        startPosition: Int,
        endPosition: Int,
        getReportIdOnPosition: (position: Int) -> String
    ) {
        for (position in startPosition..endPosition) {
            updateOrderInList(getReportIdOnPosition(position), position + 1)
        }
    }

    private fun updateOrderInList(reportId: String, position: Int) {
        abstractReportDao.updateOrderInList(reportId, position.toLong())
    }

    fun getReportsSortedByOrderInList(squadId: String): List<AbstractReport> {
        return abstractReportDao.getBySquadIdOrderByOrderInList(squadId)
    }

    fun getOrderForNewReport(squadId: String): Long {
        return abstractReportDao.getSquadMaxOrderInListBySquadId(squadId) + 1
    }

    fun getSpecificReport(reportId: String, reportType: ReportType): AbstractReport? {
        return when (reportType) {
            ReportType.PupilListReport -> {
                pupilListReportViewModel.getById(reportId)
            }
            ReportType.ProgramReport -> {
                programReportViewModel.getById(reportId)
            }
            else -> throw java.lang.Exception("Unknown data type")
        }
    }

    fun saveReportName(reportId: String, reportName: String, reportType: ReportType) {
        abstractReportDao.updateReportName(reportId, reportName)
        updateSpecificReportName(reportId, reportName, reportType)
    }
}