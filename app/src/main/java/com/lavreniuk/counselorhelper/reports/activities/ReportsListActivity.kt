package com.lavreniuk.counselorhelper.reports.activities

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.enums.ReportType
import com.lavreniuk.counselorhelper.reports.adapters.ReportDragAdapter
import com.lavreniuk.counselorhelper.reports.models.AbstractReport
import com.lavreniuk.counselorhelper.reports.viewmodels.ReportViewModel
import com.lavreniuk.counselorhelper.utils.PreferencesUtilsMethods
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import com.woxthebox.draglistview.DragListView
import kotlinx.android.synthetic.main.activity_reports_list.*
import kotlin.concurrent.thread

class ReportsListActivity : AppCompatActivity() {

    private val reportViewModel: ReportViewModel by lazy {
        ViewModelProviders.of(this).get(ReportViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reports_list)

        add_pupils_list_report_menu_button.setOnClickListener {
            add_report_floating_param_button.close(false)
            startActivity(Intent(this, PupilListReportEditorActivity::class.java))
        }

        add_program_report_menu_button.setOnClickListener {
            add_report_floating_param_button.close(false)
            startActivity(Intent(this, ProgramReportEditorActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        UtilsMethods.hideKeyboard(this)
        fillReportList()

    }

    private fun fillReportList() {
        val currentSquadId = getCurrentSquadId()
        val reports: List<AbstractReport>

        reports = if (currentSquadId == null) {
            emptyList()
        } else {
            reportViewModel.getReportsSortedByOrderInList(currentSquadId)
        }

        val listAdapter = ReportDragAdapter(
            reports.toTypedArray(),
            this,
            { this.onResume() },
            { reportViewModel.delete(it) },
            { reportId: String, reportType: ReportType ->
                reportViewModel.getSpecificReport(
                    reportId,
                    reportType
                )
            }
        )

        val dragAndDropListener = object : DragListView.DragListListener {
            override fun onItemDragging(itemPosition: Int, x: Float, y: Float) {
            }

            override fun onItemDragStarted(position: Int) {
            }

            override fun onItemDragEnded(fromPosition: Int, toPosition: Int) {
                if (fromPosition != toPosition) {
                    thread(start = true) {
                        val getReportIdOnPosition =
                            { position: Int -> listAdapter.getReportIdOnPosition(position) }
                        if (fromPosition > toPosition) {
                            reportViewModel.updateReportsOrder(toPosition, fromPosition, getReportIdOnPosition)
                        } else {
                            reportViewModel.updateReportsOrder(fromPosition, toPosition, getReportIdOnPosition)
                        }
                    }
                }
            }
        }
        reports_draganddrop_list.setDragListListener(dragAndDropListener)
        reports_draganddrop_list.setLayoutManager(LinearLayoutManager(this))

        reports_draganddrop_list.setAdapter(listAdapter, true)
        reports_draganddrop_list.setCanDragHorizontally(false)
    }

    private fun getCurrentSquadId(): String? {
        val currentSquadId = PreferencesUtilsMethods.getCurrentSquadId(this)
        return if (currentSquadId == null) {
            assert(false) { "Error!!!. CurrentSquadId cannot not be null at this point of program!!!" }
            null
        } else {
            currentSquadId
        }
    }
}
