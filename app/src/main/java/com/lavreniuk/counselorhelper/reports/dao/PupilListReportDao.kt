package com.lavreniuk.counselorhelper.reports.dao

import android.arch.persistence.room.*
import com.lavreniuk.counselorhelper.reports.models.PupilListReport

@Dao
interface PupilListReportDao {

    @Insert
    fun save(pupilListReport: PupilListReport)

    @Delete
    fun delete(pupilListReport: PupilListReport)

    @Update
    fun update(pupilListReport: PupilListReport)

    @Query("SELECT * FROM pupil_reports PR WHERE PR.reportId = :reportId ")
    fun getById(reportId: String): PupilListReport

    @Query("UPDATE pupil_reports SET reportName = :reportName WHERE reportId = :reportId ")
    fun updateReportName(reportId: String, reportName: String)

    @Query("DELETE FROM pupil_reports WHERE reportId = :reportId ")
    fun deleteById(reportId: String)
}