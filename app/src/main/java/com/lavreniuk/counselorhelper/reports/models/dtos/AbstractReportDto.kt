package com.lavreniuk.counselorhelper.reports.models.dtos

abstract class AbstractReportDto(
    var reportName: String? = null
)