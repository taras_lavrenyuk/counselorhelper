package com.lavreniuk.counselorhelper.reports.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import com.lavreniuk.counselorhelper.enums.ReportType
import java.util.*

@Entity(tableName = "program_reports")
data class ProgramReport(
    var includeAllActivities: Boolean,
    var includeProgramDescription: Boolean,
    var includeTime: Boolean,
    var includeNotes: Boolean,
    var includeTasks: Boolean,
    var includeTodoTasks: Boolean,
    var includeDoneTasks: Boolean,
    var date: Date? = null,
    val excludedActivityIds: MutableList<String> = mutableListOf()
) : AbstractReport() {

    @Ignore
    constructor() : this(true, true, true, false, false, false, false)

    @Ignore
    constructor(reportName: String, squadId: String) : this() {
        this.reportName = reportName
        this.squadId = squadId
    }

    @Ignore
    constructor(orderInList: Long, squadId: String) : this() {
        this.orderInList = orderInList
        this.squadId = squadId
        this.reportType = ReportType.ProgramReport
    }


}