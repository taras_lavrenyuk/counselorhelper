package com.lavreniuk.counselorhelper.reports.itemviews

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.models.entities.ProgramActivity
import kotlinx.android.synthetic.main.excluded_element_item_view.view.*

class ExcludedActivityItemView(
    context: Context,
    programActivity: ProgramActivity,
    removeActivityFromExcludedList: (ProgramActivity) -> Unit
) : LinearLayout(context) {

    init {
        View.inflate(context, R.layout.excluded_element_item_view, this)
        excluded_element_name.text = programActivity.programActivityName

        delete_excluded_element_image_view.setOnClickListener {
            removeActivityFromExcludedList(programActivity)
        }
    }
}