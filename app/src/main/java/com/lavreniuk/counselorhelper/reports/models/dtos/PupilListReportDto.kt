package com.lavreniuk.counselorhelper.reports.models.dtos

class PupilListReportDto(
    var includeAllPupils: Boolean = true,
    var includeAge: Boolean = false,
    var includeBirthday: Boolean = false,
    var includeParams: Boolean = false,
    var includeAllContacts: Boolean = false,
    var includeNumber: Boolean = false,
    var addBlanks: Boolean = false,
    var excludedPupilIds: List<String> = emptyList(),
    reportName: String? = null
) : AbstractReportDto(reportName)