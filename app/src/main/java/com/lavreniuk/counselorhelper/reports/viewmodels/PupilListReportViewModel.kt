package com.lavreniuk.counselorhelper.reports.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.os.Environment
import android.widget.Toast
import com.itextpdf.text.Document
import com.itextpdf.text.pdf.PdfWriter
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.dao.AppDatabase
import com.lavreniuk.counselorhelper.markerinterfaces.AbstractReportViewModel
import com.lavreniuk.counselorhelper.reports.models.PupilListReport
import com.lavreniuk.counselorhelper.reports.models.dtos.AbstractReportDto
import com.lavreniuk.counselorhelper.reports.models.dtos.PupilListReportDto
import com.lavreniuk.counselorhelper.reports.reportscreators.PupilListReportCreator
import com.lavreniuk.counselorhelper.utils.PhotoLoaderUtils
import java.io.FileOutputStream

class PupilListReportViewModel(application: Application) : AndroidViewModel(application), AbstractReportViewModel {

    private val pupilListReportDao = AppDatabase.getAppDataBase(application).pupilListReportDao()
    private val pupilDao = AppDatabase.getAppDataBase(application).pupilDao()

    private val context = application

    fun save(pupilListReport: PupilListReport) = pupilListReportDao.save(pupilListReport)

    fun update(pupilListReport: PupilListReport) = pupilListReportDao.update(pupilListReport)

    fun delete(pupilListReport: PupilListReport) = pupilListReportDao.delete(pupilListReport)

    fun getById(reportId: String) = pupilListReportDao.getById(reportId)

    fun deleteById(reportId: String) = pupilListReportDao.deleteById(reportId)

    fun updateReportName(reportId: String, reportName: String) =
        pupilListReportDao.updateReportName(reportId, reportName)

    override fun generateReportDocument(dto: AbstractReportDto): String? {
        if (dto !is PupilListReportDto) {
            assert(false) { "Parameter \"dto\" must be of type ${PupilListReportDto::class.java} " }
        }
        val document = Document()
        val fileName = dto.reportName ?: "Report${System.currentTimeMillis()}"
        val filePath =
            "${Environment.getExternalStorageDirectory()}${PhotoLoaderUtils.REPORTS_DIRECTORY}/$fileName.pdf"
        PdfWriter.getInstance(document, FileOutputStream(filePath))
        document.open()

        val pupilListReportCreator = PupilListReportCreator(pupilDao)

        try {
            pupilListReportCreator.createReport(document, dto, context)
            Toast.makeText(
                context,
                "${context.getString(R.string.ui_report_saved_to)}: $filePath",
                Toast.LENGTH_SHORT
            ).show()
            document.close()
            return filePath
        } catch (e: Exception) {
            Toast.makeText(
                context,
                context.getString(R.string.ui_report_was_not_saved),
                Toast.LENGTH_SHORT
            ).show()
            document.close()
            return null
        }
    }
}