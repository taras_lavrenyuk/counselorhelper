package com.lavreniuk.counselorhelper.reports.itemviews

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.models.entities.Pupil
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import kotlinx.android.synthetic.main.excluded_element_item_view.view.*

class ExcludedPupilItemView(
    context: Context,
    pupil: Pupil,
    removePupilFromExcludedList: (Pupil) -> Unit,
    openPupilActivity: (Pupil) -> Unit
) : LinearLayout(context) {

    init {
        View.inflate(context, R.layout.excluded_element_item_view, this)
        excluded_element_name.text = StringTransformationUtils.getFullNameString(pupil, false)

        excluded_element_name.setOnClickListener {
            openPupilActivity(pupil)
        }

        delete_excluded_element_image_view.setOnClickListener {
            removePupilFromExcludedList(pupil)
        }
    }
}