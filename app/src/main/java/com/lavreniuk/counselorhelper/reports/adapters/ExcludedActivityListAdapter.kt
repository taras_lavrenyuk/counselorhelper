package com.lavreniuk.counselorhelper.reports.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.lavreniuk.counselorhelper.models.entities.ProgramActivity
import com.lavreniuk.counselorhelper.reports.itemviews.ExcludedActivityItemView


class ExcludedActivityListAdapter(
    private val context: Context,
    private val activities: List<ProgramActivity>,
    private val removeActivityFromExcludedList: (ProgramActivity) -> Unit
) : BaseAdapter() {

    override fun getCount(): Int {
        return activities.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Any {
        return activities[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return ExcludedActivityItemView(
            context,
            activities[position],
            removeActivityFromExcludedList
        )
    }
}