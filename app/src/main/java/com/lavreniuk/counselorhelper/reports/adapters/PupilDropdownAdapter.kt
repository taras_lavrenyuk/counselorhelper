package com.lavreniuk.counselorhelper.reports.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.models.entities.Pupil
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils


class PupilDropdownAdapter(
    context: Context,
    private var initPupilList: List<Pupil>,
    private var suggestionList: List<Pupil> = emptyList()
) : ArrayAdapter<Pupil>(context, R.layout.simple_dropdown_item_view, R.id.simple_dropdown_textview, suggestionList) {

    private val pupilFilter: ListFilter = ListFilter()

    override fun getCount() = suggestionList.size

    override fun getItem(position: Int) = suggestionList[position]

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(parent.context).inflate(
            R.layout.simple_dropdown_item_view,
            parent,
            false
        )
        view.findViewById<TextView>(R.id.simple_dropdown_textview).text =
            StringTransformationUtils.getFullNameString(getItem(position), false)
        return view
    }

    override fun getFilter(): Filter {
        return pupilFilter
    }

    inner class ListFilter : Filter() {

        private val lock = Any()

        override fun performFiltering(searchString: CharSequence?): FilterResults {
            val filterResults = FilterResults()

            if (searchString.isNullOrBlank()) {
                synchronized(lock) {
                    filterResults.values = mutableListOf<String>()
                    filterResults.count = 0
                }
            } else {
                val prefixes = searchString.toString().split(" ")
                var filteredPupilsSet = emptySet<Pupil>()
                for (prefix in prefixes) {
                    val list = initPupilList.filter {
                        it.firstName.startsWith(prefix, true) ||
                                (it.secondName != null && it.secondName!!.startsWith(prefix, true)) ||
                                (it.lastName != null && it.lastName!!.startsWith(prefix, true))
                    }.toList()
                    filteredPupilsSet = filteredPupilsSet.plus(list)
                }
                filterResults.values =
                    filteredPupilsSet.toList().sortedWith(compareBy(Pupil::lastName, Pupil::firstName))
                filterResults.count = filteredPupilsSet.size
            }
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            suggestionList = if (results?.values != null) {
                results.values as List<Pupil>
            } else {
                emptyList()
            }
            if (results != null && results.count > 0) {
                notifyDataSetChanged()
            } else {
                notifyDataSetInvalidated()
            }
        }

    }
}