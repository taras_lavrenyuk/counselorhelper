package com.lavreniuk.counselorhelper.reports.models.dtos

import java.util.*

class ProgramReportDto(
    var includeAllActivities: Boolean = true,
    val includeProgramDescription: Boolean = true,
    val includeTime: Boolean = true,
    var includeNotes: Boolean = true,
    var includeTasks: Boolean = true,
    var includeTodoTasks: Boolean = true,
    var includeDoneTasks: Boolean = false,
    var date: Date? = null,
    val excludedActivityIds: List<String> = emptyList(),
    reportName: String? = null
) : AbstractReportDto(reportName)