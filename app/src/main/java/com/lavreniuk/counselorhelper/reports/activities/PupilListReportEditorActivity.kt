package com.lavreniuk.counselorhelper.reports.activities

import android.Manifest
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.PupilActivity
import com.lavreniuk.counselorhelper.markerinterfaces.ReportEditorActivity
import com.lavreniuk.counselorhelper.models.entities.Pupil
import com.lavreniuk.counselorhelper.reports.adapters.ExcludedPupilAdapter
import com.lavreniuk.counselorhelper.reports.adapters.PupilDropdownAdapter
import com.lavreniuk.counselorhelper.reports.models.PupilListReport
import com.lavreniuk.counselorhelper.reports.models.dtos.PupilListReportDto
import com.lavreniuk.counselorhelper.reports.viewmodels.PupilListReportViewModel
import com.lavreniuk.counselorhelper.reports.viewmodels.ReportViewModel
import com.lavreniuk.counselorhelper.utils.FileUtils
import com.lavreniuk.counselorhelper.utils.PreferencesUtilsMethods
import com.lavreniuk.counselorhelper.utils.RequestCodes
import com.lavreniuk.counselorhelper.viewmodels.PupilViewModel
import kotlinx.android.synthetic.main.activity_pupil_list_report_editor.*


class PupilListReportEditorActivity : AppCompatActivity(),
    ReportEditorActivity {

    private val reportViewModel: ReportViewModel by lazy {
        ViewModelProviders.of(this).get(ReportViewModel::class.java)
    }
    private val pupilListReportViewModel: PupilListReportViewModel by lazy {
        ViewModelProviders.of(this).get(PupilListReportViewModel::class.java)
    }
    private val pupilViewModel: PupilViewModel by lazy {
        ViewModelProviders.of(this).get(PupilViewModel::class.java)
    }

    private lateinit var report: PupilListReport
    private lateinit var excludePupilListAdapter: ExcludedPupilAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pupil_list_report_editor)

        val intent = intent

        // setup report object
        val reportId = intent.extras?.getString(getString(R.string.intent_report_id))
        if (reportId == null) {
            // new report should be created
            val squadId = PreferencesUtilsMethods.getCurrentSquadId(this)
            val orderInList = reportViewModel.getOrderForNewReport(squadId!!)
            report = PupilListReport(orderInList, squadId)
            kotlin.run {
                reportViewModel.save(report)
            }
        } else {
            report = pupilListReportViewModel.getById(reportId)
        }

        // setup init data interface and behaviour
        report.reportName?.let { pupil_list_report_name_input_field.setText(it) }

        setSwitcher(include_all_pupils_switch, report.includeAllPupils)
        setSwitcher(include_age_switch, report.includeAge)
        setSwitcher(include_birthday_switch, report.includeBirthday)
        setSwitcher(include_params_switch, report.includeParams)
        setSwitcher(include_all_contacts_switch, report.includeAllContacts)
        setSwitcher(include_numbers_switch, report.includeNumber)
        setSwitcher(add_blanks_switch, report.addBlanks)

        pupil_list_report_exclude_pupil_layout.visibility =
            if (include_all_pupils_switch.isChecked) View.GONE else View.VISIBLE

        include_all_pupils_switch.setOnCheckedChangeListener { _, isChecked ->
            pupil_list_report_exclude_pupil_layout.visibility = if (isChecked) View.GONE else View.VISIBLE
        }

        include_all_contacts_switch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) include_numbers_switch.isChecked = false
        }
        include_numbers_switch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) include_all_contacts_switch.isChecked = false
        }

        // setup exclude list
        val pupilList = pupilViewModel.getPupilsByIds(report.excludedPupilIds)
        excludePupilListAdapter = ExcludedPupilAdapter(
            this,
            pupilList,
            { pupil ->
                pupilList.remove(pupil)
                excludePupilListAdapter.notifyDataSetChanged()
                report.excludedPupilIds.remove(pupil.pupilId)
            },
            { pupil ->
                val pupilActivityIntent = Intent(this, PupilActivity::class.java)
                pupilActivityIntent.putExtra(getString(R.string.intent_pupil_id), pupil.pupilId)
                startActivity(pupilActivityIntent)
            }
        )
        pupil_list_report_exclude_pupil_list.adapter = excludePupilListAdapter

        // setup AutoCompleteTextView
        val excludePupilDropdownAdapter = PupilDropdownAdapter(
            context = this,
            initPupilList = pupilViewModel.getPupilsBySquadIdOrderByLastName(
                PreferencesUtilsMethods.getCurrentSquadId(
                    this
                )!!
            )
        )
        pupil_list_report_exclude_pupil_text_view.setAdapter(excludePupilDropdownAdapter)
        pupil_list_report_exclude_pupil_text_view.threshold = 1
        pupil_list_report_exclude_pupil_text_view.setOnItemClickListener { parent, _, position, _ ->
            val pupil = parent.getItemAtPosition(position)
            if (pupil is Pupil && !pupilList.contains(pupil)) {
                pupilList.add(pupil)
                pupilList.sortedWith(compareBy(Pupil::lastName, Pupil::firstName))
                excludePupilListAdapter.notifyDataSetChanged()
                report.excludedPupilIds.add(pupil.pupilId)
            }
            pupil_list_report_exclude_pupil_text_view.setText("")
        }

        pupil_list_report_generate_button.setOnClickListener {
            val reportName =
                pupil_list_report_name_input_field.text.let { if (it.isNullOrBlank()) null else it.toString() }
            if (needWriteExternalStoragePermission()) {
                val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                requestPermissions(permissions, RequestCodes.REQUEST_CODE_WRITE_STORAGE)
            } else {
                pupilListReportViewModel.generateReportDocument(getDtoObject())?.let {
                    report.path = it
                    updateReport(reportName)
                }
            }
        }

        pupil_list_report_view_button.setOnClickListener {
            report.path?.let {
                FileUtils.openPdf(it, this)
                return@setOnClickListener
            }
            Toast.makeText(this, getString(R.string.ui_generate_report_first), Toast.LENGTH_SHORT).show()
        }
    }

    override fun getDtoObject(): PupilListReportDto {
        return PupilListReportDto(
            includeAllPupils = include_all_pupils_switch.isChecked,
            includeAge = include_age_switch.isChecked,
            includeAllContacts = include_all_contacts_switch.isChecked,
            includeNumber = include_numbers_switch.isChecked,
            includeBirthday = include_birthday_switch.isChecked,
            includeParams = include_params_switch.isChecked,
            addBlanks = add_blanks_switch.isChecked,
            excludedPupilIds = report.excludedPupilIds,
            reportName = pupil_list_report_name_input_field.text.let { if (it.isNullOrBlank()) null else it.toString() }
        )
    }

    private fun needWriteExternalStoragePermission(): Boolean =
        Build.VERSION.SDK_INT > Build.VERSION_CODES.M
                && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val reportName =
            pupil_list_report_name_input_field.text.let { if (it.isNullOrBlank()) null else it.toString() }
        return if (reportName.isNullOrBlank()) {
            Toast.makeText(this, getString(R.string.ui_report_name_cannot_be_empty), Toast.LENGTH_SHORT).show()
            true
        } else {
            updateReport(reportName)
            super.onOptionsItemSelected(item)
        }
    }

    override fun updateReport(reportName: String?) {
        report.reportName = reportName
        report.includeAllPupils = include_all_pupils_switch.isChecked
        report.includeAge = include_age_switch.isChecked
        report.includeAllContacts = include_all_contacts_switch.isChecked
        report.includeNumber = include_numbers_switch.isChecked
        report.includeBirthday = include_birthday_switch.isChecked
        report.includeParams = include_params_switch.isChecked
        report.addBlanks = add_blanks_switch.isChecked
        reportViewModel.update(report)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            RequestCodes.REQUEST_CODE_WRITE_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(this, "Permission denied...!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}