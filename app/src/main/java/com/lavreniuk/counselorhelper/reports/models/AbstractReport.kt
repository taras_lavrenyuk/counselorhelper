package com.lavreniuk.counselorhelper.reports.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.lavreniuk.counselorhelper.enums.ReportType
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import java.util.*

@Entity(tableName = "abstract_reports")
open class AbstractReport() {
    @PrimaryKey
    var reportId: String = UtilsMethods.generateId()
    var reportName: String? = null
    var squadId: String? = null
    var createdAt: Date = Date()
    var path: String? = null
    var reportType: ReportType? = null
    var orderInList: Long = 0

    @Ignore
    constructor(reportName: String, squadId: String) : this() {
        this.reportName = reportName
        this.squadId = squadId
    }

}