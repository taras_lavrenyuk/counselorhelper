package com.lavreniuk.counselorhelper.reports.reportscreators

import android.content.Context
import com.itextpdf.text.Document
import com.itextpdf.text.Paragraph
import com.lavreniuk.counselorhelper.reports.models.dtos.AbstractReportDto

interface AbstractReportCreator {

    fun createReport(document: Document, dto: AbstractReportDto, context: Context): Document?

    fun addEmptyLine(paragraph: Paragraph, number: Int) {
        for (i in 0 until number) {
            paragraph.add(Paragraph(" "))
        }
    }
}