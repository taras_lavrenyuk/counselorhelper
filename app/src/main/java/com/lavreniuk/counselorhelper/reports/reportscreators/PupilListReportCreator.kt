package com.lavreniuk.counselorhelper.reports.reportscreators

import android.content.Context
import com.itextpdf.text.Chunk
import com.itextpdf.text.Document
import com.itextpdf.text.Paragraph
import com.itextpdf.text.Phrase
import com.itextpdf.text.pdf.draw.VerticalPositionMark
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.dao.PupilDao
import com.lavreniuk.counselorhelper.enums.ContactType
import com.lavreniuk.counselorhelper.reports.models.dtos.AbstractReportDto
import com.lavreniuk.counselorhelper.reports.models.dtos.PupilListReportDto
import com.lavreniuk.counselorhelper.reports.utils.ReportUtils
import com.lavreniuk.counselorhelper.utils.PreferencesUtilsMethods
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import com.lavreniuk.counselorhelper.utils.UtilsMethods

class PupilListReportCreator(
    private val pupilDao: PupilDao
) : AbstractReportCreator {

    override fun createReport(
        document: Document,
        dto: AbstractReportDto,
        context: Context
    ): Document? {
        if (dto is PupilListReportDto) {
            // set metadata
            dto.reportName?.let { document.addTitle(it) }

            // set title
            dto.reportName?.let {
                val title = Paragraph(it, ReportUtils.TITLE_FONT)
                title.alignment = Paragraph.ALIGN_CENTER
                document.add(title)
            }

            // add content
            val pupils = pupilDao.getPupilsWithParamsAndContacts(
                squadId = "%${PreferencesUtilsMethods.getCurrentSquadId(context)}%",
                excludedPupilIds = if (dto.includeAllPupils) emptyList() else dto.excludedPupilIds
            )

            for (pupil in pupils) {
                if (pupil.pupil == null) {
                    continue
                }
                val pupilParagraph = Paragraph()
                addEmptyLine(pupilParagraph, 1)
                val namePhrase = Phrase()
                namePhrase.add(
                    Chunk(
                        StringTransformationUtils.getFullNameString(pupil.pupil!!),
                        ReportUtils.PUPIL_NAME_FONT
                    )
                )

                if (dto.includeBirthday
                    && pupil.pupil!!.birthday != null
                    && pupil.pupil!!.birthday != UtilsMethods.getDefaultDate()
                ) {
                    namePhrase.add(
                        Chunk(
                            " ${StringTransformationUtils.fromDateToString(pupil.pupil!!.birthday!!)}",
                            ReportUtils.PUPIL_AGE_BIRTHDAY_FONT
                        )
                    )
                }
                if (dto.includeAge
                    && pupil.pupil!!.birthday != null
                    && pupil.pupil!!.birthday != UtilsMethods.getDefaultDate()
                ) {
                    namePhrase.add(
                        Chunk(
                            " (${StringTransformationUtils.getAgeFromBirthday(pupil.pupil!!.birthday!!)} ${context.getString(
                                R.string.ui_years_old
                            )})",
                            ReportUtils.PUPIL_AGE_BIRTHDAY_FONT
                        )
                    )
                }
                // add blanks or checkboxes
                if (dto.addBlanks) {
                    namePhrase.add(Chunk(VerticalPositionMark()))
                    namePhrase.add("_____")
                }
                pupilParagraph.add(namePhrase)

                // add contacts or numbers
                if (dto.includeNumber) {
                    for (contact in pupil.contacts) {
                        if (contact.contactType == ContactType.phone) {
                            val numberParagraph = Paragraph()
                            numberParagraph.add(
                                Phrase(
                                    StringTransformationUtils.getContactStringForReport(
                                        contact,
                                        context
                                    ),
                                    ReportUtils.PARAM_FONT
                                )
                            )
                            pupilParagraph.add(numberParagraph)
                            break
                        }
                    }
                }
                if (dto.includeAllContacts) {
                    val contactsParagraph = Paragraph()
                    contactsParagraph.add(
                        Phrase(
                            StringTransformationUtils.getContactsStringForReport(
                                pupil.contacts,
                                context
                            ),
                            ReportUtils.PARAM_FONT
                        )
                    )
                    pupilParagraph.add(contactsParagraph)
                }
                if (dto.includeParams) {
                    val paramsParagraph = Paragraph()
                    paramsParagraph.add(
                        Phrase(
                            StringTransformationUtils.getParamsStringForReport(pupil.params),
                            ReportUtils.PARAM_FONT
                        )
                    )
                    pupilParagraph.add(paramsParagraph)
                }
                document.add(pupilParagraph)
            }
            return document
        }
        throw Exception("The dto object is of wrong type.")
    }

}