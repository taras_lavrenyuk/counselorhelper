package com.lavreniuk.counselorhelper.reports.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.lavreniuk.counselorhelper.models.entities.Pupil
import com.lavreniuk.counselorhelper.reports.itemviews.ExcludedPupilItemView


class ExcludedPupilAdapter(
    private val context: Context,
    private val pupils: List<Pupil>,
    private val removePupilFromExcludedList: (Pupil) -> Unit,
    private val openPupilActivity: (Pupil) -> Unit
) : BaseAdapter() {

    override fun getCount(): Int {
        return pupils.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Any {
        return pupils[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return ExcludedPupilItemView(
            context,
            pupils[position],
            removePupilFromExcludedList,
            openPupilActivity
        )
    }
}