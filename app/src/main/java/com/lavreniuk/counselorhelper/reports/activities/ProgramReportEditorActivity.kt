package com.lavreniuk.counselorhelper.reports.activities

import android.Manifest
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.listeners.CustomOnTouchListenerForEditTextView
import com.lavreniuk.counselorhelper.listeners.DateInputOnClickListener
import com.lavreniuk.counselorhelper.markerinterfaces.ReportEditorActivity
import com.lavreniuk.counselorhelper.models.entities.ProgramActivity
import com.lavreniuk.counselorhelper.reports.adapters.ActivityDropdownAdapter
import com.lavreniuk.counselorhelper.reports.adapters.ExcludedActivityListAdapter
import com.lavreniuk.counselorhelper.reports.models.ProgramReport
import com.lavreniuk.counselorhelper.reports.models.dtos.ProgramReportDto
import com.lavreniuk.counselorhelper.reports.viewmodels.ProgramReportViewModel
import com.lavreniuk.counselorhelper.reports.viewmodels.ReportViewModel
import com.lavreniuk.counselorhelper.utils.FileUtils
import com.lavreniuk.counselorhelper.utils.PreferencesUtilsMethods
import com.lavreniuk.counselorhelper.utils.RequestCodes
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import com.lavreniuk.counselorhelper.viewmodels.ProgramActivityViewModel
import kotlinx.android.synthetic.main.activity_program_report_editor.*
import java.util.*

class ProgramReportEditorActivity : AppCompatActivity(), ReportEditorActivity {

    private val reportViewModel: ReportViewModel by lazy {
        ViewModelProviders.of(this).get(ReportViewModel::class.java)
    }
    private val programReportViewModel: ProgramReportViewModel by lazy {
        ViewModelProviders.of(this).get(ProgramReportViewModel::class.java)
    }
    private val programActivityViewModel: ProgramActivityViewModel by lazy {
        ViewModelProviders.of(this).get(ProgramActivityViewModel::class.java)
    }

    private lateinit var report: ProgramReport
    private lateinit var excludeActivityListAdapter: ExcludedActivityListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_program_report_editor)

        val intent = intent

        // setup report object
        val reportId = intent.extras?.getString(getString(R.string.intent_report_id))
        if (reportId == null) {
            // new report should be created
            val squadId = PreferencesUtilsMethods.getCurrentSquadId(this)
            val orderInList = reportViewModel.getOrderForNewReport(squadId!!)
            report = ProgramReport(orderInList, squadId)
            kotlin.run {
                reportViewModel.save(report)
            }
        } else {
            report = programReportViewModel.getById(reportId)
        }

        // setup init data interface and behaviour
        report.reportName?.let { program_report_name_input_field.setText(it) }

        setSwitcher(include_all_activities_switch, report.includeAllActivities)
        setSwitcher(include_description_switch, report.includeProgramDescription)
        setSwitcher(include_time_switch, report.includeTime)
        setSwitcher(include_notes_switch, report.includeNotes)
        setSwitcher(include_tasks_switch, report.includeTasks)
        setSwitcher(include_todo_tasks_switch, report.includeTodoTasks)
        setSwitcher(include_done_tasks_switch, report.includeDoneTasks)
        report.date?.let { program_report_target_date_input_field.setText(StringTransformationUtils.fromDateToString(it)) }

        program_report_exclude_activities_layout.visibility =
            if (include_all_activities_switch.isChecked) View.GONE else View.VISIBLE

        include_all_activities_switch.setOnCheckedChangeListener { _, isChecked ->
            program_report_exclude_activities_layout.visibility = if (isChecked) View.GONE else View.VISIBLE
        }

        // set tasks checks logic: if either include_todo or include_done is checked => include_tasks has to be checked
        setUpTaskSwitchersLogic()

        // setup target date field
        program_report_target_date_input_field.setOnClickListener(DateInputOnClickListener(this))
        program_report_target_date_input_field.setOnTouchListener(CustomOnTouchListenerForEditTextView())
        program_report_remove_target_date_icon.setOnClickListener {
            program_report_target_date_input_field.setText("")
        }


        // setup exclude list
        val activitiesList = programActivityViewModel.getActivitiesByIds(report.excludedActivityIds)
        excludeActivityListAdapter = ExcludedActivityListAdapter(
            this,
            activitiesList
        ) { activity ->
            activitiesList.remove(activity)
            excludeActivityListAdapter.notifyDataSetChanged()
            report.excludedActivityIds.remove(activity.programActivityId)
        }
        program_report_exclude_program_list.adapter = excludeActivityListAdapter

        // setup AutoCompleteTextView
        val excludeActivityDropdownAdapter = ActivityDropdownAdapter(
            context = this,
            initActivityList = programActivityViewModel.getProgramActivitiesSortedByOrderInList(
                PreferencesUtilsMethods.getCurrentSquadId(this)!!
            )
        )
        program_report_exclude_activity_text_view.setAdapter(excludeActivityDropdownAdapter)
        program_report_exclude_activity_text_view.threshold = 1
        program_report_exclude_activity_text_view.setOnItemClickListener { parent, _, position, _ ->
            val programActivity = parent.getItemAtPosition(position)
            if (programActivity is ProgramActivity && !activitiesList.contains(programActivity)) {
                activitiesList.add(programActivity)
                activitiesList.sortedWith(compareBy(ProgramActivity::orderInList))
                excludeActivityListAdapter.notifyDataSetChanged()
                report.excludedActivityIds.add(programActivity.programActivityId)
            }
            program_report_exclude_activity_text_view.setText("")
        }

        program_report_generate_button.setOnClickListener {
            val reportName =
                program_report_name_input_field.text.let { if (it.isNullOrBlank()) null else it.toString() }
            if (needWriteExternalStoragePermission()) {
                val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                requestPermissions(permissions, RequestCodes.REQUEST_CODE_WRITE_STORAGE)
            } else {
                programReportViewModel.generateReportDocument(getDtoObject())?.let {
                    report.path = it
                    updateReport(reportName)
                }
            }
        }

        program_report_view_button.setOnClickListener {
            report.path?.let {
                FileUtils.openPdf(it, this)
                return@setOnClickListener
            }
            Toast.makeText(this, getString(R.string.ui_generate_report_first), Toast.LENGTH_SHORT).show()
        }
    }

    override fun getDtoObject(): ProgramReportDto {
        return ProgramReportDto(
            includeAllActivities = include_all_activities_switch.isChecked,
            includeProgramDescription = include_description_switch.isChecked,
            includeNotes = include_notes_switch.isChecked,
            includeTasks = include_tasks_switch.isChecked,
            includeTodoTasks = include_todo_tasks_switch.isChecked,
            includeDoneTasks = include_done_tasks_switch.isChecked,
            excludedActivityIds = report.excludedActivityIds,
            date = getProgramReportTargetDate(),
            reportName = program_report_name_input_field.text.run { if (this.isNullOrBlank()) null else this.toString() }
        )
    }

    private fun getProgramReportTargetDate(): Date? {
        return program_report_target_date_input_field.text.toString().run {
            if (!StringTransformationUtils.isCorrectDateString(this)) null
            else StringTransformationUtils.fromStringToDate(this)
        }
    }

    override fun updateReport(reportName: String?) {
        report.reportName = reportName
        report.includeAllActivities = include_all_activities_switch.isChecked
        report.includeProgramDescription = include_description_switch.isChecked
        report.includeNotes = include_notes_switch.isChecked
        report.includeTasks = include_tasks_switch.isChecked
        report.includeTodoTasks = include_todo_tasks_switch.isChecked
        report.includeDoneTasks = include_done_tasks_switch.isChecked
        report.date = getProgramReportTargetDate()
        report.includeTime = include_time_switch.isChecked
        reportViewModel.update(report)
    }

    private fun setUpTaskSwitchersLogic() {
        include_tasks_switch.setOnCheckedChangeListener { _, isChecked ->
            if (!isChecked) {
                include_todo_tasks_switch.isChecked = false
                include_done_tasks_switch.isChecked = false
            } else {
                if (!include_todo_tasks_switch.isChecked && !include_done_tasks_switch.isChecked) {
                    include_todo_tasks_switch.isChecked = true
                }
            }
        }
        include_todo_tasks_switch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                include_tasks_switch.isChecked = true
            } else {
                if (!include_done_tasks_switch.isChecked) {
                    include_tasks_switch.isChecked = false
                }
            }
        }
        include_done_tasks_switch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                include_tasks_switch.isChecked = true
            } else {
                if (!include_todo_tasks_switch.isChecked) {
                    include_tasks_switch.isChecked = false
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val reportName =
            program_report_name_input_field.text.let { if (it.isNullOrBlank()) null else it.toString() }
        return if (reportName.isNullOrBlank()) {
            Toast.makeText(this, getString(R.string.ui_report_name_cannot_be_empty), Toast.LENGTH_SHORT).show()
            true
        } else {
            updateReport(reportName)
            super.onOptionsItemSelected(item)
        }
    }

    private fun needWriteExternalStoragePermission(): Boolean =
        Build.VERSION.SDK_INT > Build.VERSION_CODES.M
                && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            RequestCodes.REQUEST_CODE_WRITE_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(this, "Permission denied...!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}
