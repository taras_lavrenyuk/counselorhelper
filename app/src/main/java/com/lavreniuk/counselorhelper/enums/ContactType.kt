package com.lavreniuk.counselorhelper.enums

import com.lavreniuk.counselorhelper.R

enum class ContactType {

    phone {
        override fun getIconResourceId() = R.drawable.ic_phone_icon
        override fun getStringResourceId() = R.string.ui_phone
    },
    email {
        override fun getIconResourceId() = R.drawable.ic_email_icon
        override fun getStringResourceId() = R.string.ui_email
    },
    telegram {
        override fun getIconResourceId() = R.drawable.ic_telegram_icon
        override fun getStringResourceId() = R.string.ui_telegram
    },
    instagram {
        override fun getIconResourceId() = R.drawable.ic_instagram_icon
        override fun getStringResourceId() = R.string.ui_instagram
    },
    facebook {
        override fun getIconResourceId() = R.drawable.ic_facebook_icon
        override fun getStringResourceId() = R.string.ui_facebook
    },
    vk {
        override fun getIconResourceId() = R.drawable.ic_vk_icon
        override fun getStringResourceId() = R.string.ui_vk
    },
    linkedin {
        override fun getIconResourceId() = R.drawable.ic_linkedin_icon
        override fun getStringResourceId() = R.string.ui_linkedin

    },
    skype {
        override fun getIconResourceId() = R.drawable.ic_skype_icon
        override fun getStringResourceId() = R.string.ui_skype
    },
    twitter {
        override fun getIconResourceId() = R.drawable.ic_twitter_icon
        override fun getStringResourceId() = R.string.ui_twitter
    },
    google {
        override fun getIconResourceId() = R.drawable.ic_google_icon
        override fun getStringResourceId() = R.string.ui_google
    },
    other {
        override fun getIconResourceId() = R.drawable.ic_other_icon
        override fun getStringResourceId() = R.string.ui_other
    };

    abstract fun getIconResourceId(): Int

    abstract fun getStringResourceId(): Int

    companion object {

        fun getContactTypeArrayIndex(contactType: ContactType): Int {
            values().forEachIndexed { index, currentValue ->
                if (contactType == currentValue) return index
            }
            return 0
        }
    }

}