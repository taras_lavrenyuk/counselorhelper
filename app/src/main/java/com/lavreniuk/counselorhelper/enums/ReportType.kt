package com.lavreniuk.counselorhelper.enums

import android.content.Context
import com.lavreniuk.counselorhelper.R

enum class ReportType {

    PupilListReport {
        override fun getUiString(context: Context): String = context.getString(R.string.ui_pupil_list_report)
    },
    ProgramReport {
        override fun getUiString(context: Context): String = context.getString(R.string.ui_program_report)
    },
    Report {
        override fun getUiString(context: Context): String = context.getString(R.string.ui_report)
    };

    abstract fun getUiString(context: Context): String

}