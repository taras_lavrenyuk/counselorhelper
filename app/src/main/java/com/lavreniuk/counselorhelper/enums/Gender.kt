package com.lavreniuk.counselorhelper.enums

enum class Gender {

    Male,
    Female,
    Other;

}