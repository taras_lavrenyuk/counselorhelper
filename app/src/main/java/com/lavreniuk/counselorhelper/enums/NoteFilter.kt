package com.lavreniuk.counselorhelper.enums

enum class NoteFilter {

    All,
    Done,
    Todo,
    Notes;
}