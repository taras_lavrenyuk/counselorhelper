package com.lavreniuk.counselorhelper.enums

enum class NoteOrTask {

    Note,
    Task;

}