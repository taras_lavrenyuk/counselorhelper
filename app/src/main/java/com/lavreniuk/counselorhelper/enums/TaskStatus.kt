package com.lavreniuk.counselorhelper.enums

enum class TaskStatus {

    Todo,
    Done;

}
