package com.lavreniuk.counselorhelper.utils

object TagsForLogs {

    const val AVATAR_SELECTING = "Avatar"
    const val SAVING_IMAGE = "Image saving"
    const val DELETING_FILE = "File delete"

}