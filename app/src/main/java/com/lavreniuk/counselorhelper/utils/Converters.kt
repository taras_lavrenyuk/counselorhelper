package com.lavreniuk.counselorhelper.utils

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lavreniuk.counselorhelper.enums.*
import com.lavreniuk.counselorhelper.models.dtos.Time
import java.util.*

class Converters {

    @TypeConverter
    fun fromStringToDate(value: String?): Date? = value?.let { StringTransformationUtils.fromStringToDate(it) }

    @TypeConverter
    fun fromDateToString(date: Date?): String? = date?.let { StringTransformationUtils.fromDateToString(it) }

    @TypeConverter
    fun genderConverter(gender: Gender?): String = gender.toString()

    @TypeConverter
    fun fromStringToGenderType(genderTypeString: String): Gender = Gender.valueOf(genderTypeString)

    @TypeConverter
    fun contactTypeConverter(contactType: ContactType?): String = contactType.toString()

    @TypeConverter
    fun fromStringToContactTypeType(contactTypeString: String): ContactType =
        ContactType.valueOf(contactTypeString)

    @TypeConverter
    fun fromStringsListToJsonString(stringList: List<String>): String = Gson().toJson(stringList)

    @TypeConverter
    fun fromJsonStringToStringsList(jsonString: String): List<String> =
        Gson().fromJson(jsonString, object : TypeToken<List<String>>() {}.type)

    @TypeConverter
    fun fromTimeToString(time: Time): String = time.toString()

    @TypeConverter
    fun fromStringToTimeObject(timeString: String): Time = Time.fromStringToObject(timeString)

    @TypeConverter
    fun fromNoteStatusToString(taskStatus: TaskStatus?): String? = taskStatus?.let { it.toString() }

    @TypeConverter
    fun fromStringToNoteStatus(noteStatusString: String?): TaskStatus? =
        noteStatusString?.let { TaskStatus.valueOf(it) }

    @TypeConverter
    fun fromNoteOrTaskToString(noteOrTask: NoteOrTask): String = noteOrTask.toString()

    @TypeConverter
    fun fromStringToNoteOrTask(noteOrTaskString: String): NoteOrTask = NoteOrTask.valueOf(noteOrTaskString)


    @TypeConverter
    fun fromReportTypeToString(reportType: ReportType?): String = reportType.toString()

    @TypeConverter
    fun fromStringToReportType(reportTypeString: String): ReportType = ReportType.valueOf(reportTypeString)

}