package com.lavreniuk.counselorhelper.utils

import android.content.Context
import android.content.Intent
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.enums.ContactType
import com.lavreniuk.counselorhelper.enums.NoteOrTask
import com.lavreniuk.counselorhelper.enums.TaskStatus
import com.lavreniuk.counselorhelper.models.dtos.NameDto
import com.lavreniuk.counselorhelper.models.dtos.Time
import com.lavreniuk.counselorhelper.models.entities.*
import java.util.*

object IntentBoxingUtils {

    fun extractContactInfoFromIntent(intent: Intent?, context: Context): Contact {
        val personId = intent?.extras?.get(context.getString(R.string.intent_person_id)).toString()
        val contactType =
            ContactType.valueOf(intent?.extras?.get(context.getString(R.string.intent_contact_type)).toString())
        val contactValue = intent?.extras?.get(context.getString(R.string.intent_contact_value)).toString()
        val contactId = intent?.extras?.getString(context.getString(R.string.intent_contact_id), "empty").toString()


        return Contact(
            if (contactId == "empty") UUID.randomUUID().toString() else contactId,
            contactType,
            contactValue,
            personId
        )
    }

    fun extractParamInfoFromIntent(
        intent: Intent?,
        context: Context,
        getOrderInList: (squadId: String) -> Long
    ): Param {
        val entityId = intent?.extras?.get(context.getString(R.string.intent_entity_id)).toString()
        val paramName = intent?.extras?.get(context.getString(R.string.intent_param_name)).toString()
        val paramValue = intent?.extras?.get(context.getString(R.string.intent_param_value)).toString()
        val paramId =
            intent?.extras?.getString(context.getString(R.string.intent_param_id), "empty").toString()
        var orderInList =
            intent?.extras?.getLong(context.getString(R.string.intent_order_in_list), -1)!!.toLong()
        if (orderInList.compareTo(-1) == 0) {
            orderInList = getOrderInList(entityId)
        }

        return Param(
            if (paramId == "empty") UtilsMethods.generateId() else paramId,
            entityId,
            paramValue,
            paramName,
            orderInList
        )
    }

    fun extractProgramActivityFromIntent(
        data: Intent?,
        context: Context,
        getOrderInList: () -> Long
    ): ProgramActivity {
        val programActivityId =
            data?.extras?.getString(context.getString(R.string.intent_program_activity_id), "empty").toString()
        val programActivityName =
            data?.extras?.getString(context.getString(R.string.intent_program_activity_name)).toString()
        val programActivityDescription =
            data?.extras?.getString(context.getString(R.string.intent_program_activity_description)).toString()
        val startTime =
            Time.fromStringToObject(data?.extras?.getString(context.getString(R.string.intent_program_activity_start_time)).toString())
        val endTime =
            Time.fromStringToObject(data?.extras?.getString(context.getString(R.string.intent_program_activity_end_time)).toString())
        val squadId = PreferencesUtilsMethods.getCurrentSquadId(context)!!
        var orderInList =
            data?.extras?.getLong(context.getString(R.string.intent_order_in_list), -1)!!.toLong()
        if (orderInList.compareTo(-1) == 0) {
            orderInList = getOrderInList()
        }

        return ProgramActivity(
            if (programActivityId == "empty") UtilsMethods.generateId() else programActivityId,
            programActivityName,
            programActivityDescription,
            startTime,
            endTime,
            squadId,
            orderInList
        )
    }

    /*
    * Returns Relative object without [Relative#relativeOf] field.
    */
    fun extractRelativeFromIntent(intent: Intent?, context: Context): Relative {
        val relativeId = intent?.extras?.getString(context.getString(R.string.intent_relative_id), "empty").toString()
        val relativeConnection = intent?.extras?.get(context.getString(R.string.intent_relative_connection)).toString()
        val relativeContact = intent?.extras?.get(context.getString(R.string.intent_relative_contact)).toString()
        val relativeName = intent?.getParcelableExtra<NameDto>(context.getString(R.string.intent_relative_name))!!

        return Relative(
            if (relativeId == "empty") UtilsMethods.generateId() else relativeId,
            relativeName.firstName,
            relativeName.secondName,
            relativeName.lastName,
            relativeConnection,
            relativeContact
        )
    }

    fun extractNoteFromIntent(
        intent: Intent?,
        context: Context,
        getOrderInList: () -> Long
    ): Note {
        val noteId = intent?.extras?.getString(context.getString(R.string.intent_note_id), "empty").toString()
        val activityName = intent?.extras?.get(context.getString(R.string.intent_note_activity_name))?.toString()
        val noteText = intent?.extras?.get(context.getString(R.string.intent_note_text)).toString()
        val noteTargetDate = intent?.extras?.get(context.getString(R.string.intent_note_target_date)).toString()
        val noteOrTaskExtra = intent?.getSerializableExtra(context.getString(R.string.intent_note_or_task))
        val noteOrTask = if (noteOrTaskExtra != null) {
            NoteOrTask.valueOf(noteOrTaskExtra.toString())
        } else {
            NoteOrTask.Note
        }
        val noteStatusExtra = intent?.getSerializableExtra(context.getString(R.string.intent_task_status))
        val noteStatus: TaskStatus?
        if (noteOrTask == NoteOrTask.Task) {
            noteStatus = if (noteStatusExtra != null) {
                TaskStatus.valueOf(noteStatusExtra.toString())
            } else {
                TaskStatus.Todo
            }
        } else {
            noteStatus = null
        }

        var orderInList =
            intent?.extras?.getLong(context.getString(R.string.intent_order_in_list), -1)!!.toLong()
        if (orderInList.compareTo(-1) == 0) {
            orderInList = getOrderInList()
        }

        return Note(
            if (noteId == "empty") UtilsMethods.generateId() else noteId,
            noteText,
            if (noteTargetDate.isBlank()) null else StringTransformationUtils.fromStringToDate(noteTargetDate),
            noteOrTask,
            noteStatus,
            null,
            activityName,
            PreferencesUtilsMethods.getCurrentSquadId(context)!!,
            orderInList
        )
    }
}