package com.lavreniuk.counselorhelper.utils

import java.util.regex.Pattern

object Validators {

    private const val emailRegex = ("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
            + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
            + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
            + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$")

    private const val instagramUsernameRegex =
        ("([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\\.(?!\\.))){0,28}(?:[A-Za-z0-9_]))?)")

    private const val skypeUsernameRegex = ("[a-zA-Z][a-zA-Z0-9\\.,\\-_]{5,31}")

    private const val facebookUsernameRegex = ("/^[a-z\\d.]{5,}\$/i")

    private const val facebookLinkRegex =
        ("(?:https?:\\/\\/)?(?:www\\.)?facebook\\.com\\/.(?:(?:\\w)*#!\\/)?(?:pages\\/)?(?:[\\w\\-]*\\/)*([\\w\\-\\.]*)")

    private const val twitterUsernameRegex = ("/^[A-Za-z0-9_]+\$/")

    private const val linkedinUsernameRegex =
        ("(http(s)?:\\/\\/([\\w]+\\.)?linkedin\\.com\\/in\\/[A-Za-z0-9-_]+\\/?)|([A-Za-z0-9-_]+)")

    private const val telegramUsernameRegex = ("[A-Za-z0-9_]{5,32}")

    private const val phoneNumberRegex = ("^\\+?([0-9]?){6,14}")

    fun isEmailValid(email: String): Boolean {
        return Pattern.compile(emailRegex).matcher(email).matches()
    }

    fun isInstagramUsernameValid(instagramUsername: String): Boolean {
        return Pattern.compile(instagramUsernameRegex).matcher(instagramUsername).matches()
    }

    fun isSkypeUsernameValid(skypeUsername: String): Boolean {
        return Pattern.compile(skypeUsernameRegex).matcher(skypeUsername).matches()
    }

    fun isFacebookUsernameValid(facebookUsername: String): Boolean {
        return Pattern.compile(facebookUsernameRegex).matcher(facebookUsername).matches()
                || Pattern.compile(facebookLinkRegex).matcher(facebookUsername).matches()

    }

    fun isTwitterUsernameValid(twitterUsername: String): Boolean {
        return Pattern.compile(twitterUsernameRegex).matcher(twitterUsername).matches()
    }

    fun isLinkedinUsernameValid(linkedinUsername: String): Boolean {
        return Pattern.compile(linkedinUsernameRegex).matcher(linkedinUsername).matches()
    }

    fun isTelegramUsernameValid(telegramUsername: String): Boolean {
        return Pattern.compile(telegramUsernameRegex).matcher(telegramUsername).matches()
    }

    fun isPhoneNumberValid(phoneNumber: String): Boolean {
        return Pattern.compile(phoneNumberRegex).matcher(adjustPhoneNumber(phoneNumber)).matches()
    }

    private fun adjustPhoneNumber(phoneNumber: String): String {
        return phoneNumber
            .replace(" ", "")
            .replace("-", "")
            .replace("(", "")
            .replace(")", "")
    }


}