package com.lavreniuk.counselorhelper.utils

import android.content.Context
import android.preference.PreferenceManager
import com.lavreniuk.counselorhelper.R

object PreferencesUtilsMethods {

    fun getCurrentSquadId(context: Context): String? {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val defValue = "default"
        val currentSquadId = preferences.getString(context.getString(R.string.current_squad_id), defValue)
        return if (currentSquadId == defValue) null else currentSquadId
    }

    fun setCurrentSquadId(context: Context, squadId: String) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context).edit()
        preferences.putString(context.getString(R.string.current_squad_id), squadId)
        preferences.apply()
    }

    private fun removeCurrentSquadId(context: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context).edit()
        preferences.remove(context.getString(R.string.current_squad_id))
        preferences.apply()
    }

    fun getUserId(context: Context, firstCall: Boolean): String {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val defValue = "default"
        val userId = preferences.getString(context.getString(R.string.user_id), defValue)
        if (defValue == userId && firstCall) assert(false) { "UserId not found in Preferences!!!" }
        return userId!!
    }

    fun setUserId(context: Context, userId: String) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context).edit()
        preferences.putString(context.getString(R.string.user_id), userId)
        preferences.apply()
    }

    private fun removeUserId(context: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context).edit()
        preferences.remove(context.getString(R.string.user_id))
        preferences.apply()
    }

    fun clearData(context: Context) {
        removeUserId(context)
        removeCurrentSquadId(context)
    }
}