package com.lavreniuk.counselorhelper.utils

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.lavreniuk.counselorhelper.activities.AccountSettingsActivity
import java.util.*

object PermissionChecker {

    private const val SPLASH_TIME_OUT = 2000
    private const val TAG = "permission request"


    fun checkAndRequestPhonePermissions(activity: AppCompatActivity): Boolean {
        val phoneCallPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE)

        val listPermissionsNeeded = ArrayList<String>()

        if (phoneCallPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE)
        }
        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                activity,
                listPermissionsNeeded.toTypedArray(),
                RequestCodes.REQUEST_CODE_CALL_PHONE_PERMISSIONS
            )
            return false
        }
        return true
    }

    fun customOnRequestCallPhonePermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
        activity: AppCompatActivity
    ) {
        Log.d(TAG, "Permission callback called-------")
        when (requestCode) {
            RequestCodes.REQUEST_CODE_CALL_PHONE_PERMISSIONS -> {

                val perms = HashMap<String, Int>()
                // Initialize the map with both permissions
                perms[Manifest.permission.CALL_PHONE] = PackageManager.PERMISSION_GRANTED
                // Fill with actual results from user
                if (grantResults.isNotEmpty()) {
                    for (i in permissions.indices)
                        perms[permissions[i]] = grantResults[i]
                    // Check for both permissions
                    if (perms[Manifest.permission.CALL_PHONE] == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "call phone permission granted")
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d(TAG, "Some permissions are not granted ask again ")
                        // permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
                        // shouldShowRequestPermissionRationale will return true
                        // show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(
                                activity,
                                Manifest.permission.CALL_PHONE
                            )
                        ) {
                            showDialogOK(
                                "Service Permissions are required for this app",
                                DialogInterface.OnClickListener { _, which ->
                                    when (which) {
                                        DialogInterface.BUTTON_POSITIVE -> checkAndRequestPhonePermissions(activity)
                                        DialogInterface.BUTTON_NEGATIVE ->
                                            // proceed with logic by disabling the related features or quit the app.
                                            activity.finish()
                                    }
                                }, activity
                            )
                        } else {
                            explain(
                                "You need to give some mandatory permissions to continue. Do you want to go to app settings?",
                                activity
                            )
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }//permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                    }
                }
            }
        }

    }


    fun checkAndRequestCameraPermissions(activity: AppCompatActivity): Boolean {
        val cameraPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
        val writePermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)

        val listPermissionsNeeded = ArrayList<String>()

        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                activity,
                listPermissionsNeeded.toTypedArray(),
                RequestCodes.REQUEST_CODE_MULTIPLE_PERMISSIONS
            )
            return false
        }
        return true
    }

    fun customOnRequestCameraPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
        activity: AppCompatActivity
    ) {
        Log.d(TAG, "Permission callback called-------")
        when (requestCode) {
            RequestCodes.REQUEST_CODE_MULTIPLE_PERMISSIONS -> {

                val perms = HashMap<String, Int>()
                // Initialize the map with both permissions
                perms[Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                // Fill with actual results from user
                if (grantResults.isNotEmpty()) {
                    for (i in permissions.indices)
                        perms[permissions[i]] = grantResults[i]
                    // Check for both permissions
                    if (perms[Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                    ) {
                        Log.d(TAG, "camera & write external storage permission granted")
                        // process the normal flow
//                        val i = Intent(activity, AccountSettingsActivity::class.java)
//                        activity.startActivity(i)
//                        activity.finish()
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d(TAG, "Some permissions are not granted ask again ")
                        // permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
                        // shouldShowRequestPermissionRationale will return true
                        // show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(
                                activity,
                                Manifest.permission.CAMERA
                            )
                            || ActivityCompat.shouldShowRequestPermissionRationale(
                                activity,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            )
                        ) {
                            showDialogOK(
                                "Service Permissions are required for this app",
                                DialogInterface.OnClickListener { _, which ->
                                    when (which) {
                                        DialogInterface.BUTTON_POSITIVE -> checkAndRequestCameraPermissions(activity)
                                        DialogInterface.BUTTON_NEGATIVE ->
                                            // proceed with logic by disabling the related features or quit the app.
                                            activity.finish()
                                    }
                                }, activity
                            )
                        } else {
                            explain(
                                "You need to give some mandatory permissions to continue. Do you want to go to app settings?",
                                activity
                            )
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }//permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                    }
                }
            }
        }

    }

    private fun showDialogOK(
        message: String,
        okListener: DialogInterface.OnClickListener,
        activity: AppCompatActivity
    ) {
        AlertDialog.Builder(activity)
            .setMessage(message)
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", okListener)
            .create()
            .show()
    }

    private fun explain(msg: String, activity: AppCompatActivity) {
        val dialog = android.support.v7.app.AlertDialog.Builder(activity)
        dialog.setMessage(msg)
            .setPositiveButton("Yes") { _, _ ->
                //  permissionsclass.requestPermission(type,code);
                activity.startActivity(
                    Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:com.lavreniuk.counselorhelper.activities")
                    )
                )
            }
            .setNegativeButton("Cancel") { _, _ -> activity.finish() }
        dialog.show()
    }

}
