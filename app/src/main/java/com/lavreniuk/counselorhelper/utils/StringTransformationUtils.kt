package com.lavreniuk.counselorhelper.utils

import android.content.Context
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.models.entities.Contact
import com.lavreniuk.counselorhelper.models.entities.Param
import com.lavreniuk.counselorhelper.models.entities.PersonInfo
import com.lavreniuk.counselorhelper.models.entities.ProgramActivity
import java.util.*

object StringTransformationUtils {

    fun getFullNameString(person: PersonInfo, includeSecondName: Boolean = false): String {
        var result = person.firstName
        if (includeSecondName) {
            if (person.secondName != null) {
                result += " " + person.secondName
            }
        }
        if (person.lastName != null) {
            result += " " + person.lastName
        }
        return result
    }

    fun getAgeFromBirthday(birthdayDate: Date): Int = getDifferenceBetweenTwoDatesInYears(birthdayDate, Date())

    private fun getDifferenceBetweenTwoDatesInYears(date1: Date, date2: Date): Int =
        ((date2.time - date1.time) / 86400000 / 365).toInt()

    fun fromDateToString(date: Date): String {
        val day = date.date
        val month = date.month + 1
        val year = date.year + 1900
        return "$day/$month/$year"
    }

    fun fromStringToDate(text: String): Date {
        val dateArray = text.split("/")
        val calendar = Calendar.getInstance()
        calendar.set(dateArray[2].toInt(), dateArray[1].toInt() - 1, dateArray[0].toInt())
        return calendar.time
    }

    fun isCorrectDateString(dateString: String): Boolean {
        return dateString.matches(Regex("\\d{1,2}/\\d{1,2}/\\d{4}"))
    }

    fun getContactsStringForReport(contacts: List<Contact>, context: Context): String {
        val result = StringBuilder()
        for (contact in contacts) {
            result.append("${getContactStringForReport(contact, context)}, ")
        }
        return result.toString().trim().dropLast(1)
    }

    fun getContactStringForReport(contact: Contact, context: Context): String =
        "${context.getString(contact.contactType.getStringResourceId())}:${contact.value}"

    fun getParamsStringForReport(params: List<Param>): String {
        val result = StringBuilder()
        for (param in params) {
            result.append("${getParamStringForReport(param)}, ")
        }
        return result.toString().trim().dropLast(1)
    }

    private fun getParamStringForReport(param: Param): String =
        "${param.paramName}:${param.paramValue}"


    /**
     * @param [twoLines] is true if we need two lines time string: used in [ProgramActivityDragAdapter] when displaying list of activities
     * @param [twoLines] is false if we need one line time string: used for report creation for instance
     */
    fun getProgramActivityTime(programActivity: ProgramActivity, context: Context, twoLines: Boolean): String {
        // used when either startTime and endTime is not set
        val textTimeSeparator = if (twoLines) "\n" else " "
        // used when both startTime and endTime are set
        val timeSeparator = if (twoLines) "\n" else " - "
        return if (programActivity.endTime.toString().isEmpty()) {
            "${context.getString(R.string.ui_from)}$textTimeSeparator${programActivity.startTime}"
        } else if (programActivity.startTime.toString().isEmpty()) {
            "${context.getString(R.string.ui_until)}$textTimeSeparator${programActivity.endTime}"
        } else {
            "${programActivity.startTime}$timeSeparator${programActivity.endTime}"
        }
    }

}