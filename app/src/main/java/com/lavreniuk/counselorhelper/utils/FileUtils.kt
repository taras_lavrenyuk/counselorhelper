package com.lavreniuk.counselorhelper.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.support.v4.content.FileProvider
import android.util.Log
import android.widget.Toast
import com.lavreniuk.counselorhelper.BuildConfig
import com.lavreniuk.counselorhelper.R
import java.io.File

object FileUtils {

    fun deleteFile(path: String) {
        val fileToDelete = File(path)
        if (fileToDelete.exists()) {
            if (fileToDelete.delete()) {
                Log.d(TagsForLogs.DELETING_FILE, "File: \"$path\" deleted successfully")
            } else {
                Log.d(TagsForLogs.DELETING_FILE, "File: \"$path\" not deleted")
            }
        }
    }

    fun openPdf(path: String, context: Context) {
        val pdfReportFile = File(path)
        if (pdfReportFile.exists()) {
            val pdfIntent = Intent(Intent.ACTION_VIEW)
            val uri = FileProvider.getUriForFile(
                context,
                BuildConfig.APPLICATION_ID + ".provider",
                pdfReportFile
            )
            pdfIntent.setDataAndType(uri, "application/pdf")
            pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            pdfIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val chooserIntent = Intent.createChooser(pdfIntent, "Open File")
            try {
                context.startActivity(chooserIntent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(context, context.getString(R.string.ui_install_pdf_reader), Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(context, context.getString(R.string.ui_file_does_not_exit), Toast.LENGTH_LONG).show()
        }

    }

}