package com.lavreniuk.counselorhelper.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.lavreniuk.counselorhelper.activities.PupilActivity
import com.lavreniuk.counselorhelper.models.entities.Contact
import com.lavreniuk.counselorhelper.models.entities.PersonInfo
import com.lavreniuk.counselorhelper.models.entities.Squad
import com.lavreniuk.counselorhelper.viewmodels.SquadViewModel
import java.util.*


object UtilsMethods {

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun generateId(): String {
        return UUID.randomUUID().toString()
    }

    fun generateShortId(): String {
        return UUID.randomUUID().toString().substring(0..3)
    }

    fun getCurrentSquad(context: Context, squadViewModel: SquadViewModel): Squad {
        return squadViewModel.getSquad(PreferencesUtilsMethods.getCurrentSquadId(context)!!)!!
    }

    fun getDefaultDate(): Date {
        return Date(0)
    }

    fun dpToPx(dp: Int): Int {
        return (dp * Resources.getSystem().displayMetrics.density).toInt()
    }

    fun callNumber(contact: Contact, activity: AppCompatActivity) {
        if (PermissionChecker.checkAndRequestPhonePermissions(activity)) {
            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse("tel:${contact.value}")
            activity.startActivity(callIntent)
        }
    }
}
