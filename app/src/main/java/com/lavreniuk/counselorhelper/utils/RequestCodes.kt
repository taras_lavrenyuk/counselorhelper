package com.lavreniuk.counselorhelper.utils

object RequestCodes {

    const val REQUEST_CODE_NAME: Int = 1
    const val REQUEST_CODE_BIO: Int = 2
    const val REQUEST_CODE_BIRTHDAY: Int = 3
    const val REQUEST_CODE_MULTIPLE_PERMISSIONS = 4
    const val REQUEST_CODE_PHOTO_FROM_CAMERA = 5
    const val REQUEST_CODE_PHOTO_FROM_GALLERY = 6
    const val REQUEST_CODE_CALL_PHONE_PERMISSIONS = 7
    const val REQUEST_CODE_EDIT_CONTACT = 8
    const val REQUEST_CODE_SQUAD_NAME = 9
    const val REQUEST_CODE_PARAM_ADD = 10
    const val REQUEST_CODE_PARAM_EDIT = 11
    const val REQUEST_CODE_ENTER_PUPILS_NAME = 12
    const val REQUEST_CODE_GET_EXISTING_PUPIL_ID = 13
    const val REQUEST_CODE_GET_NEW_SQUAD_NAME = 14
    const val REQUEST_CODE_EDIT_RELATIVE = 15
    const val REQUEST_CODE_PROGRAM_ACTIVITY_ADD = 16
    const val REQUEST_CODE_PROGRAM_ACTIVITY_EDIT = 17
    const val REQUEST_CODE_NOTE_ADD = 18
    const val REQUEST_CODE_NOTE_EDIT = 19
    const val REQUEST_CODE_POSITION = 20
    const val REQUEST_CODE_WRITE_STORAGE = 21

}