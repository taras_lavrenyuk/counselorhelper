package com.lavreniuk.counselorhelper.watchers;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import com.lavreniuk.counselorhelper.models.dtos.Time;

import java.util.regex.Pattern;

public class TimeEntryTextWatcher implements TextWatcher {

    private String current = "";
    private final String hhmm = "hhmm";
    private Time time = new Time();
    private EditText inputField;

    private String lastCorrect = "";
    public static final String TIME_REGEX = "(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]";

    public TimeEntryTextWatcher(EditText inputField) {
        this.inputField = inputField;
    }

    @Override
    public void beforeTextChanged(CharSequence inputString, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence inputString, int start, int before, int count) {
        String current = inputString.toString();
        if (!isValidTimeFormat(current)) {
            inputField.setText(lastCorrect);
            inputField.setSelection(inputField.getText().length());
        } else {
            lastCorrect = current;
        }
    }

    private boolean isValidTimeFormat(@NonNull String curr) {
        return curr.equals("")
                || Pattern.matches("(0[0-9]|1[0-9]|2[0-3]|[0-9])", curr)
                || Pattern.matches("(0[0-9]|1[0-9]|2[0-3]|[0-9]):", curr)
                || Pattern.matches("(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5]", curr)
                || Pattern.matches(TIME_REGEX, curr);
    }

    @Override
    public void afterTextChanged(Editable inputString) {

    }
}
