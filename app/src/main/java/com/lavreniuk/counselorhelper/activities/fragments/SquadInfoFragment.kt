package com.lavreniuk.counselorhelper.activities.fragments

import android.app.AlertDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.SquadActivity
import com.lavreniuk.counselorhelper.activities.dialogs.ParamEditorActivity
import com.lavreniuk.counselorhelper.activities.dialogs.SetParamActivity
import com.lavreniuk.counselorhelper.itemviews.dragitemviewadapters.ParamDragAdapter
import com.lavreniuk.counselorhelper.models.entities.Param
import com.lavreniuk.counselorhelper.utils.*
import com.lavreniuk.counselorhelper.viewmodels.ParamViewModel
import com.lavreniuk.counselorhelper.viewmodels.SquadViewModel
import com.woxthebox.draglistview.DragListView
import kotlinx.android.synthetic.main.fragment_squad_info.*
import kotlinx.android.synthetic.main.fragment_squad_info.view.*
import java.io.IOException
import kotlin.concurrent.thread


class SquadInfoFragment : Fragment() {

    private val squadViewModel: SquadViewModel by lazy { ViewModelProviders.of(this).get(SquadViewModel::class.java) }

    private val paramViewModel: ParamViewModel by lazy {
        ViewModelProviders.of(this).get(ParamViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_squad_info, container, false)

        view.squad_name_textview.setOnClickListener {
            callSetParamActivity(
                RequestCodes.REQUEST_CODE_SQUAD_NAME,
                "Squad name",
                getSquadNameText()
            )
        }

        view.squad_avatar.setOnClickListener {
            if (PermissionChecker.checkAndRequestCameraPermissions(activity as SquadActivity)) {
                val avatarChangeDialog = AlertDialog.Builder(context)
                avatarChangeDialog.setTitle(getString(R.string.ui_select_action))
                val pictureDialogItems =
                    arrayOf(getString(R.string.ui_avatar_from_galery), getString(R.string.ui_avatar_from_camera))
                avatarChangeDialog.setItems(
                    pictureDialogItems
                ) { _, which ->
                    when (which) {
                        0 -> PhotoLoaderUtils.choosePhotoFromGallery(this)
                        1 -> PhotoLoaderUtils.takePhotoFromCamera(this)
                        else -> assert(false) { "Wrong argument" }
                    }
                }
                avatarChangeDialog.show()
            }
        }

        view.add_squad_param_button.setOnClickListener {
            val intent = Intent(activity as SquadActivity, ParamEditorActivity::class.java)
            intent.putExtra(getString(R.string.intent_add_or_edit), getString(R.string.intent_add))
            intent.putExtra(
                getString(R.string.intent_entity_id),
                PreferencesUtilsMethods.getCurrentSquadId(activity as SquadActivity)
            )
            startActivityForResult(intent, RequestCodes.REQUEST_CODE_PARAM_ADD)
        }

        return view
    }

    override fun onResume() {
        super.onResume()

        val squad = UtilsMethods.getCurrentSquad(context!!, squadViewModel)
        squad_name_textview.text = squad.squadName

        val image = PhotoLoaderUtils.getBitmapFromPath(squad.photo)
        if (image != null) squad_avatar.setImageBitmap(image)


        loadSquadParamsList()
    }

    private fun setUpSquadParamsList(squadParamsSortedByOrder: List<Param>) {

        val listAdapter = ParamDragAdapter(
            squadParamsSortedByOrder.toTypedArray(),
            this
        )


        val dragAndDropListener = object : DragListView.DragListListener {
            override fun onItemDragging(itemPosition: Int, x: Float, y: Float) {
            }

            override fun onItemDragStarted(position: Int) {
            }

            override fun onItemDragEnded(fromPosition: Int, toPosition: Int) {
                if (fromPosition != toPosition) {
                    thread(start = true) {
                        val getParamIdOnPosition =
                            { position: Int -> listAdapter.getParamIdOnPosition(position) }
                        if (fromPosition > toPosition) {
                            paramViewModel.updateParamsOrder(toPosition, fromPosition, getParamIdOnPosition)
                        } else {
                            paramViewModel.updateParamsOrder(fromPosition, toPosition, getParamIdOnPosition)
                        }
                    }
                }
            }
        }
        squad_params_draganddrop_list.setDragListListener(dragAndDropListener)
        squad_params_draganddrop_list.setLayoutManager(LinearLayoutManager(activity))

        squad_params_draganddrop_list.setAdapter(listAdapter, true)
        squad_params_draganddrop_list.setCanDragHorizontally(false)
    }

//    fun updateReportsOrder(
//        startPosition: Int,
//        endPosition: Int,
//        getParamIdOnPosition: (position: Int) -> String
//    ) {
//        for (position in startPosition..endPosition) {
//            paramViewModel.updateOrderInList(getParamIdOnPosition(position), position + 1)
//        }
//    }

    private fun loadSquadParamsList() {
        setUpSquadParamsList(paramViewModel.getEntityParams(PreferencesUtilsMethods.getCurrentSquadId(context!!)))
    }

    private fun getSquadNameText(): String {
        return squad_name_textview.text.toString()
    }

    private fun callSetParamActivity(
        requestCode: Int,
        infoLabelText: String,
        oldValue: String
    ) {
        val intent = Intent(activity as SquadActivity, SetParamActivity::class.java)
        intent.putExtra(getString(R.string.intent_old_value), oldValue)
        intent.putExtra(getString(R.string.intent_info_label_text), infoLabelText)
        intent.putExtra(getString(R.string.intent_request_code), requestCode)
        startActivityForResult(intent, requestCode)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == 0) return

        val currentSquad = UtilsMethods.getCurrentSquad(context!!, squadViewModel)
        val getOrderInList = { squadId: String -> paramViewModel.getOrderForNewEntityParam(squadId) }
        when (requestCode) {
            RequestCodes.REQUEST_CODE_SQUAD_NAME -> {
                currentSquad.squadName = data?.extras?.get(getString(R.string.intent_new_value)).toString()
                squadViewModel.updateSquadInfo(currentSquad)
            }
            RequestCodes.REQUEST_CODE_PHOTO_FROM_GALLERY -> {
                if (data != null) {
                    try {
                        val bitmap = PhotoLoaderUtils.getBitmapFromGalleryUri(
                            (activity as SquadActivity).contentResolver,
                            data.data
                        )
                        squad_avatar.setImageBitmap(bitmap)
                        run {
                            val newPath = PhotoLoaderUtils.saveImage(bitmap, activity as SquadActivity)
                            PhotoLoaderUtils.deleteImage(currentSquad.photo)
                            currentSquad.photo = newPath
                            squadViewModel.updateSquadInfo(currentSquad)
                        }
                        Toast.makeText(
                            activity as SquadActivity,
                            getString(R.string.ui_image_saved),
                            Toast.LENGTH_SHORT
                        ).show()
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Toast.makeText(activity as SquadActivity, getString(R.string.ui_failed), Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
            RequestCodes.REQUEST_CODE_PHOTO_FROM_CAMERA -> {
                try {
                    val thumbnail = data!!.extras!!.get("data") as Bitmap
                    val path = PhotoLoaderUtils.saveImage(thumbnail, activity as SquadActivity)
                    squad_avatar.setImageBitmap(thumbnail)
                    currentSquad.photo = path
                    squadViewModel.updateSquadInfo(currentSquad)
                    Toast.makeText(activity as SquadActivity, getString(R.string.ui_image_saved), Toast.LENGTH_SHORT)
                        .show()
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(activity as SquadActivity, getString(R.string.ui_failed), Toast.LENGTH_SHORT).show()
                }
            }
            RequestCodes.REQUEST_CODE_PARAM_ADD -> {
                paramViewModel.save(
                    IntentBoxingUtils.extractParamInfoFromIntent(
                        data,
                        activity as SquadActivity,
                        getOrderInList
                    )
                )
            }
            RequestCodes.REQUEST_CODE_PARAM_EDIT -> {
                paramViewModel.update(
                    IntentBoxingUtils.extractParamInfoFromIntent(data, activity as SquadActivity, getOrderInList)
                )
            }
        }
    }

}
