package com.lavreniuk.counselorhelper.activities.dialogs

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.enums.NoteOrTask
import com.lavreniuk.counselorhelper.enums.TaskStatus
import com.lavreniuk.counselorhelper.listeners.CustomOnTouchListenerForEditTextView
import com.lavreniuk.counselorhelper.listeners.DateInputOnClickListener
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import kotlinx.android.synthetic.main.activity_note_editor.*


class NoteEditorActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_editor)

        val programActivitiesNames =
            arrayOf("") + intent.getStringArrayExtra(getString(R.string.intent_program_activities_names))

        note_target_date_input_field.setOnClickListener(DateInputOnClickListener(this))
        note_target_date_input_field.setOnTouchListener(CustomOnTouchListenerForEditTextView())

        ArrayAdapter(
            applicationContext,
            R.layout.spinner_item,
            programActivitiesNames
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.dropdown_spinner_item)
            note_select_program_activity_spinner.adapter = adapter
        }

        ArrayAdapter(
            applicationContext,
            R.layout.spinner_item,
            NoteOrTask.values()
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.dropdown_spinner_item)
            note_note_or_task_spinner.adapter = adapter
        }

        ArrayAdapter(
            applicationContext,
            R.layout.spinner_item,
            TaskStatus.values()
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.dropdown_spinner_item)
            note_select_task_status_spinner.adapter = adapter
        }

        note_note_or_task_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (NoteOrTask.valueOf(note_note_or_task_spinner.getItemAtPosition(position).toString())) {
                    NoteOrTask.Note -> {
                        note_select_task_status_layout.visibility = View.GONE
                    }
                    NoteOrTask.Task -> {
                        note_select_task_status_layout.visibility = View.VISIBLE
                    }
                }

            }

        }

        val intent = intent
        val addOrEdit = intent.getStringExtra(getString(R.string.intent_add_or_edit))

        if (addOrEdit == getString(R.string.intent_edit)) {
            // set program_activity name
            note_select_program_activity_spinner.setSelection(
                programActivitiesNames.indexOf(intent.getStringExtra(getString(R.string.intent_note_activity_name))),
                true
            )

            // set note or task
            val taskOrNote = NoteOrTask.valueOf(intent.getStringExtra(getString(R.string.intent_note_or_task)))
            note_note_or_task_spinner.setSelection(
                NoteOrTask.values().indexOf(taskOrNote),
                true
            )
            if (taskOrNote == NoteOrTask.Note) {
                note_select_task_status_layout.visibility = View.GONE
            } else {
                note_select_task_status_layout.visibility = View.VISIBLE
                // set note status
                note_select_task_status_spinner.setSelection(
                    TaskStatus.values().indexOf(
                        TaskStatus.valueOf(intent.getStringExtra(getString(R.string.intent_task_status)))
                    ),
                    true
                )
            }

            note_target_date_input_field.setText(intent.getStringExtra(getString(R.string.intent_note_target_date)))
            note_text_input_field.setText(intent.getStringExtra(getString(R.string.intent_note_text)))
        }

        note_remove_target_date_icon.setOnClickListener {
            note_target_date_input_field.setText("")
        }

        note_activity_submit_button.setOnClickListener {
            // set target date
            val noteTargetDate = note_target_date_input_field.text.toString()
            if (noteTargetDate.isNotBlank() && !StringTransformationUtils.isCorrectDateString(noteTargetDate)) {
                Toast.makeText(this, getString(R.string.ui_incorrect_date_value), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            intent.putExtra(getString(R.string.intent_note_target_date), noteTargetDate)

            // set text
            val noteText = note_text_input_field.text.toString().trimEnd()
            if (noteText.isBlank()) {
                Toast.makeText(this, getString(R.string.ui_note_text_is_empty), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            intent.putExtra(getString(R.string.intent_note_text), noteText)

            // set program activity
            note_select_program_activity_spinner.selectedItem.toString().let {
                if (it.isNotEmpty()) {
                    intent.putExtra(getString(R.string.intent_note_activity_name), it)
                } else {
                    intent.removeExtra(getString(R.string.intent_note_activity_name))
                }
            }

            // set type
            val noteOrTask = note_note_or_task_spinner.selectedItem?.toString()
            intent.putExtra(
                getString(R.string.intent_note_or_task),
                noteOrTask
            )

            noteOrTask?.let {
                if (NoteOrTask.valueOf(it) == NoteOrTask.Task) {
                    // set status
                    intent.putExtra(
                        getString(R.string.intent_task_status),
                        note_select_task_status_spinner.selectedItem?.toString()
                    )
                }
            }

            setResult(RESULT_OK, intent)
            finish()
        }

        note_activity_cancel_button.setOnClickListener {
            setResult(RESULT_CANCELED, intent)
            finish()
        }
    }
}
