package com.lavreniuk.counselorhelper.activities.dialogs

import android.app.Activity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import com.itextpdf.text.log.LoggerFactory
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.enums.ContactType
import com.lavreniuk.counselorhelper.utils.Validators
import kotlinx.android.synthetic.main.activity_contact_editor.*

class ContactEditorActivity : Activity() {

    private val logger = LoggerFactory.getLogger(ContactEditorActivity::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_editor)

        ArrayAdapter(
            applicationContext,
            R.layout.spinner_item,
            ContactType.values()
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.dropdown_spinner_item)
            contact_type_spinner.adapter = adapter
        }

        val intent = intent
        if (intent.getStringExtra(getString(R.string.intent_add_or_edit)) == getString(R.string.intent_edit)) {
            val contactType = intent.getSerializableExtra(getString(R.string.intent_contact_type))
            if (contactType !is ContactType) {
                logger.error("Wrong datatype of contact_type")
            } else {
                contact_type_spinner.setSelection(ContactType.getContactTypeArrayIndex(contactType), true)
                contact_value_input_field.setText(intent.getStringExtra(getString(R.string.intent_contact_value)))
            }
        }

        submit_button.setOnClickListener {
            val contactType = contact_type_spinner.selectedItem.toString()
            val contactValue = contact_value_input_field.text.toString()
            if (contactValue.isBlank()) {
                Toast.makeText(this, getString(R.string.ui_field_value_cannot_be_empty), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else if (isCorrectContactValue(
                    ContactType.valueOf(contactType),
                    contactValue
                ) || contactValue.isBlank()
            ) {
                intent.putExtra(getString(R.string.intent_contact_type), contactType)
                intent.putExtra(getString(R.string.intent_contact_value), contactValue)
                setResult(RESULT_OK, intent)
                finish()
            } else {
                Toast.makeText(this, getString(R.string.ui_incorrect_value), Toast.LENGTH_SHORT).show()
            }
        }

        cancel_contact_editing_button.setOnClickListener {
            setResult(RESULT_CANCELED, intent)
            finish()
        }
    }

    private fun isCorrectContactValue(contactType: ContactType, contactValue: String): Boolean {
        when (contactType) {
            ContactType.email -> return Validators.isEmailValid(contactValue)
            ContactType.instagram -> return Validators.isInstagramUsernameValid(contactValue)
            ContactType.skype -> return Validators.isSkypeUsernameValid(contactValue)
            ContactType.facebook -> return Validators.isFacebookUsernameValid(contactValue)
            ContactType.twitter -> return Validators.isTwitterUsernameValid(contactValue)
            ContactType.vk -> return Validators.isEmailValid(contactValue)
            ContactType.linkedin -> return Validators.isLinkedinUsernameValid(contactValue)
            ContactType.telegram -> return Validators.isTelegramUsernameValid(contactValue)
            ContactType.phone -> return Validators.isPhoneNumberValid(contactValue)
            ContactType.google -> return Validators.isEmailValid(contactValue)
            ContactType.other -> return true
        }
        return false
    }
}
