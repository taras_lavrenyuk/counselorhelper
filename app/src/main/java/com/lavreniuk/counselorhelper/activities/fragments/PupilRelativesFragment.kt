package com.lavreniuk.counselorhelper.activities.fragments

import android.app.AlertDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.PupilActivity
import com.lavreniuk.counselorhelper.activities.dialogs.RelativeEditorActivity
import com.lavreniuk.counselorhelper.itemviews.itemviewadapters.RelativeAdapter
import com.lavreniuk.counselorhelper.models.dtos.NameDto
import com.lavreniuk.counselorhelper.models.entities.Relative
import com.lavreniuk.counselorhelper.utils.IntentBoxingUtils
import com.lavreniuk.counselorhelper.utils.RequestCodes
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import com.lavreniuk.counselorhelper.viewmodels.RelativeViewModel
import kotlinx.android.synthetic.main.fragment_pupil_relatives.*
import kotlinx.android.synthetic.main.fragment_pupil_relatives.view.*


class PupilRelativesFragment : Fragment() {

    private val relativeViewModel: RelativeViewModel by lazy {
        ViewModelProviders.of(this).get(RelativeViewModel::class.java)
    }

    var pupilId: String = "";

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_pupil_relatives, container, false)
        pupilId = arguments!!.getString("pupil-id").orEmpty()

        view.add_pupil_relative_param_button.setOnClickListener {
            val intent = Intent(context as PupilActivity, RelativeEditorActivity::class.java)
            intent.putExtra(getString(R.string.intent_add_or_edit), getString(R.string.intent_add))
            intent.putExtra(getString(R.string.intent_pupil_id), pupilId)
            startActivityForResult(intent, RequestCodes.REQUEST_CODE_EDIT_RELATIVE)
        }

        view.pupil_relatives_list_view.setOnItemLongClickListener { parent, view, position, id ->
            val relative = pupil_relatives_list_view.adapter.getItem(position)
            if (relative !is Relative) {
                assert(false) { "$relative is not a ${Relative::class} instance" }
                return@setOnItemLongClickListener false
            }
            val relativeChangeDialog = AlertDialog.Builder(context as PupilActivity)
            relativeChangeDialog.setTitle(getString(R.string.ui_select_action))
            val contactChangeDialogItems =
                arrayOf(
                    getString(R.string.ui_relative_popupmenu_edit),
                    getString(R.string.ui_relative_popupmenu_delete),
                    getString(R.string.ui_copy_value)
                )
            relativeChangeDialog.setItems(
                contactChangeDialogItems
            ) { _, which ->
                when (which) {
                    0 -> editRelativeInfo(relative)
                    1 -> deleteRelative(relative)
                    2 -> copyContactValueToClipboard(relative)
                    else -> assert(false) { "Wrong argument" }
                }
            }
            relativeChangeDialog.show()

            return@setOnItemLongClickListener true
        }

        return view
    }

    private fun copyContactValueToClipboard(relative: Relative) {
        val clipboard = (context as PupilActivity).getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Contact", relative.relativeContact)
        clipboard.primaryClip = clip
    }

    private fun editRelativeInfo(relative: Relative) {
        val intent = Intent(context as PupilActivity, RelativeEditorActivity::class.java)
        intent.putExtra(
            getString(R.string.intent_relative_name),
            NameDto(relative.firstName, relative.secondName, relative.lastName)
        )
        intent.putExtra(getString(R.string.intent_add_or_edit), getString(R.string.intent_edit))
        intent.putExtra(getString(R.string.intent_contact_value), relative.relativeContact)
        intent.putExtra(getString(R.string.intent_relative_connection), relative.connection)
        intent.putExtra(getString(R.string.intent_relative_id), relative.relativeId)
        startActivityForResult(intent, RequestCodes.REQUEST_CODE_EDIT_RELATIVE)
    }

    private fun deleteRelative(relative: Relative) {
        relativeViewModel.delete(relative)
        fillPupilParentsList()
    }

    override fun onResume() {
        super.onResume()
        fillPupilParentsList()
    }

    private fun fillPupilParentsList() {
        if (pupilId.isNotBlank()) {
            val relatives: List<Relative> = relativeViewModel.getPupilRelatives(pupilId)
            pupil_relatives_list_view.adapter =
                RelativeAdapter(context!!, relatives.toTypedArray())
        } else {
            assert(false) { "PupilId is incorrect isSomeActivityWillBeStarted!" }
            pupil_relatives_list_view.adapter = RelativeAdapter(
                context!!,
                emptyList<Relative>().toTypedArray()
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == 0) return

        when (requestCode) {
            RequestCodes.REQUEST_CODE_EDIT_RELATIVE -> {
                val relativeFromIntent = IntentBoxingUtils.extractRelativeFromIntent(data, context as PupilActivity)
                relativeFromIntent.relativeOf = pupilId
                val actionType = data?.extras?.get(getString(R.string.intent_add_or_edit)).toString()
                when (actionType) {
                    getString(R.string.intent_add) -> relativeViewModel.save(relativeFromIntent)
                    getString(R.string.intent_edit) -> relativeViewModel.update(relativeFromIntent)
                    else -> assert(false) { "Unknown isSomeActivityWillBeStarted!!! Process!!!" }
                }
            }
            else -> assert(false) { "Unknown isSomeActivityWillBeStarted!!!" }
        }
    }
}