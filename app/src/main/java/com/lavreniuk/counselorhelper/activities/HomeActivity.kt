package com.lavreniuk.counselorhelper.activities

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.MenuItem.SHOW_AS_ACTION_ALWAYS
import android.view.MenuItem.SHOW_AS_ACTION_NEVER
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.dialogs.SetParamActivity
import com.lavreniuk.counselorhelper.reports.activities.ReportsListActivity
import com.lavreniuk.counselorhelper.models.entities.Squad
import com.lavreniuk.counselorhelper.utils.PhotoLoaderUtils
import com.lavreniuk.counselorhelper.utils.PreferencesUtilsMethods
import com.lavreniuk.counselorhelper.utils.RequestCodes
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import com.lavreniuk.counselorhelper.viewmodels.NoteViewModel
import com.lavreniuk.counselorhelper.viewmodels.SquadViewModel
import com.lavreniuk.counselorhelper.viewmodels.UserViewModel
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.nav_header_home.*
import java.util.*

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var squadListOptionMenu: Menu

    private val squadViewModel: SquadViewModel by lazy {
        ViewModelProviders.of(this).get(SquadViewModel::class.java)
    }

    private val noteViewModel: NoteViewModel by lazy {
        ViewModelProviders.of(this).get(NoteViewModel::class.java)
    }

    private val userViewModel: UserViewModel by lazy {
        ViewModelProviders.of(this).get(UserViewModel::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activityWillBeStarted = ActivitiesStarterHelper()
        checkIfUserExist(activityWillBeStarted)
        checkIfSquadExist(activityWillBeStarted)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onResume() {
        super.onResume()
        setUpHomeScreenInfo()
        setUpNavHeaderInfo()
    }

    private fun setUpNavHeaderInfo() {
        PreferencesUtilsMethods.getCurrentSquadId(this)?.let {
            val headerLayout = nav_view.getHeaderView(0)

            headerLayout.findViewById<TextView>(R.id.nav_header_squad_name).text = squadViewModel.getSquadName(it)
            val image = PhotoLoaderUtils.getBitmapFromPath(squadViewModel.getSquadPhoto(it))
            headerLayout.findViewById<ImageView>(R.id.nav_header_squad_photo).setImageBitmap(image)
        }

    }

    /**
     * Set up current squad name, today todos number, tomorrow todos number, overall todos number
     */
    private fun setUpHomeScreenInfo() {
        PreferencesUtilsMethods.getCurrentSquadId(this)?.let {
            home_activity_squad_name.text = squadViewModel.getSquadName(it)
            home_activity_today_todos_number.text = getString(R.string.ui_todos_for_today, noteViewModel.getTodosNumberBySquadAndDate(it, Date()))
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DATE, 1)
            home_activity_tomorrow_todos_number.text = getString(R.string.ui_todos_for_tomorrow, noteViewModel.getTodosNumberBySquadAndDate(it, calendar.time))
            home_activity_todos_number.text = getString(R.string.ui_in_all_todos, noteViewModel.getTodosNumberBySquad(it))
        }
    }

    private fun checkIfUserExist(activitiesStarterHelper: ActivitiesStarterHelper) {
        val userId = PreferencesUtilsMethods.getUserId(this, true)
        val defValue = "default"
        val bool1 = userId == defValue
        val bool2 = userViewModel.getUser(userId) == null
        if ((bool1 || bool2) && activitiesStarterHelper.canStartAnotherActivity()) {
            activitiesStarterHelper.activityWillBeStarted()
            val welcomeActivityIntent = Intent(this, WelcomeActivity::class.java)
            welcomeActivityIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(welcomeActivityIntent)
            finish()
        }
    }

    private fun checkIfSquadExist(activitiesStarterHelper: ActivitiesStarterHelper) {
        if (squadViewModel.getAllSquads().isEmpty() && activitiesStarterHelper.canStartAnotherActivity()) {
            activitiesStarterHelper.activityWillBeStarted()
            launchCreateSquadActivity()
        }
    }

    /**
     * Starts SetParamActivity with @code [RequestCodes.REQUEST_CODE_GET_NEW_SQUAD_NAME] to create new squad
     */
    private fun launchCreateSquadActivity() {
        val intent = Intent(this, SetParamActivity::class.java)
        intent.putExtra(getString(R.string.intent_info_label_text), "Enter squad name:")
        startActivityForResult(intent, RequestCodes.REQUEST_CODE_GET_NEW_SQUAD_NAME)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)
        squadListOptionMenu = menu
        initOptionMenu()
        return true
    }

    private fun initOptionMenu() {
        runOnUiThread {
            if (!(::squadListOptionMenu.isInitialized)) {
                return@runOnUiThread
            }
            squadListOptionMenu.clear()
            val allSquads = squadViewModel.getAllSquads()
            val currentSquadId = PreferencesUtilsMethods.getCurrentSquadId(this)
            for (squad in allSquads) {
                val menuItem = squadListOptionMenu.add(squad.squadName)

                if (currentSquadId != squad.squadId) {
                    menuItem.setShowAsAction(SHOW_AS_ACTION_NEVER)

                    menuItem.setOnMenuItemClickListener {
                        PreferencesUtilsMethods.setCurrentSquadId(this, squad.squadId)
                        initOptionMenu()
                        setUpHomeScreenInfo()
                        setUpNavHeaderInfo()
                        return@setOnMenuItemClickListener true
                    }
                } else {
                    menuItem.setShowAsAction(SHOW_AS_ACTION_ALWAYS)
                }
            }
            val createSquadItem = squadListOptionMenu.add(
                1,
                R.id.squad_list_option_menu_add_squad,
                9999,
                getString(R.string.action_create_squad)
            )
            createSquadItem.setShowAsAction(SHOW_AS_ACTION_NEVER)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.squad_list_option_menu_add_squad -> {
                launchCreateSquadActivity()
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_squad -> {
                val currentSquadId = PreferencesUtilsMethods.getCurrentSquadId(this)
                if (currentSquadId == null || squadViewModel.getSquad(currentSquadId) == null) {
                    Toast.makeText(this, "Create squad first", Toast.LENGTH_SHORT).show()
                    return false
                }
                val intent = Intent(this, SquadActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_program -> {
                val currentSquadId = PreferencesUtilsMethods.getCurrentSquadId(this)
                if (currentSquadId == null || squadViewModel.getSquad(currentSquadId) == null) {
                    Toast.makeText(this, "Create squad first", Toast.LENGTH_SHORT).show()
                    return false
                }
                val intent = Intent(this, ProgramActivityActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_reports -> {
                val intent = Intent(this, ReportsListActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_staff -> {
                val intent = Intent(this, StaffListActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_user_info -> {
                val intent = Intent(this, AccountSettingsActivity::class.java)
                startActivity(intent)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == 0) return

        when (requestCode) {
            RequestCodes.REQUEST_CODE_GET_NEW_SQUAD_NAME -> {
                val squadId = UtilsMethods.generateId()
                val squadName = data?.extras?.get(getString(R.string.intent_new_value)).toString()
                val newSquad = Squad(
                    squadId,
                    squadName,
                    ""
                )
                squadViewModel.saveSquad(newSquad)
                if (PreferencesUtilsMethods.getCurrentSquadId(this) == null) {
                    PreferencesUtilsMethods.setCurrentSquadId(this, squadId)
                }
                initOptionMenu()
            }
        }
    }

    inner class ActivitiesStarterHelper (
        private var isSomeActivityWillBeStarted: Boolean = false
    ) {

        fun canStartAnotherActivity(): Boolean = !isSomeActivityWillBeStarted

        fun activityWillBeStarted() {
            isSomeActivityWillBeStarted = true
        }

    }
}
