package com.lavreniuk.counselorhelper.activities.fragments


import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SearchView
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.ProgramActivityActivity
import com.lavreniuk.counselorhelper.activities.dialogs.NoteEditorActivity
import com.lavreniuk.counselorhelper.enums.NoteFilter
import com.lavreniuk.counselorhelper.enums.NoteOrTask
import com.lavreniuk.counselorhelper.enums.TaskStatus
import com.lavreniuk.counselorhelper.itemviews.dragitemviewadapters.NoteDragAdapter
import com.lavreniuk.counselorhelper.models.entities.Note
import com.lavreniuk.counselorhelper.utils.*
import com.lavreniuk.counselorhelper.viewmodels.NoteViewModel
import com.lavreniuk.counselorhelper.viewmodels.ProgramActivityViewModel
import com.woxthebox.draglistview.DragListView
import kotlinx.android.synthetic.main.fragment_note.*
import kotlinx.android.synthetic.main.fragment_note.view.*
import kotlin.concurrent.thread


class NoteFragment : Fragment() {

    private val noteViewModel: NoteViewModel by lazy {
        ViewModelProviders.of(this).get(NoteViewModel::class.java)
    }

    private val programActivityViewModel: ProgramActivityViewModel by lazy {
        ViewModelProviders.of(this).get(ProgramActivityViewModel::class.java)
    }

    private val deleteNote = { noteId: String -> noteViewModel.deleteById(noteId) }
    private val fragmentOnResume = { onResume() }
    private val getProgramActivitiesNames =
        { programActivityViewModel.getProgramActivitiesNamesSortedByOrderInList(getCurrentSquadId()!!) }


    private var searchQuery = ""
    private var noteFilter = NoteFilter.All

    private var notes: List<Note> = emptyList()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_note, container, false)
        setHasOptionsMenu(true)

        ArrayAdapter(
            activity!!.applicationContext,
            R.layout.spinner_item,
            NoteFilter.values()
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.dropdown_spinner_item)
            view.note_filter_spinner.adapter = adapter
        }
        view.note_filter_spinner.setSelection(NoteFilter.values().indexOf(NoteFilter.All), true)

        view.note_filter_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(parent: AdapterView<*>?) {
                noteFilter = NoteFilter.All
                fillNotesList()
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                noteFilter = NoteFilter.values()[position]
                fillNotesList()
            }
        }

        view.add_note_param_button.setOnClickListener {
            val intent = Intent(activity as ProgramActivityActivity, NoteEditorActivity::class.java)
            intent.putExtra(getString(R.string.intent_add_or_edit), getString(R.string.intent_add))
            val squadId = getCurrentSquadId() ?: return@setOnClickListener
            intent.putExtra(
                getString(R.string.intent_program_activities_names),
                programActivityViewModel.getProgramActivitiesNamesSortedByOrderInList(squadId).toTypedArray()
            )
            startActivityForResult(intent, RequestCodes.REQUEST_CODE_NOTE_ADD)
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        UtilsMethods.hideKeyboard(context as ProgramActivityActivity)
        val currentSquadId = getCurrentSquadId()
        notes = if (currentSquadId == null) {
            emptyList()
        } else {
            noteViewModel.getNotesSortedByOrderInList(currentSquadId)
        }
        fillNotesList()
    }

    private fun fillNotesList() {
        val filteredNotes = notes.filter { note: Note ->
            when (noteFilter) {
                NoteFilter.Todo -> note.noteOrTask == NoteOrTask.Task && note.taskStatus == TaskStatus.Todo
                NoteFilter.Done -> note.noteOrTask == NoteOrTask.Task && note.taskStatus == TaskStatus.Done
                NoteFilter.Notes -> note.noteOrTask == NoteOrTask.Note
                NoteFilter.All -> true
            }.and(
                note.noteText.contains(searchQuery, true)
                    .or(note.programActivityName != null && note.programActivityName!!.contains(searchQuery, true))
                    .or(
                        note.noteTargetDate != null
                                && StringTransformationUtils.fromDateToString(note.noteTargetDate!!).contains(
                            searchQuery,
                            true
                        )
                    )
            )
        }

        val listAdapter = NoteDragAdapter(
            filteredNotes.toTypedArray(),
            this,
            getProgramActivitiesNames,
            deleteNote,
            fragmentOnResume
        )

        val dragAndDropListener = object : DragListView.DragListListener {
            override fun onItemDragging(itemPosition: Int, x: Float, y: Float) {
            }

            override fun onItemDragStarted(position: Int) {
            }

            override fun onItemDragEnded(fromPosition: Int, toPosition: Int) {
                if (fromPosition != toPosition) {
                    thread(start = true) {
                        val getParamIdOnPosition =
                            { position: Int -> listAdapter.getNoteIdOnPosition(position) }
                        if (fromPosition > toPosition) {
                            noteViewModel.updateNotesOrder(toPosition, fromPosition, getParamIdOnPosition)
                        } else {
                            noteViewModel.updateNotesOrder(fromPosition, toPosition, getParamIdOnPosition)
                        }
                    }
                }
            }
        }

        notes_draganddrop_list.setDragListListener(dragAndDropListener)
        notes_draganddrop_list.setLayoutManager(LinearLayoutManager(activity))

        notes_draganddrop_list.setAdapter(listAdapter, true)
        notes_draganddrop_list.setCanDragHorizontally(false)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()

        inflater!!.inflate(R.menu.search_menu, menu)
        val menuItem = menu.findItem(R.id.search_menu)
        val searchView = menuItem.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val query = newText.toString()
                searchQuery =
                    if (query == "null" || query.isBlank()) ""
                    else query

                fillNotesList()
                return false
            }

        })
        searchView.setOnClickListener { }
    }

    private fun getCurrentSquadId(): String? {
        val currentSquadId = PreferencesUtilsMethods.getCurrentSquadId(context as ProgramActivityActivity)
        if (currentSquadId == null) {
            assert(false) { "Error!!!. CurrentSquadId cannot not be null at this point of program!!!" }
            return null
        } else {
            return currentSquadId
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == 0) return

        val getOrderInList = { noteViewModel.getOrderForNewNote(getCurrentSquadId()!!) }

        when (requestCode) {
            RequestCodes.REQUEST_CODE_NOTE_ADD -> {
                val noteFromIntent = IntentBoxingUtils.extractNoteFromIntent(
                    data,
                    activity as ProgramActivityActivity,
                    getOrderInList
                )
                noteViewModel.save(noteFromIntent)
            }
            RequestCodes.REQUEST_CODE_NOTE_EDIT -> {
                noteViewModel.update(
                    IntentBoxingUtils.extractNoteFromIntent(
                        data,
                        activity as ProgramActivityActivity,
                        getOrderInList
                    )
                )
            }
        }
    }

}
