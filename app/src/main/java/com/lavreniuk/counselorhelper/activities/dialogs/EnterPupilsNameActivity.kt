package com.lavreniuk.counselorhelper.activities.dialogs

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import kotlinx.android.synthetic.main.activity_enter_pupils_name.*

class EnterPupilsNameActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_pupils_name)

        new_pupil_submit_button.setOnClickListener {
            var pupilsFirstName = new_pupil_first_name_input_field.text.toString()
            var pupilsLastName = new_pupil_last_name_input_field.text.toString()

            if (pupilsFirstName.isBlank()) {
                Toast.makeText(this, getString(R.string.error_invalid_first_name), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            pupilsFirstName = pupilsFirstName.replace("\\s".toRegex(), "")
            pupilsLastName = pupilsLastName.replace("\\s".toRegex(), "")

            intent.putExtra(getString(R.string.intent_pupils_first_name), pupilsFirstName)
            intent.putExtra(getString(R.string.intent_pupils_last_name), pupilsLastName)
            setResult(RESULT_OK, intent)
            finish()
        }

        new_pupil_cancel_button.setOnClickListener {
            setResult(RESULT_CANCELED, intent)
            finish()
        }

    }
}
