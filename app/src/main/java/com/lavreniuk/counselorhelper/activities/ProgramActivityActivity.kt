package com.lavreniuk.counselorhelper.activities

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.adapters.pageradapters.ProgramActivityPagerAdapter
import kotlinx.android.synthetic.main.activity_program_activity.*

class ProgramActivityActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_program_activity)

        setUpTabs();
    }

    private fun setUpTabs() {
        program_activity_tabs_layout.addTab(program_activity_tabs_layout.newTab().setText("Program"))
        program_activity_tabs_layout.addTab(program_activity_tabs_layout.newTab().setText("Tasks&Notes"))
        program_activity_tabs_layout.tabGravity = TabLayout.GRAVITY_FILL
        val pagerAdapter = ProgramActivityPagerAdapter(supportFragmentManager)
        program_activity_pager.adapter = pagerAdapter
        program_activity_pager.addOnPageChangeListener(
            TabLayout.TabLayoutOnPageChangeListener(
                program_activity_tabs_layout
            )
        )
        program_activity_tabs_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                program_activity_pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }
}
