package com.lavreniuk.counselorhelper.activities.dialogs

import android.app.Activity
import android.os.Bundle
import android.text.InputFilter
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.models.dtos.NameDto
import kotlinx.android.synthetic.main.activity_relative_editor.*

class RelativeEditorActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_relative_editor)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        relative_first_name_input_field.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(40))
        relative_second_name_input_field.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(40))
        relative_last_name_input_field.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(40))

        val adapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_dropdown_item_1line,
            resources.getStringArray(R.array.relative_connection_suggestions)
        )
        relative_connection_input_field.threshold = 1
        relative_connection_input_field.setAdapter<ArrayAdapter<String>>(adapter)

        val addOrEdit = intent.getStringExtra(getString(R.string.intent_add_or_edit))
        if (addOrEdit == getString(R.string.intent_edit)) {
            val relativeName = intent.getParcelableExtra<NameDto>(getString(R.string.intent_relative_name))

            relative_first_name_input_field.setText(relativeName.firstName)
            relative_second_name_input_field.setText(relativeName.secondName)
            relative_last_name_input_field.setText(relativeName.lastName)

            relative_contact_input_field.setText(intent.getStringExtra(getString(R.string.intent_contact_value)))
            relative_connection_input_field.setText(intent.getStringExtra(getString(R.string.intent_relative_connection)))
        }

        relative_activity_submit_button.setOnClickListener {
            if (relative_first_name_input_field.text.isBlank()) {
                Toast.makeText(this, getString(R.string.ui_field_first_name_cannot_be_empty), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (relative_second_name_input_field.text.isBlank()) {
                relative_second_name_input_field.setText("")
            }
            if (relative_last_name_input_field.text.isBlank()) {
                relative_last_name_input_field.setText("")
            }
            val relativeName = NameDto(
                relative_first_name_input_field.text.toString(),
                relative_second_name_input_field.text.toString(),
                relative_last_name_input_field.text.toString()
            )

            val relativeConnection = relative_connection_input_field.text
            if (relativeConnection.isBlank()) {
                Toast.makeText(
                    this,
                    getString(R.string.ui_field_relative_connection_cannot_be_empty),
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            val relativeContact = relative_contact_input_field.text
            if (relativeContact.isBlank()) {
                Toast.makeText(this, getString(R.string.ui_field_relative_contact_cannot_be_empty), Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }

            intent.putExtra(getString(R.string.intent_relative_connection), relativeConnection)
            intent.putExtra(getString(R.string.intent_relative_contact), relativeContact)
            intent.putExtra(getString(R.string.intent_relative_name), relativeName)
            setResult(RESULT_OK, intent)
            finish()
        }

        relative_activity_cancel_button.setOnClickListener {
            setResult(RESULT_CANCELED, intent)
            finish()
        }

    }
}
