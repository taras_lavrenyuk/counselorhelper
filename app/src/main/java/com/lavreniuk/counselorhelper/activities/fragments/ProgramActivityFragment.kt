package com.lavreniuk.counselorhelper.activities.fragments


import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.ProgramActivityActivity
import com.lavreniuk.counselorhelper.activities.dialogs.ProgramActivityEditorActivity
import com.lavreniuk.counselorhelper.itemviews.dragitemviewadapters.ProgramActivityDragAdapter
import com.lavreniuk.counselorhelper.models.entities.ProgramActivity
import com.lavreniuk.counselorhelper.utils.IntentBoxingUtils
import com.lavreniuk.counselorhelper.utils.PreferencesUtilsMethods
import com.lavreniuk.counselorhelper.utils.RequestCodes
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import com.lavreniuk.counselorhelper.viewmodels.ProgramActivityViewModel
import com.woxthebox.draglistview.DragListView
import kotlinx.android.synthetic.main.fragment_program_activity.*
import kotlinx.android.synthetic.main.fragment_program_activity.view.*
import kotlin.concurrent.thread

class ProgramActivityFragment : Fragment() {

    private val programActivityViewModel: ProgramActivityViewModel by lazy {
        ViewModelProviders.of(this).get(ProgramActivityViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_program_activity, container, false)

        view.add_program_activity_param_button.setOnClickListener {
            val intent = Intent(activity as ProgramActivityActivity, ProgramActivityEditorActivity::class.java)
            intent.putExtra(getString(R.string.intent_add_or_edit), getString(R.string.intent_add))
            val squadId = getCurrentSquadId() ?: return@setOnClickListener
            intent.putExtra(getString(R.string.intent_squad_id), squadId)
            intent.putExtra(
                getString(R.string.intent_program_activities_names),
                programActivityViewModel.getProgramActivitiesNamesSortedByOrderInList(squadId).toTypedArray()
            )
            startActivityForResult(intent, RequestCodes.REQUEST_CODE_PROGRAM_ACTIVITY_ADD)
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        UtilsMethods.hideKeyboard(context as ProgramActivityActivity)
        fillProgramActivitiesList()
    }

    private fun getCurrentSquadId(): String? {
        val currentSquadId = PreferencesUtilsMethods.getCurrentSquadId(context as ProgramActivityActivity)
        return if (currentSquadId == null) {
            assert(false) { "Error!!!. CurrentSquadId cannot not be null at this point of program!!!" }
            null
        } else {
            currentSquadId
        }
    }

    private fun fillProgramActivitiesList() {
        val currentSquadId = getCurrentSquadId()
        val programActivities: List<ProgramActivity>
        programActivities = if (currentSquadId == null) {
            emptyList()
        } else {
            programActivityViewModel.getProgramActivitiesSortedByOrderInList(currentSquadId)
        }

        val listAdapter = ProgramActivityDragAdapter(
            programActivities.toTypedArray(),
            this
        )

        val dragAndDropListener = object : DragListView.DragListListener {
            override fun onItemDragging(itemPosition: Int, x: Float, y: Float) {
            }

            override fun onItemDragStarted(position: Int) {
            }

            override fun onItemDragEnded(fromPosition: Int, toPosition: Int) {
                if (fromPosition != toPosition) {
                    thread(start = true) {
                        val getParamIdOnPosition =
                            { position: Int -> listAdapter.getProgramActivityIdOnPosition(position) }
                        if (fromPosition > toPosition) {
                            programActivityViewModel.updateParamsOrder(toPosition, fromPosition, getParamIdOnPosition)
                        } else {
                            programActivityViewModel.updateParamsOrder(fromPosition, toPosition, getParamIdOnPosition)
                        }
                    }
                }
            }
        }
        program_activities_draganddrop_list.setDragListListener(dragAndDropListener)
        program_activities_draganddrop_list.setLayoutManager(LinearLayoutManager(activity))

        program_activities_draganddrop_list.setAdapter(listAdapter, true)
        program_activities_draganddrop_list.setCanDragHorizontally(false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == 0) return

        val getOrderInList = { programActivityViewModel.getOrderForNewEntityParam(getCurrentSquadId()!!) }
        when (requestCode) {
            RequestCodes.REQUEST_CODE_PROGRAM_ACTIVITY_ADD -> {
                programActivityViewModel.save(
                    IntentBoxingUtils.extractProgramActivityFromIntent(
                        data,
                        activity as ProgramActivityActivity,
                        getOrderInList
                    )
                )
            }
            RequestCodes.REQUEST_CODE_PROGRAM_ACTIVITY_EDIT -> {
                programActivityViewModel.update(
                    IntentBoxingUtils.extractProgramActivityFromIntent(
                        data,
                        activity as ProgramActivityActivity,
                        getOrderInList
                    )
                )
            }
        }
    }
}
