package com.lavreniuk.counselorhelper.activities

import android.app.AlertDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.dialogs.SetNameActivity
import com.lavreniuk.counselorhelper.adapters.pageradapters.PupilActivityPagerAdapter
import com.lavreniuk.counselorhelper.listeners.CustomOnTouchListenerForEditTextView
import com.lavreniuk.counselorhelper.listeners.DateInputOnClickListener
import com.lavreniuk.counselorhelper.models.dtos.NameDto
import com.lavreniuk.counselorhelper.models.entities.Pupil
import com.lavreniuk.counselorhelper.utils.PermissionChecker
import com.lavreniuk.counselorhelper.utils.PhotoLoaderUtils
import com.lavreniuk.counselorhelper.utils.RequestCodes
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import com.lavreniuk.counselorhelper.viewmodels.PupilViewModel
import kotlinx.android.synthetic.main.activity_pupil.*
import org.joda.time.DateTimeComparator
import java.io.IOException


class PupilActivity : AppCompatActivity() {

    private val pupilViewModel: PupilViewModel by lazy {
        ViewModelProviders.of(this).get(PupilViewModel::class.java)
    }

    lateinit var pupilId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pupil)

        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }

        pupil_avatar.setOnClickListener {
            if (PermissionChecker.checkAndRequestCameraPermissions(this)) {
                val avatarChangeDialog = AlertDialog.Builder(this)
                avatarChangeDialog.setTitle(getString(R.string.ui_select_action))
                var pictureDialogItems = arrayOf(
                    getString(R.string.ui_avatar_from_galery),
                    getString(R.string.ui_avatar_from_camera)
                )
                val pupil = getPupil()
                val avatarPath = pupil.photo
                if (avatarPath != null) {
                    pictureDialogItems = pictureDialogItems.plus(getString(R.string.ui_avatar_delete))
                }
                avatarChangeDialog.setItems(
                    pictureDialogItems
                ) { _, which ->
                    when (which) {
                        0 -> PhotoLoaderUtils.choosePhotoFromGallery(this)
                        1 -> PhotoLoaderUtils.takePhotoFromCamera(this)
                        2 -> deletePupilAvatar(avatarPath, pupil)
                        else -> assert(false) { "Wrong argument" }
                    }
                }
                avatarChangeDialog.show()
            }
        }

        pupil_name_textview.setOnClickListener {
            val pupil = getPupil()
            callSetNameActivity(
                NameDto(pupil.firstName, pupil.secondName, pupil.lastName)
            )
        }

        pupil_birthday_textview.setOnClickListener(DateInputOnClickListener(this))
        pupil_birthday_textview.setOnTouchListener(CustomOnTouchListenerForEditTextView())

        pupil_birthday_textview.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                Thread().run {
                    val newBirthday =
                        StringTransformationUtils.fromStringToDate(pupil_birthday_textview.text.toString())
                    val pupil = getPupil()
                    // if two dates are equals => do not update user.birthday
                    if (DateTimeComparator.getDateOnlyInstance().compare(newBirthday, pupil.birthday) == 0) {
                        return@run
                    }
                    pupil.birthday = newBirthday
                    pupilViewModel.updatePupil(pupil)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        setUpTabs()
    }

    private fun callSetNameActivity(
        oldName: NameDto
    ) {
        val intent = Intent(this, SetNameActivity::class.java)
        intent.putExtra(getString(R.string.intent_old_value), oldName)
        startActivityForResult(intent, RequestCodes.REQUEST_CODE_NAME)
    }

    private fun deletePupilAvatar(avatarPath: String?, pupil: Pupil) {
        pupil_avatar.setImageDrawable(getDrawable(R.mipmap.ic_launcher_round))

        avatarPath?.let {
            run {
                PhotoLoaderUtils.deleteImage(it)
                pupil.photo = ""
                pupilViewModel.updatePupil(pupil)
            }
        }
    }

    private fun getPupil(): Pupil {
        return pupilViewModel.getPupilById(pupilId)
    }

    override fun onResume() {
        super.onResume()
        val pupil = getPupil()
        pupil_name_textview.text = StringTransformationUtils.getFullNameString(pupil, false)
        pupil.birthday?.let { pupil_birthday_textview.text = StringTransformationUtils.fromDateToString(it) }

        val image = PhotoLoaderUtils.getBitmapFromPath(pupil.photo)
        if (image != null) pupil_avatar.setImageBitmap(image)
    }


    private fun setUpTabs() {
        pupil_activity_tabs_layout.addTab(pupil_activity_tabs_layout.newTab().setText(getString(R.string.ui_pupils_info)))
        pupil_activity_tabs_layout.addTab(pupil_activity_tabs_layout.newTab().setText(getString(R.string.ui_pupils_contacts)))
        pupil_activity_tabs_layout.addTab(pupil_activity_tabs_layout.newTab().setText(getString(R.string.ui_pupils_relatives)))
        pupil_activity_tabs_layout.tabGravity = TabLayout.GRAVITY_FILL
        pupilId = intent.getStringExtra(getString(R.string.intent_pupil_id))
        val pagerAdapter = PupilActivityPagerAdapter(
            supportFragmentManager,
            pupilId
        )
        pupil_activity_pager.adapter = pagerAdapter
        pupil_activity_pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(pupil_activity_tabs_layout))

        pupil_activity_tabs_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                pupil_activity_pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == 0) return

        val pupil = getPupil()

        when (requestCode) {
            RequestCodes.REQUEST_CODE_PHOTO_FROM_GALLERY -> {
                if (data != null) {
                    try {
                        val bitmap = PhotoLoaderUtils.getBitmapFromGalleryUri(contentResolver, data.data)
                        pupil_avatar.setImageBitmap(bitmap)
                        run {
                            val newPath = PhotoLoaderUtils.saveImage(bitmap, this)
                            pupil.photo?.let { PhotoLoaderUtils.deleteImage(it) }
                            pupil.photo = newPath
                            pupilViewModel.updatePupil(pupil)
                        }
                        Toast.makeText(this, getString(R.string.ui_image_saved), Toast.LENGTH_SHORT).show()
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Toast.makeText(this, getString(R.string.ui_failed), Toast.LENGTH_SHORT).show()
                    }
                }
            }
            RequestCodes.REQUEST_CODE_PHOTO_FROM_CAMERA -> {
                try {
                    val thumbnail = data!!.extras!!.get("data") as Bitmap
                    val path = PhotoLoaderUtils.saveImage(thumbnail, this)
                    pupil_avatar.setImageBitmap(thumbnail)
                    pupil.photo = path
                    pupilViewModel.updatePupil(pupil)
                    Toast.makeText(this, getString(R.string.ui_image_saved), Toast.LENGTH_SHORT).show()
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this, getString(R.string.ui_failed), Toast.LENGTH_SHORT).show()
                }
            }
            RequestCodes.REQUEST_CODE_NAME -> {
                val fullName = data?.getParcelableExtra<NameDto>(getString(R.string.intent_new_value))!!
                pupil.firstName = fullName.firstName
                pupil.secondName = fullName.secondName
                pupil.lastName = fullName.lastName
                pupilViewModel.updatePupil(pupil)
            }
            RequestCodes.REQUEST_CODE_BIRTHDAY -> {
                assert(false) { "Should not be the case any more!!! Use ${DateInputOnClickListener::class.java} instead." }
            }

        }
    }
}
