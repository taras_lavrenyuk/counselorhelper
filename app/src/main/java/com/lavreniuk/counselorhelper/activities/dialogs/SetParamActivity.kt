package com.lavreniuk.counselorhelper.activities.dialogs

import android.app.Activity
import android.os.Bundle
import android.text.InputFilter
import android.widget.ArrayAdapter
import android.widget.Toast
import com.itextpdf.text.log.LoggerFactory
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.listeners.DateInputOnClickListener
import com.lavreniuk.counselorhelper.utils.RequestCodes
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import kotlinx.android.synthetic.main.activity_set_param.*
import java.util.*


class SetParamActivity : Activity() {

    private val logger = LoggerFactory.getLogger(SetParamActivity::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_param)

        val intent = intent
        info_view_label.text = intent.getStringExtra(getString(R.string.intent_info_label_text))

        val oldValue = intent.getSerializableExtra(getString(R.string.intent_old_value))
        if (oldValue != null) {
            when (oldValue) {
                is Date -> {
                    logger.warn("Should not be the case any more!!! Use ${DateInputOnClickListener::class.java} instead.")
                }
                is String -> info_input_field.setText(oldValue.toString())
                else -> logger.error("Process new datatype!!!")
            }
        }

        val requestCode = intent.getSerializableExtra(getString(R.string.intent_request_code))
        if (requestCode != null)
            when (requestCode) {
                RequestCodes.REQUEST_CODE_BIO ->
                    info_input_field.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(140))
                RequestCodes.REQUEST_CODE_POSITION -> {
                    val adapter = ArrayAdapter<String>(
                        this,
                        android.R.layout.simple_dropdown_item_1line,
                        resources.getStringArray(R.array.positions_suggestions)
                    )
                    info_input_field.threshold = 1
                    info_input_field.setAdapter<ArrayAdapter<String>>(adapter)
                }
            }

        ok_button.setOnClickListener {
            val text = info_input_field.text
            if (text.isBlank()) {
                Toast.makeText(this, getString(R.string.ui_field_cannot_be_empty), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (requestCode == RequestCodes.REQUEST_CODE_BIRTHDAY) {
                try {
                    intent.putExtra(
                        getString(R.string.intent_new_value),
                        StringTransformationUtils.fromStringToDate(text.toString())
                    )
                } catch (exception: NumberFormatException) {
                    exception.printStackTrace()
                    Toast.makeText(this, getString(R.string.ui_incorrect_value), Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
            } else {
                intent.putExtra(getString(R.string.intent_new_value), text)
            }
            setResult(RESULT_OK, intent)
            finish()
        }

        cancel_button.setOnClickListener {
            setResult(RESULT_CANCELED, intent)
            finish()
        }
    }

}
