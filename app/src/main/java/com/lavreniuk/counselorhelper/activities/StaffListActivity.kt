package com.lavreniuk.counselorhelper.activities

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.widget.SearchView
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.dialogs.StaffEditorActivity
import com.lavreniuk.counselorhelper.itemviews.dragitemviewadapters.StaffDragAdapter
import com.lavreniuk.counselorhelper.models.entities.Staff
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import com.lavreniuk.counselorhelper.viewmodels.StaffViewModel
import com.woxthebox.draglistview.DragListView
import kotlinx.android.synthetic.main.activity_staff_list.*
import kotlin.concurrent.thread


class StaffListActivity : AppCompatActivity() {

    private val staffViewModel: StaffViewModel by lazy {
        ViewModelProviders.of(this).get(StaffViewModel::class.java)
    }

    private var searchQuery = ""
    private var staffList: List<Staff> = emptyList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_staff_list)

        add_staff_param_button.setOnClickListener {
            val intent = Intent(this, StaffEditorActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        UtilsMethods.hideKeyboard(this)
        staffList = staffViewModel.getStaffSortedByOrderInList()
        fillStaffList()
    }

    private fun fillStaffList() {
        val filteredStaffList = staffList.filter {
            it.firstName.contains(searchQuery, true)
                .or(it.lastName != null && it.lastName!!.contains(searchQuery, true))
                .or(it.secondName != null && it.secondName!!.contains(searchQuery, true))
                .or(it.bio != null && it.bio!!.contains(searchQuery, true))
                .or(it.position != null && it.position!!.contains(searchQuery, true))
        }

        val listAdapter = StaffDragAdapter(
            filteredStaffList.toTypedArray(),
            this,
            { onResume() },
            { staffViewModel.deleteStaff(it) }
        )

        val dragAndDropListener = object : DragListView.DragListListener {
            override fun onItemDragging(itemPosition: Int, x: Float, y: Float) {
            }

            override fun onItemDragStarted(position: Int) {
            }

            override fun onItemDragEnded(fromPosition: Int, toPosition: Int) {
                if (fromPosition != toPosition) {
                    thread(start = true) {
                        val staffIdOnPosition =
                            { position: Int -> listAdapter.getStaffIdOnPosition(position) }
                        if (fromPosition > toPosition) {
                            staffViewModel.updateStaffOrder(toPosition, fromPosition, staffIdOnPosition)
                        } else {
                            staffViewModel.updateStaffOrder(fromPosition, toPosition, staffIdOnPosition)
                        }
                    }
                }
            }
        }

        staff_draganddrop_list.setDragListListener(dragAndDropListener)
        staff_draganddrop_list.setLayoutManager(LinearLayoutManager(this))

        staff_draganddrop_list.setAdapter(listAdapter, true)
        staff_draganddrop_list.setCanDragHorizontally(false)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        super.onPrepareOptionsMenu(menu)
        menu!!.clear()

        menuInflater.inflate(R.menu.search_menu, menu)
        val menuItem = menu.findItem(R.id.search_menu)
        val searchView = menuItem.actionView as SearchView

        searchView.queryHint = resources.getString(R.string.ui_search)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                val query = newText
                searchQuery =
                    if (query == "null" || query.isBlank()) ""
                    else query

                fillStaffList()
                return false
            }
        })
        return true
    }
}
