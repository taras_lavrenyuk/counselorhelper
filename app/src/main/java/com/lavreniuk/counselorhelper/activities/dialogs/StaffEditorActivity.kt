package com.lavreniuk.counselorhelper.activities.dialogs

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.StaffPageActivity
import com.lavreniuk.counselorhelper.models.dtos.NameDto
import kotlinx.android.synthetic.main.activity_staff_editor.*

class StaffEditorActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_staff_editor)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        staff_first_name_input_field.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(40))
        staff_second_name_input_field.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(40))
        staff_last_name_input_field.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(40))

        val adapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_dropdown_item_1line,
            resources.getStringArray(R.array.positions_suggestions)
        )
        staff_position_input_field.threshold = 1
        staff_position_input_field.setAdapter<ArrayAdapter<String>>(adapter)

        staff_editor_activity_submit_button.setOnClickListener {
            if (staff_first_name_input_field.text.isBlank()) {
                Toast.makeText(this, getString(R.string.ui_field_first_name_cannot_be_empty), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (staff_position_input_field.text.isBlank()) {
                staff_position_input_field.setText("")
            }
            if (staff_second_name_input_field.text.isBlank()) {
                staff_second_name_input_field.setText("")
            }
            if (staff_last_name_input_field.text.isBlank()) {
                staff_last_name_input_field.setText("")
            }
            if (staff_additional_info_input_field.text.isBlank()) {
                staff_additional_info_input_field.setText("")
            }


            val staffName = NameDto(
                staff_first_name_input_field.text.toString(),
                staff_second_name_input_field.text.toString(),
                staff_last_name_input_field.text.toString()
            )

            val staffPosition = staff_position_input_field.text.toString().let { if (it.isBlank()) null else it }
            val staffAdditionalInfo = staff_additional_info_input_field.text.toString().let { if (it.isBlank()) null else it }

            val intent = Intent(this, StaffPageActivity::class.java)

            intent.putExtra(getString(R.string.intent_new_or_existing), getString(R.string.intent_new))
            intent.putExtra(getString(R.string.intent_staff_name), staffName)
            intent.putExtra(getString(R.string.intent_staff_position), staffPosition)
            intent.putExtra(getString(R.string.intent_staff_additional_info), staffAdditionalInfo)

            startActivity(intent)
            finish()
        }

        staff_editor_activity_cancel_button.setOnClickListener {
            setResult(RESULT_CANCELED, intent)
            finish()
        }

    }
}
