package com.lavreniuk.counselorhelper.activities

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.listeners.CustomOnTouchListenerForEditTextView
import com.lavreniuk.counselorhelper.listeners.DateInputOnClickListener
import com.lavreniuk.counselorhelper.models.entities.User
import com.lavreniuk.counselorhelper.utils.Converters
import com.lavreniuk.counselorhelper.utils.PreferencesUtilsMethods
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import com.lavreniuk.counselorhelper.viewmodels.UserViewModel
import kotlinx.android.synthetic.main.activity_welcome.*
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class WelcomeActivity : AppCompatActivity() {

    private val userViewModel: UserViewModel by lazy {
        ViewModelProviders.of(this).get(UserViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        PreferencesUtilsMethods.clearData(this)

        birthday_input_field.setOnClickListener(DateInputOnClickListener(this))
        birthday_input_field.setOnTouchListener(CustomOnTouchListenerForEditTextView())

        submit_userinfo_button.setOnClickListener {
            val fullname = fullname_input_field.text.toString()
            if (fullname.isBlank()) {
                Toast.makeText(applicationContext, "Incorrect name", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (birthday_input_field.text.isBlank()) {
                Toast.makeText(applicationContext, "Input you birthday", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val birthday: Date
            try {
                birthday = StringTransformationUtils.fromStringToDate(birthday_input_field.text.toString())
            } catch (exception: NumberFormatException) {
                exception.printStackTrace()
                Toast.makeText(this, getString(R.string.ui_incorrect_value), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (Date() < birthday) {
                Toast.makeText(applicationContext, "Incorrect birthday", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val userId = UUID.randomUUID().toString()
            PreferencesUtilsMethods.setUserId(this, userId)

            GlobalScope.launch(Dispatchers.Default, CoroutineStart.DEFAULT) {
                userViewModel.addUserInfo(
                    User(
                        userId,
                        fullname,
                        birthday
                    )
                )
            }
            startActivity(Intent(applicationContext, HomeActivity::class.java))
            finish()
        }
    }
}
