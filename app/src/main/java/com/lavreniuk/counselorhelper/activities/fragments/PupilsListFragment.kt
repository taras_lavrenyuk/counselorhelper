package com.lavreniuk.counselorhelper.activities.fragments

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itextpdf.text.log.LoggerFactory
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.PupilActivity
import com.lavreniuk.counselorhelper.activities.SelectExistingPupilActivity
import com.lavreniuk.counselorhelper.activities.SquadActivity
import com.lavreniuk.counselorhelper.activities.dialogs.EnterPupilsNameActivity
import com.lavreniuk.counselorhelper.itemviews.itemviewadapters.PupilAdapter
import com.lavreniuk.counselorhelper.models.entities.Pupil
import com.lavreniuk.counselorhelper.utils.PreferencesUtilsMethods
import com.lavreniuk.counselorhelper.utils.RequestCodes
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import com.lavreniuk.counselorhelper.viewmodels.PupilViewModel
import kotlinx.android.synthetic.main.fragment_pupils_list.*
import kotlinx.android.synthetic.main.fragment_pupils_list.view.*


class PupilsListFragment : Fragment() {

    private val pupilViewModel: PupilViewModel by lazy { ViewModelProviders.of(this).get(PupilViewModel::class.java) }

    private var logger = LoggerFactory.getLogger(PupilsListFragment::class.java)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_pupils_list, container, false)

        view.create_new_pupil_menu_button.setOnClickListener {
            val intent = Intent(activity as SquadActivity, EnterPupilsNameActivity::class.java)
            add_pupil_floating_param_button.close(false)
            startActivityForResult(intent, RequestCodes.REQUEST_CODE_ENTER_PUPILS_NAME)
        }

        view.select_existing_pupil_menu_button.setOnClickListener {
            val intent = Intent(activity as SquadActivity, SelectExistingPupilActivity::class.java)
            add_pupil_floating_param_button.close(false)
            intent.putExtra(getString(R.string.intent_previous_activity), this.javaClass.name)
            startActivityForResult(intent, RequestCodes.REQUEST_CODE_GET_EXISTING_PUPIL_ID)
        }

        view.pupils_list_view.setOnItemClickListener { parent, list_view, position, id ->
            val pupil = pupils_list_view.adapter.getItem(position)
            if (pupil !is Pupil) {
                logger.error("$pupil is not a ${Pupil::class} instance")
                return@setOnItemClickListener
            }
            val intent = Intent(activity as SquadActivity, PupilActivity::class.java)
            intent.putExtra(getString(R.string.intent_pupil_id), pupil.pupilId)
            startActivity(intent)
        }


        return view
    }

    override fun onResume() {
        super.onResume()
        fillPupilsList()
    }


    private fun fillPupilsList() {
        val pupils =
            pupilViewModel.getPupilsBySquadIdOrderByLastName(PreferencesUtilsMethods.getCurrentSquadId(context!!)!!)
        pupils_list_view.adapter =
            PupilAdapter(context!!, pupils.toTypedArray())
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_CANCELED) return

        when (requestCode) {
            RequestCodes.REQUEST_CODE_ENTER_PUPILS_NAME -> {
                val pupilsFirstName = data?.extras?.get(getString(R.string.intent_pupils_first_name)).toString()
                val pupilsLastName = data?.extras?.get(getString(R.string.intent_pupils_last_name)).toString()
                val newPupilsSquadsList = listOf(PreferencesUtilsMethods.getCurrentSquadId(this.context!!)!!)
                val newPupil = Pupil(
                    UtilsMethods.generateId(),
                    pupilsFirstName,
                    pupilsLastName,
                    newPupilsSquadsList
                )
                pupilViewModel.savePupil(newPupil)
            }
        }
    }
}