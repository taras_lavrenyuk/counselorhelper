package com.lavreniuk.counselorhelper.activities

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.AlertDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.widget.ImageView
import android.widget.Toast
import com.itextpdf.text.log.LoggerFactory
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.dialogs.ContactEditorActivity
import com.lavreniuk.counselorhelper.activities.dialogs.SetNameActivity
import com.lavreniuk.counselorhelper.activities.dialogs.SetParamActivity
import com.lavreniuk.counselorhelper.itemviews.itemviewadapters.ContactAdapter
import com.lavreniuk.counselorhelper.listeners.CustomOnTouchListenerForEditTextView
import com.lavreniuk.counselorhelper.listeners.DateInputOnClickListener
import com.lavreniuk.counselorhelper.models.UserWithContacts
import com.lavreniuk.counselorhelper.models.dtos.NameDto
import com.lavreniuk.counselorhelper.models.entities.Contact
import com.lavreniuk.counselorhelper.models.entities.User
import com.lavreniuk.counselorhelper.utils.*
import com.lavreniuk.counselorhelper.viewmodels.ContactViewModel
import com.lavreniuk.counselorhelper.viewmodels.UserViewModel
import kotlinx.android.synthetic.main.activity_account_settings.*
import org.joda.time.DateTimeComparator
import java.io.IOException


class AccountSettingsActivity : AppCompatActivity() {

    private val userViewModel: UserViewModel by lazy {
        ViewModelProviders.of(this).get(UserViewModel::class.java)
    }
    private val contactViewModel: ContactViewModel by lazy {
        ViewModelProviders.of(this).get(ContactViewModel::class.java)
    }

    private val logger = LoggerFactory.getLogger(AccountSettingsActivity::class.java)

    @TargetApi(Build.VERSION_CODES.O)
    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_settings)

        name_textview.setOnClickListener {
            val user = getUser()
            callSetNameActivity(
                NameDto(user.firstName, user.secondName, user.lastName)
            )
        }

        bio_textview.setOnClickListener {
            callSetParamActivity(
                InputType.TYPE_CLASS_TEXT,
                RequestCodes.REQUEST_CODE_BIO,
                "Your bio",
                getBioText()
            )
        }

        birthday_textview.setOnClickListener(DateInputOnClickListener(this))
        birthday_textview.setOnTouchListener(CustomOnTouchListenerForEditTextView())

        birthday_textview.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                Thread().run {
                    val newBirthday = StringTransformationUtils.fromStringToDate(birthday_textview.text.toString())
                    val user = getUser()
                    // if two dates are equals => do not update user.birthday
                    if (DateTimeComparator.getDateOnlyInstance().compare(newBirthday, user.birthday) == 0) {
                        return@run
                    }
                    user.birthday = newBirthday
                    userViewModel.updateUser(user)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        avatar.setOnClickListener {
            if (PermissionChecker.checkAndRequestCameraPermissions(this)) {
                val avatarChangeDialog = AlertDialog.Builder(this)
                avatarChangeDialog.setTitle(getString(R.string.ui_select_action))
                var pictureDialogItems = arrayOf(
                    getString(R.string.ui_avatar_from_galery),
                    getString(R.string.ui_avatar_from_camera)
                )
                val user = getUser()
                val avatarPath = user.photo
                if (avatarPath != null) {
                    pictureDialogItems = pictureDialogItems.plus(getString(R.string.ui_avatar_delete))
                }
                avatarChangeDialog.setItems(
                    pictureDialogItems
                ) { _, which ->
                    when (which) {
                        0 -> PhotoLoaderUtils.choosePhotoFromGallery(this)
                        1 -> PhotoLoaderUtils.takePhotoFromCamera(this)
                        2 -> deleteUserAvatar(avatarPath, user, avatar)
                        else -> logger.error("Wrong argument")
                    }
                }
                avatarChangeDialog.show()
            }
        }

        add_contact_button.setOnClickListener {
            val intent = Intent(this, ContactEditorActivity::class.java)
            intent.putExtra(getString(R.string.intent_add_or_edit), getString(R.string.intent_add))
            intent.putExtra(getString(R.string.intent_person_id), PreferencesUtilsMethods.getUserId(this, false))
            startActivityForResult(intent, RequestCodes.REQUEST_CODE_EDIT_CONTACT)
        }

        contacts_list_view.setOnItemLongClickListener { _, _, position, _ ->
            val contact = contacts_list_view.adapter.getItem(position)
            if (contact !is Contact) {
                logger.error("$contact is not a ${Contact::class} instance")
                return@setOnItemLongClickListener false
            }
            val contactChangeDialog = AlertDialog.Builder(this)
            contactChangeDialog.setTitle(getString(R.string.ui_select_action))
            val contactChangeDialogItems =
                arrayOf(
                    getString(R.string.ui_contact_popupmenu_edit),
                    getString(R.string.ui_contact_popupmenu_delete),
                    getString(R.string.ui_copy_value)
                )
            contactChangeDialog.setItems(
                contactChangeDialogItems
            ) { _, which ->
                when (which) {
                    0 -> editContact(contact)
                    1 -> deleteContact(contact)
                    2 -> copyContactValueToClipboard(contact)
                    else -> logger.error("Wrong argument")
                }
            }
            contactChangeDialog.show()

            return@setOnItemLongClickListener true
        }
    }

    private fun deleteUserAvatar(avatarPath: String?, user: User, avatarView: ImageView) {
        avatarView.setImageDrawable(getDrawable(R.mipmap.ic_launcher_round))

        run {
            avatarPath?.let {
                PhotoLoaderUtils.deleteImage(it)
                user.photo = null
                userViewModel.updateUser(user)
            }
        }
    }

    private fun copyContactValueToClipboard(contact: Contact) {
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Contact", contact.value)
        clipboard.primaryClip = clip
    }

    private fun deleteContact(contact: Contact) {
        contactViewModel.deleteContact(contact)
        fillContactsList(contactViewModel.getUserContacts(PreferencesUtilsMethods.getUserId(this, false)))
    }

    private fun editContact(contact: Contact) {
        val intent = Intent(this, ContactEditorActivity::class.java)
        intent.putExtra(getString(R.string.intent_add_or_edit), getString(R.string.intent_edit))
        intent.putExtra(getString(R.string.intent_person_id), PreferencesUtilsMethods.getUserId(this, false))
        intent.putExtra(getString(R.string.intent_contact_value), contact.value)
        intent.putExtra(getString(R.string.intent_contact_type), contact.contactType)
        intent.putExtra(getString(R.string.intent_contact_id), contact.contactId)
        startActivityForResult(intent, RequestCodes.REQUEST_CODE_EDIT_CONTACT)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        PermissionChecker.customOnRequestCameraPermissionsResult(
            requestCode,
            permissions as Array<String>,
            grantResults,
            this
        )
    }

    override fun onResume() {
        super.onResume()
        val user = getUserWithContacts()
        name_textview.text = StringTransformationUtils.getFullNameString(user.user!!, true)
        bio_textview.text = user.user!!.bio
        birthday_textview.text = StringTransformationUtils.fromDateToString(user.user!!.birthday!!)

        val image = PhotoLoaderUtils.getBitmapFromPath(user.user!!.photo)
        if (image != null) avatar.setImageBitmap(image)

        fillContactsList(user.contacts)
    }

    private fun fillContactsList(contacts: List<Contact>) {
        contacts_list_view.adapter = ContactAdapter(
            applicationContext,
            contacts.toTypedArray()
        )
    }

    private fun getUserWithContacts(): UserWithContacts {
        val userId = PreferencesUtilsMethods.getUserId(this, false)
        return userViewModel.getUserWithContacts(userId)!!
    }

    private fun getUser(): User {
        val userId = PreferencesUtilsMethods.getUserId(this, false)
        return userViewModel.getUser(userId)!!
    }

    private fun getBioText(): String {
        return bio_textview.text.toString()
    }

    private fun callSetParamActivity(
        inputFieldType: Int,
        requestCode: Int,
        infoLabelText: String,
        oldBio: String
    ) {
        val intent = Intent(this, SetParamActivity::class.java)
        intent.putExtra(getString(R.string.intent_input_field_type), inputFieldType)
        intent.putExtra(getString(R.string.intent_old_value), oldBio)
        intent.putExtra(getString(R.string.intent_info_label_text), infoLabelText)
        intent.putExtra(getString(R.string.intent_request_code), requestCode)
        startActivityForResult(intent, requestCode)
    }

    private fun callSetNameActivity(
        oldName: NameDto
    ) {
        val intent = Intent(this, SetNameActivity::class.java)
        intent.putExtra(getString(R.string.intent_old_value), oldName)
        startActivityForResult(intent, RequestCodes.REQUEST_CODE_NAME)
    }

    @TargetApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == 0) return
        val user = getUser()
        when (requestCode) {
            RequestCodes.REQUEST_CODE_BIRTHDAY -> {
                logger.warn("Should not be the case any more!!! Use ${DateInputOnClickListener::class.java} instead.")
            }
            RequestCodes.REQUEST_CODE_BIO -> {
                user.bio = data?.extras?.get(getString(R.string.intent_new_value)).toString()
                userViewModel.updateUser(user)
            }
            RequestCodes.REQUEST_CODE_NAME -> {
                val fullName = data?.getParcelableExtra<NameDto>(getString(R.string.intent_new_value))!!
                user.firstName = fullName.firstName
                user.secondName = fullName.secondName
                user.lastName = fullName.lastName
                userViewModel.updateUser(user)
            }
            RequestCodes.REQUEST_CODE_PHOTO_FROM_GALLERY -> {
                if (data != null) {
                    try {
                        val bitmap = PhotoLoaderUtils.getBitmapFromGalleryUri(contentResolver, data.data)
                        val newPath = PhotoLoaderUtils.saveImage(bitmap, applicationContext)
                        avatar.setImageBitmap(bitmap)
                        user.photo?.let { PhotoLoaderUtils.deleteImage(it) }
                        user.photo = newPath
                        Toast.makeText(applicationContext, getString(R.string.ui_image_saved), Toast.LENGTH_SHORT)
                            .show()
                    } catch (e: IOException) {
                        e.printStackTrace()
                        Toast.makeText(applicationContext, getString(R.string.ui_failed), Toast.LENGTH_SHORT).show()
                    }
                }
                userViewModel.updateUser(user)
            }
            RequestCodes.REQUEST_CODE_PHOTO_FROM_CAMERA -> {
                try {
                    val thumbnail = data!!.extras!!.get("data") as Bitmap
                    val newPath = PhotoLoaderUtils.saveImage(thumbnail, applicationContext)
                    avatar.setImageBitmap(thumbnail)
                    val oldPath = user.photo
                    oldPath?.let {
                        run {
                            PhotoLoaderUtils.deleteImage(it)
                        }
                    }
                    user.photo = newPath
                    Toast.makeText(applicationContext, getString(R.string.ui_image_saved), Toast.LENGTH_SHORT).show()
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(applicationContext, getString(R.string.ui_failed), Toast.LENGTH_SHORT).show()
                }
                userViewModel.updateUser(user)
            }
            RequestCodes.REQUEST_CODE_EDIT_CONTACT -> {
                val actionType = data?.extras?.get(getString(R.string.intent_add_or_edit)).toString()
                val contact: Contact = IntentBoxingUtils.extractContactInfoFromIntent(data, applicationContext)
                when (actionType) {
                    getString(R.string.intent_add) -> contactViewModel.saveContact(contact)
                    getString(R.string.intent_edit) -> contactViewModel.updateContact(contact)
                    else -> logger.error("Unknown isSomeActivityWillBeStarted!!! Process!!!")
                }
            }
            else -> throw Exception()
        }
    }

}
