package com.lavreniuk.counselorhelper.activities.dialogs

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.listeners.CustomOnTouchListenerForEditTextView
import com.lavreniuk.counselorhelper.listeners.TimeInputOnClickListener
import com.lavreniuk.counselorhelper.watchers.TimeEntryTextWatcher
import kotlinx.android.synthetic.main.activity_program_editor.*

class ProgramActivityEditorActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_program_editor)

        val intent = intent

        program_activity_start_time_input_field.setOnClickListener(
            TimeInputOnClickListener(
                this,
                program_activity_start_time_input_field
            )
        )
        program_activity_start_time_input_field.setOnTouchListener(CustomOnTouchListenerForEditTextView())

        program_activity_end_time_input_field.setOnClickListener(
            TimeInputOnClickListener(
            this,
                program_activity_end_time_input_field
            )
        )
        program_activity_end_time_input_field.setOnTouchListener(CustomOnTouchListenerForEditTextView())

        program_activity_remove_start_time_icon.setOnClickListener {
            program_activity_start_time_input_field.setText("")
        }
        program_activity_remove_end_time_icon.setOnClickListener {
            program_activity_end_time_input_field.setText("")
        }

        val actionType = intent.getStringExtra(getString(R.string.intent_add_or_edit))
        if (actionType == getString(R.string.intent_edit)) {
            program_activity_name_input_field.setText(intent.getStringExtra(getString(R.string.intent_program_activity_name)))
            program_activity_description_input_field.setText(intent.getStringExtra(getString(R.string.intent_program_activity_description)))
            program_activity_start_time_input_field.setText(intent.getStringExtra(getString(R.string.intent_program_activity_start_time)))
            program_activity_end_time_input_field.setText(intent.getStringExtra(getString(R.string.intent_program_activity_end_time)))
        } else if (actionType != getString(R.string.intent_add)) {
            assert(false) { "Unknown isSomeActivityWillBeStarted!!!" }
        }

        program_activity_submit_button.setOnClickListener {
            val programActivitiesNames = intent.getStringArrayExtra(getString(R.string.intent_program_activities_names))
            val name = program_activity_name_input_field.text.toString()
            val programActivityId = intent.getStringExtra(getString(R.string.intent_program_activity_id))
            if (programActivitiesNames.contains(name) && programActivityId == null) {
                Toast.makeText(this, getString(R.string.ui_activity_name_is_already_in_use), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (name.isEmpty() || name.isBlank()) {
                program_activity_name_input_field.setText("")
                Toast.makeText(this, getString(R.string.ui_enter_activity_name), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val startTime = program_activity_start_time_input_field.text.toString()
            val endTime = program_activity_end_time_input_field.text.toString()
            if (startTime.isNotEmpty() && !startTime.matches(Regex(TimeEntryTextWatcher.TIME_REGEX))) {
                Toast.makeText(this, getString(R.string.ui_correct_start_time), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (endTime.isNotEmpty() && !endTime.matches(Regex(TimeEntryTextWatcher.TIME_REGEX))) {
                Toast.makeText(this, getString(R.string.ui_correct_end_time), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            intent.putExtra(getString(R.string.intent_program_activity_name), name.trimEnd())
            intent.putExtra(getString(R.string.intent_program_activity_start_time), startTime)
            intent.putExtra(getString(R.string.intent_program_activity_end_time), endTime)
            intent.putExtra(
                getString(R.string.intent_program_activity_description),
                program_activity_description_input_field.text.toString().trimEnd()
            )

            setResult(RESULT_OK, intent)
            finish()
        }

        program_activity_cancel_button.setOnClickListener {
            setResult(RESULT_CANCELED, intent)
            finish()
        }
    }

}
