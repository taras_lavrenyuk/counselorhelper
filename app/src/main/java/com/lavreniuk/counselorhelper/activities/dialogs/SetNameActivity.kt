package com.lavreniuk.counselorhelper.activities.dialogs

import android.app.Activity
import android.os.Bundle
import android.text.InputFilter
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.models.dtos.NameDto
import kotlinx.android.synthetic.main.activity_set_name.*

class SetNameActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_name)

        first_name_input_field.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(40))
        second_name_input_field.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(40))
        last_name_input_field.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(40))

        val name = intent.getParcelableExtra<NameDto>(getString(R.string.intent_old_value))

        first_name_input_field.setText(name.firstName)
        second_name_input_field.setText(name.secondName)
        last_name_input_field.setText(name.lastName)

        set_name_activity_ok_button.setOnClickListener {
            if (first_name_input_field.text.isBlank()) {
                Toast.makeText(this, getString(R.string.ui_field_cannot_be_empty), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (second_name_input_field.text.isBlank()) {
                second_name_input_field.setText("")
            }
            if (last_name_input_field.text.isBlank()) {
                last_name_input_field.setText("")
            }
            val result = NameDto(
                first_name_input_field.text.toString(),
                second_name_input_field.text.toString(),
                last_name_input_field.text.toString()
            )
            intent.putExtra(getString(R.string.intent_new_value), result)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

        set_name_activity_cancel_button.setOnClickListener {
            setResult(Activity.RESULT_CANCELED, intent)
            finish()
        }

    }
}
