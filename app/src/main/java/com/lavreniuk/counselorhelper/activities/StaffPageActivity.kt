package com.lavreniuk.counselorhelper.activities

import android.app.AlertDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.dialogs.ContactEditorActivity
import com.lavreniuk.counselorhelper.activities.dialogs.SetNameActivity
import com.lavreniuk.counselorhelper.activities.dialogs.SetParamActivity
import com.lavreniuk.counselorhelper.enums.ContactType
import com.lavreniuk.counselorhelper.itemviews.itemviewadapters.ContactAdapter
import com.lavreniuk.counselorhelper.models.dtos.NameDto
import com.lavreniuk.counselorhelper.models.entities.Contact
import com.lavreniuk.counselorhelper.models.entities.Staff
import com.lavreniuk.counselorhelper.utils.*
import com.lavreniuk.counselorhelper.viewmodels.ContactViewModel
import com.lavreniuk.counselorhelper.viewmodels.StaffViewModel
import kotlinx.android.synthetic.main.activity_staff_page.*
import java.io.IOException


class StaffPageActivity : AppCompatActivity() {

    private val staffViewModel: StaffViewModel by lazy {
        ViewModelProviders.of(this).get(StaffViewModel::class.java)
    }

    private val contactViewModel: ContactViewModel by lazy {
        ViewModelProviders.of(this).get(ContactViewModel::class.java)
    }

    lateinit var currentStaff: Staff

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_staff_page)

        val newOrExisting = intent.getStringExtra(getString(R.string.intent_new_or_existing))
        if (newOrExisting == getString(R.string.intent_new)) {
            val staffPosition = intent.getStringExtra(getString(R.string.intent_staff_position))
            val staffAddInfo = intent.getStringExtra(getString(R.string.intent_staff_additional_info))
            val staffName = intent.getParcelableExtra<NameDto>(getString(R.string.intent_staff_name))!!

            val newStaff = Staff(
                UtilsMethods.generateId(),
                staffName.firstName,
                staffName.secondName,
                staffName.lastName,
                staffPosition,
                staffAddInfo,
                staffViewModel.getOrderInListForNewElement()
            )
            currentStaff = newStaff
            staffViewModel.saveStaff(
                newStaff
            )
        } else if (newOrExisting == getString(R.string.intent_existing)) {
            currentStaff = staffViewModel.getStaffById(intent.getStringExtra(getString(R.string.intent_staff_id)))
        }

        // avatar editing
        staff_avatar.setOnClickListener {
            if (PermissionChecker.checkAndRequestCameraPermissions(this)) {
                val avatarChangeDialog = AlertDialog.Builder(this)
                avatarChangeDialog.setTitle(getString(R.string.ui_select_action))
                var pictureDialogItems = arrayOf(
                    getString(R.string.ui_avatar_from_galery),
                    getString(R.string.ui_avatar_from_camera)
                )
                val avatarPath = currentStaff.photo
                if (avatarPath != null) {
                    pictureDialogItems = pictureDialogItems.plus(getString(R.string.ui_avatar_delete))
                }
                avatarChangeDialog.setItems(
                    pictureDialogItems
                ) { _, which ->
                    when (which) {
                        0 -> PhotoLoaderUtils.choosePhotoFromGallery(this)
                        1 -> PhotoLoaderUtils.takePhotoFromCamera(this)
                        2 -> deleteStaffAvatar(avatarPath)
                        else -> assert(false) { "Wrong argument" }
                    }
                }
                avatarChangeDialog.show()
            }
        }

        staff_name_textview.setOnClickListener {
            callSetNameActivity(
                NameDto(currentStaff.firstName, currentStaff.secondName, currentStaff.lastName)
            )
        }

        staff_add_info_textview.setOnClickListener {
            callSetParamActivity(
                InputType.TYPE_CLASS_TEXT,
                RequestCodes.REQUEST_CODE_BIO,
                "Additional info",
                currentStaff.bio
            )
        }

        staff_position_textview.setOnClickListener {
            callSetParamActivity(
                InputType.TYPE_CLASS_TEXT,
                RequestCodes.REQUEST_CODE_POSITION,
                "Position",
                currentStaff.position
            )
        }

        add_staff_contact_param_button.setOnClickListener {
            val intent = Intent(this, ContactEditorActivity::class.java)
            intent.putExtra(getString(R.string.intent_add_or_edit), getString(R.string.intent_add))
            intent.putExtra(getString(R.string.intent_person_id), currentStaff.staffId)
            startActivityForResult(intent, RequestCodes.REQUEST_CODE_EDIT_CONTACT)
        }

        staff_contact_list_view.setOnItemLongClickListener { _, _, position, _ ->
            val contact = staff_contact_list_view.adapter.getItem(position)
            if (contact !is Contact) {
                assert(false) { "$contact is not a ${Contact::class} instance" }
                return@setOnItemLongClickListener false
            }
            val contactChangeDialog = AlertDialog.Builder(this)
            contactChangeDialog.setTitle(getString(R.string.ui_select_action))
            val contactChangeDialogItems =
                mutableListOf(
                    getString(R.string.ui_contact_popupmenu_edit),
                    getString(R.string.ui_contact_popupmenu_delete),
                    getString(R.string.ui_copy_value)
                )
            if (contact.contactType == ContactType.phone) {
                contactChangeDialogItems.add(getString(R.string.ui_call))
            }
            contactChangeDialog.setItems(
                contactChangeDialogItems.toTypedArray()
            ) { _, which ->
                when (which) {
                    0 -> editContact(contact)
                    1 -> deleteContact(contact)
                    2 -> copyContactValueToClipboard(contact)
                    3 -> UtilsMethods.callNumber(contact, this)
                    else -> assert(false) { "Wrong argument" }
                }
            }
            contactChangeDialog.show()

            return@setOnItemLongClickListener true
        }

    }

    override fun onResume() {
        super.onResume()
        staff_name_textview.text = StringTransformationUtils.getFullNameString(currentStaff, true)
        currentStaff.position?.let { staff_position_textview.text = it }
        currentStaff.bio?.let { staff_add_info_textview.text = it }

        val image = PhotoLoaderUtils.getBitmapFromPath(currentStaff.photo)
        if (image != null) staff_avatar.setImageBitmap(image)

        fillPupilContactsList()
    }

    private fun copyContactValueToClipboard(contact: Contact) {
        val clipboard = (this).getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Contact", contact.value)
        clipboard.primaryClip = clip
    }

    private fun deleteContact(contact: Contact) {
        contactViewModel.deleteContact(contact)
        fillPupilContactsList()
    }

    private fun editContact(contact: Contact) {
        val intent = Intent(this, ContactEditorActivity::class.java)
        intent.putExtra(getString(R.string.intent_person_id), currentStaff.staffId)
        intent.putExtra(getString(R.string.intent_add_or_edit), getString(R.string.intent_edit))
        intent.putExtra(getString(R.string.intent_contact_value), contact.value)
        intent.putExtra(getString(R.string.intent_contact_type), contact.contactType)
        intent.putExtra(getString(R.string.intent_contact_id), contact.contactId)
        startActivityForResult(intent, RequestCodes.REQUEST_CODE_EDIT_CONTACT)
    }

    private fun fillPupilContactsList() {
        val staffId = currentStaff.staffId
        val contacts: List<Contact> = contactViewModel.getUserContacts(staffId)
        staff_contact_list_view.adapter = ContactAdapter(
            this,
            contacts.toTypedArray()
        )
    }

    private fun deleteStaffAvatar(avatarPath: String?) {
        staff_avatar.setImageDrawable(getDrawable(R.mipmap.ic_launcher_round))
        currentStaff.photo = null
        avatarPath?.let {
            run {
                PhotoLoaderUtils.deleteImage(it)
                staffViewModel.deleteAvatar(currentStaff.staffId)
            }
        }
    }

    private fun callSetNameActivity(
        oldName: NameDto
    ) {
        val intent = Intent(this, SetNameActivity::class.java)
        intent.putExtra(getString(R.string.intent_old_value), oldName)
        startActivityForResult(intent, RequestCodes.REQUEST_CODE_NAME)
    }

    private fun callSetParamActivity(
        inputFieldType: Int,
        requestCode: Int,
        infoLabelText: String,
        oldValue: String?
    ) {
        val intent = Intent(this, SetParamActivity::class.java)
        intent.putExtra(getString(R.string.intent_input_field_type), inputFieldType)
        intent.putExtra(getString(R.string.intent_old_value), oldValue)
        intent.putExtra(getString(R.string.intent_info_label_text), infoLabelText)
        intent.putExtra(getString(R.string.intent_request_code), requestCode)
        startActivityForResult(intent, requestCode)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        PermissionChecker.customOnRequestCallPhonePermissionsResult(
            requestCode,
            permissions as Array<String>,
            grantResults,
            this
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == 0) return

        when (requestCode) {
            RequestCodes.REQUEST_CODE_PHOTO_FROM_GALLERY -> {
                if (data != null) {
                    try {
                        val bitmap = PhotoLoaderUtils.getBitmapFromGalleryUri(contentResolver, data.data)
                        staff_avatar.setImageBitmap(bitmap)
                        run {
                            val newPath = PhotoLoaderUtils.saveImage(bitmap, this)
                            currentStaff.photo?.let { PhotoLoaderUtils.deleteImage(it) }
                            currentStaff.photo = newPath
                            staffViewModel.setAvatar(currentStaff.staffId, newPath)
                        }
                        Toast.makeText(this, getString(R.string.ui_image_saved), Toast.LENGTH_SHORT).show()
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Toast.makeText(this, getString(R.string.ui_failed), Toast.LENGTH_SHORT).show()
                    }
                }
            }
            RequestCodes.REQUEST_CODE_PHOTO_FROM_CAMERA -> {
                try {
                    val thumbnail = data!!.extras!!.get("data") as Bitmap
                    val path = PhotoLoaderUtils.saveImage(thumbnail, this)
                    staff_avatar.setImageBitmap(thumbnail)
                    currentStaff.photo = path
                    staffViewModel.setAvatar(currentStaff.staffId, path)
                    Toast.makeText(this, getString(R.string.ui_image_saved), Toast.LENGTH_SHORT).show()
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this, getString(R.string.ui_failed), Toast.LENGTH_SHORT).show()
                }
            }
            RequestCodes.REQUEST_CODE_NAME -> {
                val fullName = data?.getParcelableExtra<NameDto>(getString(R.string.intent_new_value))!!
                currentStaff.firstName = fullName.firstName
                currentStaff.secondName = fullName.secondName
                currentStaff.lastName = fullName.lastName
                staffViewModel.updateStaff(currentStaff)
            }
            RequestCodes.REQUEST_CODE_BIO -> {
                val newBio = data?.extras?.get(getString(R.string.intent_new_value)).toString()
                staff_add_info_textview.text = newBio
                currentStaff.bio = newBio
                staffViewModel.updateBio(currentStaff.staffId, newBio)
            }
            RequestCodes.REQUEST_CODE_POSITION -> {
                val newPosition = data?.extras?.get(getString(R.string.intent_new_value)).toString()
                staff_position_textview.text = newPosition
                currentStaff.position = newPosition
                staffViewModel.updatePosition(currentStaff.staffId, newPosition)
            }
            RequestCodes.REQUEST_CODE_EDIT_CONTACT -> {
                val actionType = data?.extras?.get(getString(R.string.intent_add_or_edit)).toString()
                val contact: Contact = IntentBoxingUtils.extractContactInfoFromIntent(data, this)
                when (actionType) {
                    getString(R.string.intent_add) -> contactViewModel.saveContact(contact)
                    getString(R.string.intent_edit) -> contactViewModel.updateContact(contact)
                    else -> assert(false) { "Unknown isSomeActivityWillBeStarted!!! Process!!!" }
                }
            }
        }
    }
}
