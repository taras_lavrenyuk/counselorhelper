package com.lavreniuk.counselorhelper.activities

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.itemviews.itemviewadapters.PupilAdapter
import com.lavreniuk.counselorhelper.utils.PreferencesUtilsMethods
import com.lavreniuk.counselorhelper.viewmodels.PupilViewModel
import com.lavreniuk.counselorhelper.itemviews.PupilItemView
import kotlinx.android.synthetic.main.activity_select_existing_pupil.*


class SelectExistingPupilActivity : AppCompatActivity() {

    private val pupilViewModel: PupilViewModel by lazy { ViewModelProviders.of(this).get(PupilViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_existing_pupil)

        new_pupil_name_input_field.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                val params = getParams(editable!!.toString().split(" "))
                val existingPupils = pupilViewModel.getExistingPupils(
                    PreferencesUtilsMethods.getCurrentSquadId(this@SelectExistingPupilActivity)!!,
                    params
                )
                new_suggested_pupils.adapter =
                    PupilAdapter(
                        this@SelectExistingPupilActivity,
                        existingPupils.toTypedArray()
                    )
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        new_pupils_cancel_button.setOnClickListener {
            finish()
        }

        new_suggested_pupils.setOnItemClickListener { parent, view, position, id ->
            if (view is PupilItemView) {
                val pupilToBeAdded = view.pupil
                pupilToBeAdded.pupilsSquads += PreferencesUtilsMethods.getCurrentSquadId(this@SelectExistingPupilActivity)!!
                pupilViewModel.updatePupil(pupilToBeAdded)
                finish()
            }

        }
    }

    private fun getParams(list: List<String>): List<String> {
        val result = ArrayList<String>()
        list.forEachIndexed { _, s ->
            run {
                if (s.isNotBlank()) {
                    result.add(s)
                }
            }
        }
        return result
    }

    override fun getParentActivityIntent(): Intent? {
        val parentIntent = intent
        val parentClassName = parentIntent.getStringExtra(getString(R.string.intent_previous_activity))

        var newIntent: Intent? = null
        try {
            newIntent = Intent(this@SelectExistingPupilActivity, Class.forName(parentClassName))

        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        }
        return newIntent
    }
}
