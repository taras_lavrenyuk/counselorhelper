package com.lavreniuk.counselorhelper.activities.fragments

import android.app.Activity
import android.app.AlertDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.PupilActivity
import com.lavreniuk.counselorhelper.activities.dialogs.ContactEditorActivity
import com.lavreniuk.counselorhelper.enums.ContactType
import com.lavreniuk.counselorhelper.itemviews.itemviewadapters.ContactAdapter
import com.lavreniuk.counselorhelper.models.entities.Contact
import com.lavreniuk.counselorhelper.utils.IntentBoxingUtils
import com.lavreniuk.counselorhelper.utils.RequestCodes
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import com.lavreniuk.counselorhelper.viewmodels.ContactViewModel
import kotlinx.android.synthetic.main.fragment_pupil_contacts.*
import kotlinx.android.synthetic.main.fragment_pupil_contacts.view.*


class PupilContactsFragment : Fragment() {

    private val contactViewModel: ContactViewModel by lazy {
        ViewModelProviders.of(this).get(ContactViewModel::class.java)
    }

    var pupilId: String = "";

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_pupil_contacts, container, false)

        pupilId = arguments!!.getString("pupil-id").orEmpty()

        view.add_pupil_contact_param_button.setOnClickListener {
            val intent = Intent(context as PupilActivity, ContactEditorActivity::class.java)
            intent.putExtra(getString(R.string.intent_add_or_edit), getString(R.string.intent_add))
            intent.putExtra(getString(R.string.intent_person_id), pupilId)
            startActivityForResult(intent, RequestCodes.REQUEST_CODE_EDIT_CONTACT)
        }

        view.pupil_contacts_list_view.setOnItemLongClickListener { parent, view, position, id ->
            val contact = pupil_contacts_list_view.adapter.getItem(position)
            if (contact !is Contact) {
                assert(false) { "$contact is not a ${Contact::class} instance" }
                return@setOnItemLongClickListener false
            }
            val contactChangeDialog = AlertDialog.Builder(context as PupilActivity)
            contactChangeDialog.setTitle(getString(R.string.ui_select_action))
            val contactChangeDialogItems =
                mutableListOf(
                    getString(R.string.ui_contact_popupmenu_edit),
                    getString(R.string.ui_contact_popupmenu_delete),
                    getString(R.string.ui_copy_value)
                )
            if (contact.contactType == ContactType.phone) {
                contactChangeDialogItems.add(getString(R.string.ui_call))
            }
            contactChangeDialog.setItems(
                contactChangeDialogItems.toTypedArray()
            ) { _, which ->
                when (which) {
                    0 -> editContact(contact)
                    1 -> deleteContact(contact)
                    2 -> copyContactValueToClipboard(contact)
                    3 -> UtilsMethods.callNumber(contact, context as PupilActivity)
                    else -> assert(false) { "Wrong argument" }
                }
            }
            contactChangeDialog.show()

            return@setOnItemLongClickListener true
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        fillPupilContactsList()
    }

    private fun fillPupilContactsList() {
        if (pupilId.isNotBlank()) {
            val contacts: List<Contact> = contactViewModel.getUserContacts(pupilId)
            pupil_contacts_list_view.adapter = ContactAdapter(
                context!!,
                contacts.toTypedArray()
            )
        } else {
            assert(false) { "PupilId is incorrect isSomeActivityWillBeStarted!" }
            pupil_contacts_list_view.adapter = ContactAdapter(
                context!!,
                emptyList<Contact>().toTypedArray()
            )
        }
    }

    private fun copyContactValueToClipboard(contact: Contact) {
        val clipboard = (context as PupilActivity).getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Contact", contact.value)
        clipboard.primaryClip = clip
    }

    private fun deleteContact(contact: Contact) {
        contactViewModel.deleteContact(contact)
        fillPupilContactsList()
    }

    private fun editContact(contact: Contact) {
        val intent = Intent(context as PupilActivity, ContactEditorActivity::class.java)
        intent.putExtra(getString(R.string.intent_person_id), pupilId)
        intent.putExtra(getString(R.string.intent_add_or_edit), getString(R.string.intent_edit))
        intent.putExtra(getString(R.string.intent_contact_value), contact.value)
        intent.putExtra(getString(R.string.intent_contact_type), contact.contactType)
        intent.putExtra(getString(R.string.intent_contact_id), contact.contactId)
        startActivityForResult(intent, RequestCodes.REQUEST_CODE_EDIT_CONTACT)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_CANCELED) return

        when (requestCode) {
            RequestCodes.REQUEST_CODE_EDIT_CONTACT -> {
                val actionType = data?.extras?.get(getString(R.string.intent_add_or_edit)).toString()
                val contact: Contact = IntentBoxingUtils.extractContactInfoFromIntent(data, activity as PupilActivity)
                when (actionType) {
                    getString(R.string.intent_add) -> contactViewModel.saveContact(contact)
                    getString(R.string.intent_edit) -> contactViewModel.updateContact(contact)
                    else -> assert(false) { "Unknown isSomeActivityWillBeStarted!!! Process!!!" }
                }
            }
            else -> assert(false)

        }
    }
}
