package com.lavreniuk.counselorhelper.activities.fragments

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itextpdf.text.log.LoggerFactory
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.PupilActivity
import com.lavreniuk.counselorhelper.activities.dialogs.ParamEditorActivity
import com.lavreniuk.counselorhelper.itemviews.dragitemviewadapters.ParamDragAdapter
import com.lavreniuk.counselorhelper.models.entities.Param
import com.lavreniuk.counselorhelper.utils.IntentBoxingUtils
import com.lavreniuk.counselorhelper.utils.RequestCodes
import com.lavreniuk.counselorhelper.viewmodels.ParamViewModel
import com.woxthebox.draglistview.DragListView
import kotlinx.android.synthetic.main.fragment_info_about_pupil.*
import kotlinx.android.synthetic.main.fragment_info_about_pupil.view.*
import kotlin.concurrent.thread


class InfoAboutPupilFragment : Fragment() {

    private val paramViewModel: ParamViewModel by lazy {
        ViewModelProviders.of(this).get(ParamViewModel::class.java)
    }

    private val logger = LoggerFactory.getLogger(InfoAboutPupilFragment::class.java)

    var pupilId: String = "";

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_info_about_pupil, container, false)

        pupilId = arguments!!.getString("pupil-id").orEmpty()

        view.add_pupil_param_menu_button.setOnClickListener {
            val intent = Intent(activity as PupilActivity, ParamEditorActivity::class.java)
            intent.putExtra(getString(R.string.intent_add_or_edit), getString(R.string.intent_add))
            intent.putExtra(getString(R.string.intent_entity_id), pupilId)
            startActivityForResult(intent, RequestCodes.REQUEST_CODE_PARAM_ADD)
        }

        return view
    }

    override fun onResume() {
        super.onResume()

        if (pupilId.isBlank()) {
            logger.error("PupilId is incorrect!!!")
        }

        setUpSquadParamsList(paramViewModel.getEntityParams(pupilId))
    }

    private fun setUpSquadParamsList(pupilParamsSortedByOrder: List<Param>) {

        val listAdapter = ParamDragAdapter(pupilParamsSortedByOrder.toTypedArray(), this)


        val dragAndDropListener = object : DragListView.DragListListener {
            override fun onItemDragging(itemPosition: Int, x: Float, y: Float) {
            }

            override fun onItemDragStarted(position: Int) {
            }

            override fun onItemDragEnded(fromPosition: Int, toPosition: Int) {
                if (fromPosition != toPosition) {
                    thread(start = true) {
                        val getParamIdOnPosition =
                            { position: Int -> listAdapter.getParamIdOnPosition(position) }
                        if (fromPosition > toPosition) {
                            paramViewModel.updateParamsOrder(toPosition, fromPosition, getParamIdOnPosition)
                        } else {
                            paramViewModel.updateParamsOrder(fromPosition, toPosition, getParamIdOnPosition)
                        }
                    }
                }
            }
        }
        pupil_params_draganddrop_list.setDragListListener(dragAndDropListener)
        pupil_params_draganddrop_list.setLayoutManager(LinearLayoutManager(activity))

        pupil_params_draganddrop_list.setAdapter(listAdapter, true)
        pupil_params_draganddrop_list.setCanDragHorizontally(false)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == 0) return

        val getOrderInList = { pupilId: String -> paramViewModel.getOrderForNewEntityParam(pupilId) }
        when (requestCode) {
            RequestCodes.REQUEST_CODE_PARAM_ADD -> {
                paramViewModel.save(
                    IntentBoxingUtils.extractParamInfoFromIntent(data, activity as PupilActivity, getOrderInList)
                )
            }
            RequestCodes.REQUEST_CODE_PARAM_EDIT -> {
                paramViewModel.update(
                    IntentBoxingUtils.extractParamInfoFromIntent(data, activity as PupilActivity, getOrderInList)
                )
            }
        }
    }


}
