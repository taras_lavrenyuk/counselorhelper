package com.lavreniuk.counselorhelper.activities.dialogs

import android.app.Activity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import com.itextpdf.text.log.LoggerFactory
import com.lavreniuk.counselorhelper.R
import kotlinx.android.synthetic.main.activity_param_editor.*


class ParamEditorActivity : Activity() {

    private val logger = LoggerFactory.getLogger(ParamEditorActivity::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_param_editor)

        val intent = intent
        val entityId = intent.getStringExtra(getString(R.string.intent_entity_id))
        val paramId = intent.getStringExtra(getString(R.string.intent_param_id))
        val actionType = intent.getStringExtra(getString(R.string.intent_add_or_edit))
        if (actionType == getString(R.string.intent_edit)) {
            param_name_input_field.setText(intent.getStringExtra(getString(R.string.intent_param_name)))
            param_value_input_field.setText(intent.getStringExtra(getString(R.string.intent_param_value)))
        } else if (actionType != getString(R.string.intent_add)) {
            logger.error("Unknown isSomeActivityWillBeStarted!!!")
        }

        val adapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_dropdown_item_1line,
            resources.getStringArray(R.array.params_suggestions)
        )
        param_name_input_field.threshold = 1
        param_name_input_field.setAdapter<ArrayAdapter<String>>(adapter)

        submit_button.setOnClickListener {
            val paramName = param_name_input_field.text
            val paramValue = param_value_input_field.text
            if (paramName.isBlank() || paramValue.isBlank()) {
                Toast.makeText(this, getString(R.string.ui_field_cannot_be_empty), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            intent.putExtra(getString(R.string.intent_param_name), paramName)
            intent.putExtra(getString(R.string.intent_param_value), paramValue)
            setResult(RESULT_OK, intent)
            finish()
        }

        cancel_button.setOnClickListener {
            setResult(RESULT_CANCELED, intent)
            finish()
        }

    }
}
