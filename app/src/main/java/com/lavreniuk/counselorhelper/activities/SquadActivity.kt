package com.lavreniuk.counselorhelper.activities

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.adapters.pageradapters.SquadActivityPagerAdapter
import kotlinx.android.synthetic.main.activity_squad.*

class SquadActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_squad)

        setUpTabs()
    }

    private fun setUpTabs() {
        squad_activity_tabs_layout.addTab(squad_activity_tabs_layout.newTab().setText("Squad info"))
        squad_activity_tabs_layout.addTab(squad_activity_tabs_layout.newTab().setText("Children"))
        squad_activity_tabs_layout.tabGravity = TabLayout.GRAVITY_FILL
        val pagerAdapter =
            SquadActivityPagerAdapter(supportFragmentManager)
        squad_activity_pager.adapter = pagerAdapter
        squad_activity_pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(squad_activity_tabs_layout))
        squad_activity_tabs_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                squad_activity_pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

}
