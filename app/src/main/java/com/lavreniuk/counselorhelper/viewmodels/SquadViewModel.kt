package com.lavreniuk.counselorhelper.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.lavreniuk.counselorhelper.dao.AppDatabase
import com.lavreniuk.counselorhelper.models.entities.Squad

class SquadViewModel(application: Application) : AndroidViewModel(application) {

    private val squadDao = AppDatabase.getAppDataBase(application).squadDao()

    fun getAllSquads(): List<Squad> = squadDao.loadAllSquads()

    fun saveSquad(newSquad: Squad) = squadDao.insert(newSquad)

    fun getSquad(squadId: String): Squad? = squadDao.getSquadById(squadId)

    fun updateSquadInfo(currentSquad: Squad) = squadDao.update(currentSquad)

    fun getSquadName(squadId: String): String = squadDao.getSquadNameById(squadId)

    fun getSquadPhoto(squadId: String): String = squadDao.getSquadPhoto(squadId)
}