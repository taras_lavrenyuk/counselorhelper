package com.lavreniuk.counselorhelper.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.lavreniuk.counselorhelper.dao.AppDatabase
import com.lavreniuk.counselorhelper.models.entities.Pupil

class PupilViewModel(application: Application) : AndroidViewModel(application) {

    private val pupilDao = AppDatabase.getAppDataBase(application).pupilDao()

    fun savePupil(newPupil: Pupil) = pupilDao.insert(newPupil)

    fun updatePupil(pupil: Pupil) = pupilDao.update(pupil)


    fun getPupilById(pupilId: String): Pupil = pupilDao.getPupilById(pupilId)

    fun getPupilsBySquadIdOrderByLastName(squadId: String): List<Pupil> =
        pupilDao.getPupilsOrderByLastName("%$squadId%")

    fun getExistingPupils(squadId: String, params: List<String>): List<Pupil> {
        val numberOfParams = params.size
        if (numberOfParams < 1 || numberOfParams > 3) return emptyList()
        return when (numberOfParams) {
            1 -> pupilDao.getExistingPupils("%$squadId%", params[0] + "%")
            2 -> pupilDao.getExistingPupils("%$squadId%", params[0] + "%", params[1] + "%")
            3 -> pupilDao.getExistingPupils("%$squadId%", params[0] + "%", params[1] + "%", params[2] + "%")
            else -> emptyList()
        }
    }

    fun getPupilsByIds(pupilIds: List<String>): ArrayList<Pupil> =
        pupilDao.getPupilsByIdsOrderByLastName(pupilIds) as ArrayList<Pupil>
}