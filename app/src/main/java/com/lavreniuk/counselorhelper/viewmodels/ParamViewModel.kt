package com.lavreniuk.counselorhelper.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.lavreniuk.counselorhelper.dao.AppDatabase
import com.lavreniuk.counselorhelper.models.entities.Param

class ParamViewModel(application: Application) : AndroidViewModel(application) {

    private val squadParamDao = AppDatabase.getAppDataBase(application).squadParamDao()

    fun save(param: Param) {
        squadParamDao.insert(param)
    }

    fun update(param: Param) {
        squadParamDao.update(param)
    }

    fun delete(param: Param) {
        squadParamDao.delete(param)
    }

    fun getEntityParams(entityId: String?): List<Param> {
        if (entityId == null) return emptyList()
        return squadParamDao.getOrderedEntityParams(entityId)
    }

    fun getOrderForNewEntityParam(squadId: String): Long {
        return squadParamDao.getEntityParamsCount(squadId) + 1
    }

    fun getParamById(squadParamId: String): Param {
        return squadParamDao.getParamById(squadParamId)
    }

    fun updateParamsOrder(
        startPosition: Int,
        endPosition: Int,
        getParamIdOnPosition: (position: Int) -> String
    ) {
        for (position in startPosition..endPosition) {
            updateOrderInList(getParamIdOnPosition(position), position + 1)
        }
    }

    private fun updateOrderInList(paramId: String, position: Int) {
        squadParamDao.updateOrderInList(paramId, position.toLong())
    }
}