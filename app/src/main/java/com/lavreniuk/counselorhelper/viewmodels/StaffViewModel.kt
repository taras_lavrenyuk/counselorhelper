package com.lavreniuk.counselorhelper.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.lavreniuk.counselorhelper.dao.AppDatabase
import com.lavreniuk.counselorhelper.models.entities.Staff

class StaffViewModel(application: Application) : AndroidViewModel(application) {

    private val staffDao = AppDatabase.getAppDataBase(application).staffDao()

    fun saveStaff(staff: Staff) {
        staffDao.insert(staff)
    }

    fun deleteStaff(staff: Staff) {
        staffDao.delete(staff)
    }

    fun updateStaff(staff: Staff) {
        staffDao.update(staff)
    }

    fun getStaffSortedByOrderInList(): List<Staff> {
        return staffDao.getStaffSortedByOrderInList()
    }

    fun updateStaffOrder(
        toPosition: Int,
        fromPosition: Int,
        getStaffIdOnPosition: (Int) -> String
    ) {
        for (position in toPosition..fromPosition) {
            updateOrderInList(getStaffIdOnPosition(position), position + 1)
        }

    }

    private fun updateOrderInList(staffIdOnPosition: String, orderInList: Int) {
        staffDao.updateOrderInList(staffIdOnPosition, orderInList)
    }

    fun getStaffById(staffId: String): Staff = staffDao.getStaffById(staffId)

    fun getOrderInListForNewElement(): Long = staffDao.getMaxOrderInListValue() + 1

    fun deleteAvatar(staffId: String) = staffDao.setAvatarPath(staffId, "")

    fun setAvatar(staffId: String, newPath: String) = staffDao.setAvatarPath(staffId, newPath)

    fun updateBio(staffId: String, bio: String) = staffDao.updateBio(staffId, bio)

    fun updatePosition(staffId: String, position: String) = staffDao.updatePosition(staffId, position)

}