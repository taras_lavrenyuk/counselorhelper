package com.lavreniuk.counselorhelper.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.lavreniuk.counselorhelper.dao.AppDatabase
import com.lavreniuk.counselorhelper.enums.TaskStatus
import com.lavreniuk.counselorhelper.models.entities.Note
import java.util.*

class NoteViewModel(application: Application) : AndroidViewModel(application) {

    private val noteDao = AppDatabase.getAppDataBase(application).noteDao()

    fun save(note: Note) {
        noteDao.insert(note)
    }

    fun update(note: Note) {
        noteDao.update(note)
    }

    fun delete(note: Note) {
        noteDao.delete(note)
    }

    fun deleteById(noteId: String) {
        noteDao.deleteById(noteId)
    }

    fun getNotesSortedByOrderInList(squadId: String): List<Note> {
        return noteDao.getNotesBySquadIdSortedByOrderInList(squadId)
    }

    fun updateNotesOrder(
        startPosition: Int,
        endPosition: Int,
        getNoteIdOnPosition: (Int) -> String
    ) {
        for (position in startPosition..endPosition) {
            updateOrderInList(getNoteIdOnPosition(position), position + 1)
        }
    }

    private fun updateOrderInList(noteId: String, position: Int) {
        noteDao.updateOrderInList(noteId, position.toLong())
    }

    fun getOrderForNewNote(squadId: String): Long {
        return noteDao.getSquadProgramActivitiesCount(squadId) + 1
    }

    fun markNoteAsDone(noteId: String) {
        noteDao.setNoteStatus(noteId, TaskStatus.Done)
    }

    fun markNoteAsTodo(noteId: String) {
        noteDao.setNoteStatus(noteId, TaskStatus.Todo)
    }

    fun getTodosNumberBySquadAndDate(squadId: String, date: Date): Int = noteDao.getTodosNumberBySquadAndDate(squadId, date)

    fun getTodosNumberBySquad(squadId: String): Int = noteDao.getTodosNumberBySquad(squadId)

}