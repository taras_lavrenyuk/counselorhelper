package com.lavreniuk.counselorhelper.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.lavreniuk.counselorhelper.dao.AppDatabase
import com.lavreniuk.counselorhelper.models.entities.Relative

class RelativeViewModel(application: Application) : AndroidViewModel(application) {

    private val relativeDao = AppDatabase.getAppDataBase(application).relativeDao()

    fun getPupilRelatives(pupilId: String): List<Relative> {
        return relativeDao.getPupilRelatives(pupilId)
    }

    fun save(relative: Relative) {
        relativeDao.insert(relative)
    }

    fun delete(relative: Relative) {
        relativeDao.delete(relative)
    }

    fun update(relative: Relative) {
        relativeDao.update(relative)
    }

}