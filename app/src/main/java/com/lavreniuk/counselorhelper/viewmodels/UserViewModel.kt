package com.lavreniuk.counselorhelper.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.lavreniuk.counselorhelper.dao.AppDatabase
import com.lavreniuk.counselorhelper.models.UserWithContacts
import com.lavreniuk.counselorhelper.models.entities.User

class UserViewModel(application: Application) : AndroidViewModel(application) {

    private val userInfoDao = AppDatabase.getAppDataBase(application).userInfoDao()

    fun addUserInfo(user: User) {
        userInfoDao.insert(user)
    }

    fun getUser(personId: String): User? {
        return userInfoDao.getUserInfo(personId)
    }

    fun getUserWithContacts(personId: String): UserWithContacts? {
        return userInfoDao.getUserInfoWithContacts(personId)
    }

    fun updateUser(user: User) {
        userInfoDao.update(user)
    }

}