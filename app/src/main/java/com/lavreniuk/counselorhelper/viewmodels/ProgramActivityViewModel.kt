package com.lavreniuk.counselorhelper.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.lavreniuk.counselorhelper.dao.AppDatabase
import com.lavreniuk.counselorhelper.models.entities.ProgramActivity

class ProgramActivityViewModel(application: Application) : AndroidViewModel(application) {

    private val programActivityDao = AppDatabase.getAppDataBase(application).programActivityDao()
    private val noteDao = AppDatabase.getAppDataBase(application).noteDao()

    fun save(programActivity: ProgramActivity) {
        programActivityDao.insert(programActivity)
    }

    fun delete(programActivity: ProgramActivity) {
        programActivityDao.delete(programActivity)
        noteDao.deleteNotesByProgramActivityName(programActivity.programActivityName)
    }

    fun update(programActivity: ProgramActivity) {
        val programActivityOldName = programActivityDao.getProgramActivityNameById(programActivity.programActivityId)
        if (programActivityOldName != programActivity.programActivityName) {
            noteDao.updateProgramActivitiesNames(programActivityOldName, programActivity.programActivityName)
        }
        programActivityDao.update(programActivity)
    }

    fun getProgramActivitiesSortedByOrderInList(squadId: String): List<ProgramActivity> {
        return programActivityDao.getProgramActivitiesSortedByOrderInList(squadId)
    }

    fun updateParamsOrder(
        startPosition: Int,
        endPosition: Int,
        getParamIdOnPosition: (position: Int) -> String
    ) {
        for (position in startPosition..endPosition) {
            updateOrderInList(getParamIdOnPosition(position), position + 1)
        }
    }

    private fun updateOrderInList(programActivityId: String, position: Int) {
        programActivityDao.updateOrderInList(programActivityId, position.toLong())
    }

    fun getOrderForNewEntityParam(squadId: String): Long {
        return programActivityDao.getSquadProgramActivitiesCount(squadId) + 1
    }

    fun getProgramActivitiesNamesSortedByOrderInList(squadId: String): List<String> {
        return programActivityDao.getProgramActivitiesNamesSortedByOrderInList(squadId)
    }

    fun getActivitiesByIds(activityIds: List<String>): ArrayList<ProgramActivity> =
        programActivityDao.getByIdsOrderBySort(activityIds) as ArrayList<ProgramActivity>
}