package com.lavreniuk.counselorhelper.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.lavreniuk.counselorhelper.dao.AppDatabase
import com.lavreniuk.counselorhelper.models.entities.Contact

class ContactViewModel(application: Application) : AndroidViewModel(application) {

    private val contactDao = AppDatabase.getAppDataBase(application).contactDao()

    fun saveContact(contact: Contact) {
        contactDao.insert(contact)
    }

    fun deleteContact(contact: Contact) {
        contactDao.delete(contact)
    }

    fun getUserContacts(userId: String?): List<Contact> {
        if (userId == null) return emptyList()
        return contactDao.loadUserContacts(userId)
    }

    fun updateContact(contact: Contact) {
        contactDao.update(contact)
    }
}