package com.lavreniuk.counselorhelper.dao

import android.arch.persistence.room.*
import com.lavreniuk.counselorhelper.models.entities.Contact

@Dao
interface ContactDao {

    @Insert
    fun insert(contact: Contact)

    @Delete
    fun delete(contact: Contact)

    @Query("SELECT * FROM contacts C WHERE C.personId = :userId ")
    fun loadUserContacts(userId: String): List<Contact>

    @Update
    fun update(contact: Contact)

}