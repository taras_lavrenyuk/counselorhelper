package com.lavreniuk.counselorhelper.dao

import android.arch.persistence.room.*
import com.lavreniuk.counselorhelper.models.entities.Param

@Dao
interface ParamDao {

    @Insert
    fun insert(param: Param)

    @Update
    fun update(param: Param)

    @Delete
    fun delete(param: Param)

    @Query("SELECT * from params SP WHERE entityId = :entityId ORDER BY SP.orderInList")
    fun getOrderedEntityParams(entityId: String): List<Param>

    @Query("SELECT MAX(P.orderInList) FROM params P WHERE P.entityId = :entityId ")
    fun getEntityParamsCount(entityId: String): Long

    @Query("UPDATE params SET orderInList = :position WHERE paramId = :paramId ")
    fun updateOrderInList(paramId: String, position: Long)

    @Query("SELECT * FROM params P WHERE P.paramId = :paramId ")
    fun getParamById(paramId: String): Param

}