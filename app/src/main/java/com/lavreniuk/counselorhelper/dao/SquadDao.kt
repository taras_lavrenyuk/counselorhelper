package com.lavreniuk.counselorhelper.dao

import android.arch.persistence.room.*
import com.lavreniuk.counselorhelper.models.entities.Squad

@Dao
interface SquadDao {

    @Insert
    fun insert(squad: Squad)

    @Update
    fun update(squad: Squad)

    @Delete
    fun delete(squad: Squad)

    @Query("SELECT * FROM squads S WHERE S.squadId = :squadId ")
    fun getSquadById(squadId: String): Squad

    @Query("SELECT * FROM squads ")
    fun loadAllSquads(): List<Squad>

    @Query("SELECT S.squadName FROM squads S WHERE S.squadId = :squadId ")
    fun getSquadNameById(squadId: String): String

    @Query("SELECT S.photo FROM squads S WHERE S.squadId = :squadId ")
    fun getSquadPhoto(squadId: String): String

}