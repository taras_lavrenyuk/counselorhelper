package com.lavreniuk.counselorhelper.dao

import android.arch.persistence.room.*
import com.lavreniuk.counselorhelper.models.UserWithContacts
import com.lavreniuk.counselorhelper.models.entities.User

@Dao
interface UserDao {

    @Insert
    fun insert(user: User)

    @Update
    fun update(user: User)

    @Delete
    fun delete(user: User)

    @Query("SELECT * FROM users U WHERE U.userId = :userId ")
    fun getUserInfo(userId: String): User

    @Query("SELECT * FROM users U WHERE U.userId = :userId ")
    fun getUserInfoWithContacts(userId: String): UserWithContacts

}