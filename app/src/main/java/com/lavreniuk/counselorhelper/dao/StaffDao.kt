package com.lavreniuk.counselorhelper.dao

import android.arch.persistence.room.*
import com.lavreniuk.counselorhelper.models.entities.Staff

@Dao
interface StaffDao {

    @Insert
    fun insert(staff: Staff)

    @Delete
    fun delete(staff: Staff)

    @Update
    fun update(staff: Staff)

    @Query("SELECT * FROM staff S ORDER BY S.orderInList ")
    fun getStaffSortedByOrderInList(): List<Staff>

    @Query("UPDATE staff SET orderInList = :orderInList WHERE staffId = :staffId ")
    fun updateOrderInList(staffId: String, orderInList: Int)

    @Query("SELECT * FROM staff S WHERE S.staffId = :staffId ")
    fun getStaffById(staffId: String): Staff

    @Query("SELECT MAX(S.orderInList) FROM staff S ")
    fun getMaxOrderInListValue(): Long

    @Query("UPDATE staff SET photo = :avatarPath  WHERE staffId = :staffId")
    fun setAvatarPath(staffId: String, avatarPath: String)

    @Query("UPDATE staff SET bio = :bio  WHERE staffId = :staffId")
    fun updateBio(staffId: String, bio: String)

    @Query("UPDATE staff SET position = :position  WHERE staffId = :staffId")
    fun updatePosition(staffId: String, position: String)

}