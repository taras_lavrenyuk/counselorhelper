package com.lavreniuk.counselorhelper.dao

import android.arch.persistence.room.*
import com.lavreniuk.counselorhelper.models.PupilWithContactsAndParams
import com.lavreniuk.counselorhelper.models.entities.Pupil

@Dao
interface PupilDao {

    @Insert
    fun insert(pupil: Pupil)

    @Update
    fun update(pupil: Pupil)

    @Delete
    fun delete(pupil: Pupil)

    @Query("SELECT * FROM pupils P WHERE P.pupilId = :pupilId ")
    fun getPupilById(pupilId: String): Pupil

    @Query("SELECT * FROM pupils ")
    fun loadAllPupils(): List<Pupil>

    @Query("SELECT * FROM pupils P WHERE P.pupilsSquads LIKE :squadId ORDER BY P.lastName ")
    fun getPupilsOrderByLastName(squadId: String): List<Pupil>

    @Query("SELECT * FROM pupils P WHERE P.pupilsSquads LIKE :squadId ORDER BY P.lastName ")
    fun getPupilsWithParamsAndContacts(squadId: String): List<PupilWithContactsAndParams>

    @Query("SELECT * FROM pupils P WHERE P.pupilsSquads LIKE :squadId AND P.pupilId NOT IN (:excludedPupilIds) ORDER BY P.lastName ")
    fun getPupilsWithParamsAndContacts(squadId: String, excludedPupilIds: List<String>): List<PupilWithContactsAndParams>

    @Query("SELECT * FROM pupils P WHERE (P.pupilsSquads NOT LIKE :squadId) AND (P.firstName LIKE :param1 OR P.secondName LIKE :param1 OR P.lastName LIKE :param1) ORDER BY P.lastName ")
    fun getExistingPupils(
        squadId: String,
        param1: String
    ): List<Pupil>

    @Query("SELECT * FROM pupils P WHERE (P.pupilsSquads NOT LIKE :squadId) AND (P.firstName LIKE :param1 OR P.secondName LIKE :param1 OR P.lastName LIKE :param1) AND (P.firstName LIKE :param2 OR P.secondName LIKE :param2 OR P.lastName LIKE :param2) ORDER BY P.lastName ")
    fun getExistingPupils(
        squadId: String,
        param1: String,
        param2: String
    ): List<Pupil>

    @Query("SELECT * FROM pupils P WHERE (P.pupilsSquads NOT LIKE :squadId) AND (P.firstName LIKE :param1 OR P.secondName LIKE :param1 OR P.lastName LIKE :param1) AND (P.firstName LIKE :param2 OR P.secondName LIKE :param2 OR P.lastName LIKE :param2) AND (P.firstName LIKE :param3 OR P.secondName LIKE :param3 OR P.lastName LIKE :param3) ORDER BY P.lastName ")
    fun getExistingPupils(
        squadId: String,
        param1: String,
        param2: String,
        param3: String
    ): List<Pupil>

    @Query("SELECT * FROM pupils P WHERE P.pupilId IN (:pupilIds) ORDER BY P.lastName ")
    fun getPupilsByIdsOrderByLastName(pupilIds: List<String>): List<Pupil>
}