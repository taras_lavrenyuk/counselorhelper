package com.lavreniuk.counselorhelper.dao

import android.arch.persistence.room.*
import com.lavreniuk.counselorhelper.models.entities.Relative

@Dao
interface RelativeDao {

    @Insert
    fun insert(relative: Relative)

    @Update
    fun update(relative: Relative)

    @Delete
    fun delete(relative: Relative)

    @Query("SELECT * FROM relatives R WHERE R.relativeOf = :pupilId ")
    fun getPupilRelatives(pupilId: String): List<Relative>

}