package com.lavreniuk.counselorhelper.dao

import android.arch.persistence.room.*
import com.lavreniuk.counselorhelper.enums.TaskStatus
import com.lavreniuk.counselorhelper.models.entities.Note
import java.util.*

@Dao
interface NoteDao {

    @Insert
    fun insert(note: Note)

    @Update
    fun update(note: Note)

    @Delete
    fun delete(note: Note)

    @Query("SELECT * FROM notes ")
    fun getAll(): List<Note>

    @Query("SELECT strftime('%Y-%m-%d', (:date)) ")
    fun getOneDate(date: Date): Date

    @Query("SELECT * FROM notes N WHERE N.squadId = :squadId ORDER BY N.orderInList ")
    fun getNotesBySquadIdSortedByOrderInList(squadId: String): List<Note>

    @Query("SELECT * FROM notes N WHERE N.squadId = :squadId AND N.programActivityId NOT IN (:excludedActivityIds) ORDER BY N.orderInList ")
    fun getNotesBySquadIdSortedByOrderInList(squadId: String, excludedActivityIds: List<String>): List<Note>

    @Query("DELETE FROM  notes WHERE noteId = :noteId ")
    fun deleteById(noteId: String)

    @Query("DELETE FROM notes WHERE programActivityName = :programActivityName ")
    fun deleteNotesByProgramActivityName(programActivityName: String)

    @Query("UPDATE notes SET orderInList = :orderInList WHERE noteId = :noteId ")
    fun updateOrderInList(noteId: String, orderInList: Long)

    @Query("SELECT MAX(N.orderInList) FROM notes N WHERE N.squadId = :squadId ")
    fun getSquadProgramActivitiesCount(squadId: String): Long

    @Query("UPDATE notes SET programActivityName = :programActivityNewName WHERE programActivityName = :programActivityOldName ")
    fun updateProgramActivitiesNames(programActivityOldName: String, programActivityNewName: String)

    @Query("UPDATE notes SET taskStatus = :status WHERE noteId = :noteId ")
    fun setNoteStatus(noteId: String, status: TaskStatus)

    @Query("SELECT N.* FROM notes N WHERE N.noteOrTask = 'Note' AND N.programActivityId = :programActivityName AND strftime('%Y-%m-%d', N.noteTargetDate) = strftime('%Y-%m-%d', :date) ORDER BY N.noteTargetDate ASC ")
    fun getNotesByProgramActivityAndDateSortedByDescending(programActivityName: String, date: Date): List<Note>

    @Query("SELECT N.* FROM notes N WHERE N.noteOrTask = 'Note' AND N.programActivityName = :programActivityName ORDER BY N.noteTargetDate IS NULL ASC ")
    fun getNotesByProgramActivitySortedByDescending(programActivityName: String): List<Note>

    @Query("SELECT N.* FROM notes N WHERE N.noteOrTask = 'Task' AND N.taskStatus = :taskStatus AND N.programActivityName = :programActivityName AND N.noteTargetDate = :date ORDER BY N.noteTargetDate ASC ")
    fun getTasksByProgramActivityAndStatusAndDateSortedByDateAsc(
        taskStatus: String,
        programActivityName: String,
        date: Date
    ): List<Note>

    @Query("SELECT N.* FROM notes N WHERE N.noteOrTask = 'Task' AND N.taskStatus = :taskStatus AND N.programActivityName = :programActivityName ORDER BY N.noteTargetDate IS NULL ASC ")
    fun getTasksByProgramActivityAndStatusSortedByDateAsc(
        taskStatus: String,
        programActivityName: String
    ): List<Note>

    @Query("SELECT COUNT(*) FROM notes N WHERE N.noteOrTask = 'Task' AND N.taskStatus = 'Todo' AND N.noteTargetDate = :date AND N.squadId = :squadId ")
    fun getTodosNumberBySquadAndDate(squadId: String, date: Date): Int

    @Query("SELECT COUNT(*) FROM notes N WHERE N.noteOrTask = 'Task' AND N.taskStatus = 'Todo' AND N.squadId = :squadId ")
    fun getTodosNumberBySquad(squadId: String): Int
}