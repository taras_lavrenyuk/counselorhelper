package com.lavreniuk.counselorhelper.dao

import android.arch.persistence.room.*
import com.lavreniuk.counselorhelper.models.entities.ProgramActivity

@Dao
interface ProgramActivityDao {

    @Insert
    fun insert(programActivity: ProgramActivity)

    @Update
    fun update(programActivity: ProgramActivity)

    @Delete
    fun delete(programActivity: ProgramActivity)

    @Query("SELECT * FROM program_activities PA WHERE PA.squadId = :squadId ORDER BY PA.orderInList")
    fun getProgramActivitiesSortedByOrderInList(squadId: String): List<ProgramActivity>

    @Query("UPDATE program_activities SET orderInList = :orderInList WHERE programActivityId = :programActivityId ")
    fun updateOrderInList(programActivityId: String, orderInList: Long)

    @Query("SELECT MAX(PA.orderInList) FROM program_activities PA WHERE PA.squadId = :squadId ")
    fun getSquadProgramActivitiesCount(squadId: String): Long

    @Query("SELECT PA.programActivityName FROM program_activities PA WHERE PA.squadId = :squadId ORDER BY PA.orderInList ")
    fun getProgramActivitiesNamesSortedByOrderInList(squadId: String): List<String>

    @Query("SELECT PA.programActivityName FROM program_activities PA WHERE PA.programActivityId = :programActivityId ")
    fun getProgramActivityNameById(programActivityId: String): String

    @Query("SELECT PA.* FROM program_activities PA WHERE PA.programActivityId in (:activityIds) ORDER BY PA.orderInList ")
    fun getByIdsOrderBySort(activityIds: List<String>): List<ProgramActivity>

    @Query("SELECT PA.* FROM program_activities PA WHERE PA.squadId = :squadId AND PA.programActivityId NOT IN (:excludedActivityIds) ORDER BY PA.orderInList ")
    fun getBySquadSortedByOrder(squadId: String, excludedActivityIds: List<String>): List<ProgramActivity>
}