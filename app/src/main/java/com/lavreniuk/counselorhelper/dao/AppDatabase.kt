package com.lavreniuk.counselorhelper.dao

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.lavreniuk.counselorhelper.models.entities.*
import com.lavreniuk.counselorhelper.reports.dao.AbstractReportDao
import com.lavreniuk.counselorhelper.reports.dao.ProgramReportDao
import com.lavreniuk.counselorhelper.reports.dao.PupilListReportDao
import com.lavreniuk.counselorhelper.reports.models.AbstractReport
import com.lavreniuk.counselorhelper.reports.models.ProgramReport
import com.lavreniuk.counselorhelper.reports.models.PupilListReport
import com.lavreniuk.counselorhelper.utils.Converters


@Database(
    entities = [
        User::class, Squad::class, Contact::class, Pupil::class, Param::class, Relative::class,
        ProgramActivity::class, Note::class, Staff::class, AbstractReport::class, PupilListReport::class, ProgramReport::class],
    version = 42
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userInfoDao(): UserDao
    abstract fun squadDao(): SquadDao
    abstract fun contactDao(): ContactDao
    abstract fun pupilDao(): PupilDao
    abstract fun squadParamDao(): ParamDao
    abstract fun relativeDao(): RelativeDao
    abstract fun programActivityDao(): ProgramActivityDao
    abstract fun noteDao(): NoteDao
    abstract fun staffDao(): StaffDao
    abstract fun pupilListReportDao(): PupilListReportDao
    abstract fun programReportDao(): ProgramReportDao
    abstract fun abstractReportDao(): AbstractReportDao

    companion object {
        var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "counselor_database"
                    )
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE as AppDatabase

        }

        fun destroyDataBase() {
            INSTANCE = null
        }

    }
}