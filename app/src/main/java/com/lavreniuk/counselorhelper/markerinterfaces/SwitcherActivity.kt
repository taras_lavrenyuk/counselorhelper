package com.lavreniuk.counselorhelper.markerinterfaces

import android.widget.Switch

interface SwitcherActivity {

    fun setSwitcher(switcher: Switch, value: Boolean) {
        switcher.isChecked = value
    }
}
