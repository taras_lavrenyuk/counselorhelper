package com.lavreniuk.counselorhelper.markerinterfaces

import com.lavreniuk.counselorhelper.reports.models.dtos.AbstractReportDto

interface ReportEditorActivity : SwitcherActivity {

    abstract fun updateReport(reportName: String?)

    abstract fun getDtoObject(): AbstractReportDto

}