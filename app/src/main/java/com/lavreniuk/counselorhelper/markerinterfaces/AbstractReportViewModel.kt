package com.lavreniuk.counselorhelper.markerinterfaces

import com.lavreniuk.counselorhelper.reports.models.dtos.AbstractReportDto

interface AbstractReportViewModel {

    abstract fun generateReportDocument(dto: AbstractReportDto): String?

}