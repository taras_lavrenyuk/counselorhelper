package com.lavreniuk.counselorhelper.listeners

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.models.dtos.Time
import com.lavreniuk.counselorhelper.utils.Converters
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import java.util.*

class TimeInputOnClickListener(
    private val activity: Activity,
    private val editText: EditText
) : View.OnClickListener {

    override fun onClick(view: View?) {
        UtilsMethods.hideKeyboard(activity)

        val tpd = TimePickerDialog(
            view!!.context,
            R.style.Theme_AppCompat_Light_Dialog,
            TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                editText.setText(Time(hourOfDay, minute).toString())
            },
            9,
            0,
            true
        )

        tpd.show()
    }
}