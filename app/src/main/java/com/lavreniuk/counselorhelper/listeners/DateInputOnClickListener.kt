package com.lavreniuk.counselorhelper.listeners

import android.app.Activity
import android.app.DatePickerDialog
import android.view.View
import android.widget.TextView
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import java.util.*

class DateInputOnClickListener(
    private val activity: Activity
) : View.OnClickListener {

    override fun onClick(view: View?) {
        UtilsMethods.hideKeyboard(activity)
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(
            view!!.context,
            R.style.Theme_AppCompat_Light_Dialog,
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->

                val cal = Calendar.getInstance()
                cal.set(year, monthOfYear, dayOfMonth)
                val targetDate = cal.time

                (view as TextView).text = StringTransformationUtils.fromDateToString(targetDate)

            },
            year,
            month,
            day
        )

        dpd.show()
    }

}