package com.lavreniuk.counselorhelper.itemviews

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.models.entities.Pupil
import com.lavreniuk.counselorhelper.utils.PhotoLoaderUtils
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import kotlinx.android.synthetic.main.pupil_item_view.view.*

class PupilItemView(context: Context, val pupil: Pupil) : LinearLayout(context) {

    init {
        View.inflate(context, R.layout.pupil_item_view, this)

        pupil_item_pupil_name.text = StringTransformationUtils.getFullNameString(pupil, false)

        val pupilPhoto = PhotoLoaderUtils.getBitmapFromPath(pupil.photo)
        if (pupilPhoto != null) pupil_item_pupil_photo.setImageBitmap(pupilPhoto)
    }
}