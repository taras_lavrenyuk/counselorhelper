package com.lavreniuk.counselorhelper.itemviews

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.models.entities.Contact
import kotlinx.android.synthetic.main.contact_item_view.view.*

class ContactItemView(context: Context, contact: Contact) : LinearLayout(context) {

    init {
        View.inflate(context, R.layout.contact_item_view, this)
        contact_item_contact_type.text = contact.contactType.toString()
        contact_item_contact_value.text = contact.value

        val drawable = resources.getDrawable(contact.contactType.getIconResourceId(), null)
        contact_item_contact_icon.setImageDrawable(drawable)
    }
}