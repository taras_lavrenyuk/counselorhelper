package com.lavreniuk.counselorhelper.itemviews

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.models.entities.Relative
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import kotlinx.android.synthetic.main.relative_item_view.view.*

class RelativeItemView(context: Context, val relative: Relative) : LinearLayout(context) {

    init {
        View.inflate(context, R.layout.relative_item_view, this)

        relative_name.text = StringTransformationUtils.getFullNameString(relative, true)
        relative_connection.text = relative.connection
        relative_contact.text = relative.relativeContact
    }

}