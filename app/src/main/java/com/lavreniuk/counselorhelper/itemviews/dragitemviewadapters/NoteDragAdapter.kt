package com.lavreniuk.counselorhelper.itemviews.dragitemviewadapters

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.ProgramActivityActivity
import com.lavreniuk.counselorhelper.activities.dialogs.NoteEditorActivity
import com.lavreniuk.counselorhelper.enums.NoteOrTask
import com.lavreniuk.counselorhelper.enums.TaskStatus
import com.lavreniuk.counselorhelper.models.entities.Note
import com.lavreniuk.counselorhelper.utils.RequestCodes
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import com.lavreniuk.counselorhelper.viewmodels.NoteViewModel
import com.woxthebox.draglistview.DragItemAdapter
import kotlinx.android.synthetic.main.note_drag_item_view.view.*

class NoteDragAdapter(
    list: Array<Note>,
    private val fragment: Fragment,
    private val getProgramActivitiesNames: () -> List<String>,
    private val deleteNote: (String) -> Unit,
    private val fragmentOnResume: () -> Unit
) : DragItemAdapter<Note, NoteDragAdapter.ViewHolder>() {

    private val noteViewModel: NoteViewModel by lazy {
        ViewModelProviders.of(fragment.context as AppCompatActivity).get(NoteViewModel::class.java)
    }

    init {
        itemList = list.toMutableList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.note_drag_item_view,
                parent,
                false
            )
        )
    }

    override fun getUniqueItemId(position: Int): Long {
        return mItemList[position].orderInList
    }

    override fun onBindViewHolder(holder: NoteDragAdapter.ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.initHolder(mItemList[position], fragment.context!!)
    }

    fun getNoteIdOnPosition(position: Int): String {
        return mItemList[position].noteId
    }

    inner class ViewHolder(itemView: View) :
        DragItemAdapter.ViewHolder(itemView, R.id.note_drag_item_layout, true) {

        private val noteDate: TextView = itemView.findViewById(R.id.note_date)
        private val noteActivityName: TextView = itemView.findViewById(R.id.note_activity_name)
        private val noteText: TextView = itemView.findViewById(R.id.note_text)
        private val noteStatus: TextView = itemView.findViewById(R.id.note_status)
        private val noteParamIcon: ImageView = itemView.findViewById(R.id.note_param_icon)


        override fun onItemClicked(view: View) {
        }

        override fun onItemLongClicked(view: View): Boolean {
            return true
        }

        fun initHolder(note: Note, context: Context) {

            val noteTargetDateString =
                if (note.noteTargetDate == null || note.noteTargetDate!!.equals(UtilsMethods.getDefaultDate())) ""
                else StringTransformationUtils.fromDateToString(note.noteTargetDate!!)

            noteText.text = note.noteText
            noteStatus.text = if (note.noteOrTask == NoteOrTask.Note) {
                note.noteOrTask.name.toLowerCase()
            } else {
                note.taskStatus!!.name.toLowerCase()
            }

            if (noteTargetDateString.isBlank() && note.programActivityName.isNullOrBlank()) {
                itemView.note_date_and_activity_name_layout.visibility = View.GONE
            } else if (noteTargetDateString.isBlank()) {
                itemView.note_date_layout.visibility = View.GONE
                setMarginStart(itemView.note_activity_name_layout, 10)
                noteActivityName.text = note.programActivityName
            } else {
                noteDate.text = noteTargetDateString
                noteActivityName.text = note.programActivityName
            }

            itemView.tag = note.orderInList

            noteParamIcon.setOnClickListener {
                val popupMenu = PopupMenu(context, noteParamIcon)

                if (note.noteOrTask == NoteOrTask.Task && note.taskStatus == TaskStatus.Todo) {
                    val markAsDonePopupMenuItem = popupMenu.menu.add(context.getString(R.string.ui_mark_as_done))
                    markAsDonePopupMenuItem.setOnMenuItemClickListener {
                        noteViewModel.markNoteAsDone(note.noteId)
                        fragmentOnResume()
                        Toast.makeText(context, context.getString(R.string.ui_marked_as_done), Toast.LENGTH_SHORT)
                            .show()
                        return@setOnMenuItemClickListener true
                    }
                } else if (note.noteOrTask == NoteOrTask.Task && note.taskStatus == TaskStatus.Done) {
                    val markAsTodoPopupMenuItem = popupMenu.menu.add(context.getString(R.string.ui_mark_as_todo))
                    markAsTodoPopupMenuItem.setOnMenuItemClickListener {
                        noteViewModel.markNoteAsTodo(note.noteId)
                        fragmentOnResume()
                        Toast.makeText(context, context.getString(R.string.ui_marked_as_todo), Toast.LENGTH_SHORT)
                            .show()
                        return@setOnMenuItemClickListener true
                    }
                }

                val editPopupMenuItem = popupMenu.menu.add(context.getString(R.string.ui_edit))
                editPopupMenuItem.setOnMenuItemClickListener {
                    val intent = Intent(context as ProgramActivityActivity, NoteEditorActivity::class.java)
                    intent.putExtra(
                        context.getString(R.string.intent_add_or_edit),
                        context.getString(R.string.intent_edit)
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_note_text),
                        note.noteText
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_note_id),
                        note.noteId
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_note_activity_id),
                        note.programActivityId
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_note_activity_name),
                        note.programActivityName
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_note_target_date),
                        noteTargetDateString
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_task_status),
                        note.taskStatus.toString()
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_note_or_task),
                        note.noteOrTask.toString()
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_order_in_list),
                        note.orderInList
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_program_activities_names),
                        getProgramActivitiesNames().toTypedArray()
                    )
                    fragment.startActivityForResult(intent, RequestCodes.REQUEST_CODE_NOTE_EDIT)
                    return@setOnMenuItemClickListener true
                }

                val deletePopupMenuItem = popupMenu.menu.add(context.getString(R.string.ui_delete))
                deletePopupMenuItem.setOnMenuItemClickListener {
                    deleteNote(note.noteId)
                    fragmentOnResume()
                    Toast.makeText(context, context.getString(R.string.ui_deleted), Toast.LENGTH_SHORT).show()
                    return@setOnMenuItemClickListener true
                }
                popupMenu.show()
            }
        }

        private fun setMarginStart(layout: RelativeLayout, marginStartDp: Int) {
            val layoutParams = layout.layoutParams as RelativeLayout.LayoutParams
            layoutParams.marginStart = UtilsMethods.dpToPx(marginStartDp)
            layout.layoutParams = layoutParams
        }
    }

}