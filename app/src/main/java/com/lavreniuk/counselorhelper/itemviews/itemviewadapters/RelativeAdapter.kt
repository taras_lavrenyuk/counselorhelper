package com.lavreniuk.counselorhelper.itemviews.itemviewadapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.lavreniuk.counselorhelper.models.entities.Relative
import com.lavreniuk.counselorhelper.itemviews.RelativeItemView

class RelativeAdapter(
    private val context: Context,
    private val relatives: Array<Relative>
) : BaseAdapter() {

    override fun getCount(): Int {
        return relatives.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Any {
        return relatives[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return RelativeItemView(context, relatives[position])
    }
}