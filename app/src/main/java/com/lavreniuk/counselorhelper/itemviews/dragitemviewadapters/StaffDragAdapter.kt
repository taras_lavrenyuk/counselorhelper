package com.lavreniuk.counselorhelper.itemviews.dragitemviewadapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.StaffListActivity
import com.lavreniuk.counselorhelper.activities.StaffPageActivity
import com.lavreniuk.counselorhelper.models.entities.Staff
import com.lavreniuk.counselorhelper.utils.PhotoLoaderUtils
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import com.woxthebox.draglistview.DragItemAdapter
import kotlinx.android.synthetic.main.staff_drag_item_view.view.*

class StaffDragAdapter(
    list: Array<Staff>,
    private val staffListActivity: StaffListActivity,
    private val activityOnResume: () -> Unit,
    private val deleteStaff: (Staff) -> Unit
) : DragItemAdapter<Staff, StaffDragAdapter.ViewHolder>() {

    init {
        itemList = list.toMutableList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.staff_drag_item_view,
                parent,
                false
            )
        )
    }

    override fun getUniqueItemId(position: Int): Long {
        return mItemList[position].orderInList
    }

    override fun onBindViewHolder(holder: StaffDragAdapter.ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.initHolder(mItemList[position], staffListActivity)
    }

    fun getStaffIdOnPosition(position: Int): String {
        return mItemList[position].staffId
    }

    inner class ViewHolder(itemView: View) :
        DragItemAdapter.ViewHolder(itemView, R.id.staff_drag_item_layout, true) {

        private val staffPhoto: ImageView = itemView.findViewById(R.id.staff_item_photo)
        private val staffName: TextView = itemView.findViewById(R.id.staff_item_name)
        private val staffAdditionalInfo: TextView = itemView.findViewById(R.id.staff_item_additional_info)
        private val staffParamIcon: ImageView = itemView.findViewById(R.id.staff_param_icon)

        lateinit var currentStaff: Staff

        override fun onItemLongClicked(view: View): Boolean {
            Toast.makeText(staffListActivity, "LONG CLICK", Toast.LENGTH_SHORT).show()
            return true
        }

        fun initHolder(staff: Staff, context: Context) {

            itemView.staff_drag_item_layout.setOnClickListener {
                val intent = Intent(staffListActivity, StaffPageActivity::class.java)
                intent.putExtra(
                    staffListActivity.getString(R.string.intent_new_or_existing),
                    staffListActivity.getString(R.string.intent_existing)
                )
                intent.putExtra(staffListActivity.getString(R.string.intent_staff_id), currentStaff.staffId)
                staffListActivity.startActivity(intent)
            }

            this.currentStaff = staff

            staffName.text = StringTransformationUtils.getFullNameString(staff, false)
            staff.position?.let {
                if (it.isNotBlank()) {
                    staffAdditionalInfo.text = staff.position
                }
            }
            val staffPhotoBitmap = PhotoLoaderUtils.getBitmapFromPath(staff.photo)
            if (staffPhotoBitmap != null) staffPhoto.setImageBitmap(staffPhotoBitmap)

            itemView.tag = staff.orderInList

            staffParamIcon.setOnClickListener {
                val popupMenu = PopupMenu(context, staffParamIcon)

                val deletePopupMenuItem = popupMenu.menu.add(context.getString(R.string.ui_delete))
                deletePopupMenuItem.setOnMenuItemClickListener {
                    deleteStaff(staff)
                    activityOnResume()
                    Toast.makeText(context, context.getString(R.string.ui_deleted), Toast.LENGTH_SHORT).show()
                    return@setOnMenuItemClickListener true
                }
                popupMenu.show()
            }
        }
    }

}