package com.lavreniuk.counselorhelper.itemviews

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.models.entities.PersonInfo
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import kotlinx.android.synthetic.main.simple_dropdown_item_view.view.*

@Deprecated("Should be deleted")
class PersonNameItemView(
    context: Context,
    person: PersonInfo
) : LinearLayout(context) {

    init {
        View.inflate(context, R.layout.simple_dropdown_item_view, this)
        simple_dropdown_textview.text = StringTransformationUtils.getFullNameString(person, false)
    }
}