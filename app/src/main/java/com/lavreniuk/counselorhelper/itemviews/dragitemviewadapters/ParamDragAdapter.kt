package com.lavreniuk.counselorhelper.itemviews.dragitemviewadapters

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.dialogs.ParamEditorActivity
import com.lavreniuk.counselorhelper.models.entities.Param
import com.lavreniuk.counselorhelper.utils.RequestCodes
import com.lavreniuk.counselorhelper.viewmodels.ParamViewModel
import com.woxthebox.draglistview.DragItemAdapter


class ParamDragAdapter(
    list: Array<Param>,
    private val fragment: Fragment
) : DragItemAdapter<Param, ParamDragAdapter.ViewHolder>() {

    private val paramViewModel: ParamViewModel by lazy {
        ViewModelProviders.of(fragment.context as AppCompatActivity).get(ParamViewModel::class.java)
    }

    init {
        itemList = list.toMutableList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.param_drag_item_view,
                parent,
                false
            )
        )
    }

    fun getParamValueOnPosition(position: Int): String {
        return mItemList[position].paramValue
    }

    fun getParamIdOnPosition(position: Int): String {
        return mItemList[position].paramId
    }

    override fun getUniqueItemId(position: Int): Long {
        return mItemList[position].orderInList
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.initHolder(mItemList[position], fragment.context!!)
    }

    inner class ViewHolder(itemView: View) :
        DragItemAdapter.ViewHolder(itemView, R.id.param_drag_item_layout, true) {

        private val paramValue: TextView = itemView.findViewById(R.id.drag_item_param_value)
        private val paramName: TextView = itemView.findViewById(R.id.drag_item_param_name)
        private val paramIcon: ImageView = itemView.findViewById(R.id.drag_item_param_icon)

        override fun onItemClicked(view: View) {
        }

        override fun onItemLongClicked(view: View): Boolean {
            return true
        }

        fun initHolder(param: Param, context: Context) {
            paramName.text = param.paramName
            paramValue.text = param.paramValue
            itemView.tag = param.orderInList

            paramIcon.setOnClickListener {
                val popupMenu = PopupMenu(context, paramIcon)

                val editPopupMenuItem = popupMenu.menu.add(context.getString(R.string.ui_edit))
                editPopupMenuItem.setOnMenuItemClickListener {
                    val intent = Intent(context as AppCompatActivity, ParamEditorActivity::class.java)
                    intent.putExtra(
                        context.getString(R.string.intent_add_or_edit),
                        context.getString(R.string.intent_edit)
                    )
                    intent.putExtra(context.getString(R.string.intent_entity_id), param.entityId)
                    intent.putExtra(context.getString(R.string.intent_param_id), param.paramId)
                    intent.putExtra(context.getString(R.string.intent_param_name), param.paramName)
                    intent.putExtra(context.getString(R.string.intent_param_value), param.paramValue)
                    intent.putExtra(
                        context.getString(R.string.intent_order_in_list),
                        param.orderInList
                    )
                    fragment.startActivityForResult(intent, RequestCodes.REQUEST_CODE_PARAM_EDIT)
                    return@setOnMenuItemClickListener true
                }

                val deletePopupMenuItem = popupMenu.menu.add(context.getString(R.string.ui_delete))
                deletePopupMenuItem.setOnMenuItemClickListener {
                    paramViewModel.delete(param)
                    fragment.onResume()
                    Toast.makeText(context, "Param \"${param.paramName}\" deleted.", Toast.LENGTH_SHORT).show()
                    return@setOnMenuItemClickListener true
                }

                popupMenu.show()
            }
        }
    }


}