package com.lavreniuk.counselorhelper.itemviews.itemviewadapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.lavreniuk.counselorhelper.models.entities.Contact
import com.lavreniuk.counselorhelper.itemviews.ContactItemView


class ContactAdapter(
    private val context: Context,
    private val contacts: Array<Contact>
) : BaseAdapter() {

    override fun getCount(): Int {
        return contacts.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Any {
        return contacts[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return ContactItemView(context, contacts[position])
    }
}