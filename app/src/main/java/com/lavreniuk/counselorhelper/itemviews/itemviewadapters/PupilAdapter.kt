package com.lavreniuk.counselorhelper.itemviews.itemviewadapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.lavreniuk.counselorhelper.models.entities.Pupil
import com.lavreniuk.counselorhelper.itemviews.PupilItemView

class PupilAdapter(
    private val context: Context,
    private val pupils: Array<Pupil>
) : BaseAdapter() {

    override fun getCount(): Int {
        return pupils.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Any {
        return pupils[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return PupilItemView(context, pupils[position])
    }
}