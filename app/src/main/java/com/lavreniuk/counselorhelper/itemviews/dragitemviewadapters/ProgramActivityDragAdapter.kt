package com.lavreniuk.counselorhelper.itemviews.dragitemviewadapters

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.lavreniuk.counselorhelper.R
import com.lavreniuk.counselorhelper.activities.dialogs.ProgramActivityEditorActivity
import com.lavreniuk.counselorhelper.models.entities.ProgramActivity
import com.lavreniuk.counselorhelper.utils.RequestCodes
import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import com.lavreniuk.counselorhelper.utils.UtilsMethods
import com.lavreniuk.counselorhelper.viewmodels.ProgramActivityViewModel
import com.woxthebox.draglistview.DragItemAdapter
import kotlinx.android.synthetic.main.program_activity_drag_item_view.view.*

class ProgramActivityDragAdapter(
    list: Array<ProgramActivity>,
    private val fragment: Fragment
) : DragItemAdapter<ProgramActivity, ProgramActivityDragAdapter.ViewHolder>() {

    private val programActivityViewModel: ProgramActivityViewModel by lazy {
        ViewModelProviders.of(fragment.context as AppCompatActivity).get(ProgramActivityViewModel::class.java)
    }

    init {
        itemList = list.toMutableList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.program_activity_drag_item_view,
                parent,
                false
            )
        )
    }

    override fun getUniqueItemId(position: Int): Long {
        return mItemList[position].orderInList
    }

    override fun onBindViewHolder(holder: ProgramActivityDragAdapter.ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.initHolder(mItemList[position], fragment.context!!)
    }

    fun getProgramActivityIdOnPosition(position: Int): String {
        return mItemList[position].programActivityId
    }

    inner class ViewHolder(itemView: View) :
        DragItemAdapter.ViewHolder(itemView, R.id.program_activity_drag_item_layout, true) {

        private val time: TextView = itemView.findViewById(R.id.program_activity_time)
        private val programActivityName: TextView = itemView.findViewById(R.id.program_activity_name)
        private val programActivityDescription: TextView = itemView.findViewById(R.id.program_activity_description)
        private val paramIcon: ImageView = itemView.findViewById(R.id.program_activity_drag_item_param_icon)


        override fun onItemClicked(view: View) {
        }

        override fun onItemLongClicked(view: View): Boolean {
            return true
        }

        @SuppressLint("SetTextI18n")
        fun initHolder(programActivity: ProgramActivity, context: Context) {

            // set up position of: time and name textViews
            // both times (startTime and endTime) are not set
            if (programActivity.startTime.toString().isEmpty() && programActivity.endTime.toString().isEmpty()) {
                itemView.program_activity_time_layout.visibility = View.GONE
                setMarginStart(itemView.program_activity_name_layout, 15)
            } else { // at least one time is set
                time.text = StringTransformationUtils.getProgramActivityTime(programActivity, context, true)
            }

            programActivityName.text = programActivity.programActivityName

            if (programActivity.programActivityDescription.isBlank()) {
                itemView.program_activity_description_layout.visibility = View.GONE
            } else {
                programActivityDescription.text = programActivity.programActivityDescription
            }
            itemView.tag = programActivity.orderInList

            paramIcon.setOnClickListener {
                val popupMenu = PopupMenu(context, paramIcon)

                val editPopupMenuItem = popupMenu.menu.add(context.getString(R.string.ui_edit))
                editPopupMenuItem.setOnMenuItemClickListener {
                    val intent = Intent(context as AppCompatActivity, ProgramActivityEditorActivity::class.java)
                    intent.putExtra(
                        context.getString(R.string.intent_add_or_edit),
                        context.getString(R.string.intent_edit)
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_program_activity_name),
                        programActivity.programActivityName
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_program_activity_description),
                        programActivity.programActivityDescription
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_program_activity_start_time),
                        programActivity.startTime.toString()
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_program_activity_end_time),
                        programActivity.endTime.toString()
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_program_activity_id),
                        programActivity.programActivityId
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_order_in_list),
                        programActivity.orderInList
                    )
                    intent.putExtra(
                        context.getString(R.string.intent_program_activities_names),
                        programActivityViewModel.getProgramActivitiesNamesSortedByOrderInList(programActivity.squadId).toTypedArray()
                    )

                    fragment.startActivityForResult(intent, RequestCodes.REQUEST_CODE_PROGRAM_ACTIVITY_EDIT)
                    return@setOnMenuItemClickListener true
                }

                val deletePopupMenuItem = popupMenu.menu.add(context.getString(R.string.ui_delete))
                deletePopupMenuItem.setOnMenuItemClickListener {
                    programActivityViewModel.delete(programActivity)
                    fragment.onResume()
                    Toast.makeText(context, context.getString(R.string.ui_deleted), Toast.LENGTH_SHORT).show()
                    return@setOnMenuItemClickListener true
                }
                popupMenu.show()
            }
        }

        private fun setMarginStart(layout: RelativeLayout, marginStartDp: Int) {
            val layoutParams = layout.layoutParams as RelativeLayout.LayoutParams
            layoutParams.marginStart = UtilsMethods.dpToPx(marginStartDp)
            layout.layoutParams = layoutParams
        }
    }
}