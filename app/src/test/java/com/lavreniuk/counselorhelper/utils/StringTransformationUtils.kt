package com.lavreniuk.counselorhelper.utils

import junit.framework.Assert.assertEquals
import junitparams.JUnitParamsRunner
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(JUnitParamsRunner::class)
class StringTransformationUtilsTest {

    @Test
    fun getYearDifference() {
        val calendar = Calendar.getInstance()
        calendar.set(1997, Calendar.MARCH, 22)
        assertEquals(22, StringTransformationUtils.getAgeFromBirthday(calendar.time))
    }

}