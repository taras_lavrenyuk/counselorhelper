package com.lavreniuk.counselorhelper

import com.lavreniuk.counselorhelper.utils.StringTransformationUtils
import org.junit.Assert.assertEquals
import org.junit.Test
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun dateTest() {
        val birthdayText = "22/03/1997"
        val localDate = LocalDate.parse(birthdayText, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
        val date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant())

        val message = SimpleDateFormat("dd/MM/yyyy").format(date)

        println(message is String)
    }

    @Test
    fun fromStringToDateTest() {
        val birthdayText = "22/03/1997"
        val result = StringTransformationUtils.fromStringToDate(birthdayText)
        println(result)
    }

    @Test
    fun UUIDtest() {
        println(UUID.randomUUID().toString())
    }

    @Test
    fun rangeLoopTest() {
        val a = 6
        val b = 9

        for (i in b downTo a) {
            println(i)
        }
    }

    @Test
    fun getClassNameTest() {
        println(this.javaClass.name)
    }
}
